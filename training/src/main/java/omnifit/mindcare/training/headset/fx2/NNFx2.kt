package omnifit.mindcare.training.headset.fx2

import android.bluetooth.BluetoothSocket
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import omnifit.mindcare.training.headset.*

interface NNFx2 {

    fun init(context: Context)

    fun getName(): String

    fun setBluetoothSocket(socket: BluetoothSocket)

    fun isMeasuring(): Boolean

    fun startMeasurement(
        owner: LifecycleOwner,
        limit: Int = MEASUREMENT_DURATION_INFINITE,
        eyesState: EyesStatus,
        onChange: (MeasurementStatus) -> Unit = {}
    )

    fun stopMeasurement(cause: CancelCause? = null)

    fun powerMode(): PowerMode

    fun operationMode(): OperationMode

    fun observeOperationModeChange(
        owner: LifecycleOwner,
        onChange: (OperationMode) -> Unit
    )

    fun batteryLevel(): Int

    fun observeBatteryLevelChange(
        owner: LifecycleOwner,
        onChange: (Int) -> Unit
    )

    fun isSensorsAttached(): Boolean

    fun observeSensorsAttachmentStatusChange(
        owner: LifecycleOwner,
        onChange: (SensorsAttachmentStatus) -> Unit
    )

    fun observeBrainWaveChange(
        owner: LifecycleOwner,
        onChange: (Double) -> Unit
    )
}