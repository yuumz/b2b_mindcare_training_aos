package omnifit.mindcare.training.headset.fx2.io

interface Frame {

    val length: Int

    val flag: ByteArray

    interface Parser {
        fun parse(b: ByteArray)
    }
}

class NNFx2Frame : Frame {

    override val length: Int
        get() = 20

    override val flag: ByteArray
        get() = byteArrayOf(0xFF.toByte(), 0xFE.toByte())
}