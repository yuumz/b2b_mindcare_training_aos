package omnifit.mindcare.training.headset

import android.bluetooth.BluetoothDevice
import android.graphics.Path
import android.util.SparseArray

enum class MeasurementEquipment {
    UNKNOWN {
        override fun deviceName(): String {
            return ""
        }

        override fun deviceId(): Int {
            return 0
        }
    },
    NEURO_NICLE_FX2 {
        override fun deviceName(): String {
            return "neuroNicle FX2"
        }

        override fun deviceId(): Int {
            return 35
        }
    },
    OCW_H11 {
        override fun deviceName(): String {
            return "OCW-H11"
        }

        override fun deviceId(): Int {
            return 44
        }
    };

    abstract fun deviceName(): String

    abstract fun deviceId(): Int
}

enum class ConnectionStatus {
    UNKNOWN,
    CONNECTING,
    CONNECTED,
    DISCONNECTED,
    FORCE_DISCONNECTED,
    CONNECT_FAILURE,
    POWER_OFF;

    companion object {
        fun wrap(status: Int): ConnectionStatus {
            return ConnectionStatus.values()[status]
        }
    }
}

enum class PowerMode {
    UNKNOWN,
    BATTERY,
    ADAPTER
}

enum class OperationMode {
    WAITING,
    MEASURING,
    CHARGING;

    companion object {
        operator fun get(op: Int): OperationMode {
            return OperationMode.values()[op]
        }
    }
}

enum class SensorsAttachmentStatus {
    ATTACHED,
    DETACHED
}

enum class SensorsSignalStability(val value: Int) {
    UNSTABILIZED(0),
    STABILIZED(1);

    companion object {
        fun of(raw: Int): SensorsSignalStability = values()[raw]
    }
}

enum class Heartbeat {
    OCCURRED,
    NOT_OCCURRED
}

sealed class ScanStatus {
    object Start : ScanStatus()
    data class Found(
        val result: BluetoothDevice
    ) : ScanStatus()

    object Finished : ScanStatus()
}

enum class EyesStatus {
    CLOSED, OPENED
}

sealed class MeasurementStatus {
    open val elapsedTime: Int = 0

    data class Start(
        override val elapsedTime: Int,
        val sensorsStatus: SensorsAttachmentStatus
    ) : MeasurementStatus()

    data class Measuring(
        override val elapsedTime: Int,
        val sensorsStatus: SensorsAttachmentStatus,
        val signalStability: SensorsSignalStability,
        val values: DoubleArray = doubleArrayOf()
    ) : MeasurementStatus() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Measuring
            if (elapsedTime != other.elapsedTime) return false
            if (sensorsStatus != other.sensorsStatus) return false
            if (!values.contentEquals(other.values)) return false
            return true
        }

        override fun hashCode(): Int {
            var result = elapsedTime
            result = 31 * result + sensorsStatus.hashCode()
            result = 31 * result + values.contentHashCode()
            return result
        }
    }

    data class Stop(
        override val elapsedTime: Int,
        val sensorsStatus: SensorsAttachmentStatus,
        val result: Pair<SparseArray<MutableList<Double>>, SparseArray<MutableList<DoubleArray>>>
    ) : MeasurementStatus()


    data class Cancel(
        override val elapsedTime: Int,
        val cause: CancelCause = CancelCause.Forced
    ) : MeasurementStatus()
}

sealed class RecursiveData {
    data class ElapsedTime(val seconds: Int) : RecursiveData()
    data class SensorsAttachment(val status: SensorsAttachmentStatus) : RecursiveData()
    data class SignalStability(val status: SensorsSignalStability) : RecursiveData()
    data class Electroencephalography(val values: DoubleArray) : RecursiveData() {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Electroencephalography
            if (!values.contentEquals(other.values)) return false
            return true
        }

        override fun hashCode(): Int {
            return values.contentHashCode()
        }
    }
}

sealed class CancelCause {
    object Forced : CancelCause()
    data class Disconnected(val device: BluetoothDevice?) : CancelCause()
    data class Detached(val status: SensorsAttachmentStatus) : CancelCause()
    object Unstabilized : CancelCause()
    data class Error(val throwable: Throwable) : CancelCause()
}

