package omnifit.mindcare.training.headset.fx2

import android.annotation.SuppressLint
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.util.SparseArray
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observables.ConnectableObservable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import io.reactivex.functions.Function
import io.reactivex.subjects.PublishSubject
import omnifit.commons.core.BitCount
import omnifit.commons.core.doseNothing
import omnifit.mindcare.training.configuration.HeadsetConfiguration
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.headset.fx2.io.*
import timber.log.Timber
import java.io.IOException
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.SynchronousQueue
import kotlin.properties.Delegates


class NNFx2Impl : NNFx2, Frame.Parser {

    private var bluetoothSocket: BluetoothSocket? = null

    @Volatile
    private var delayConfirmMeasurableTimer: Timer? = null

    var reader: Reader? = null

    private var pcdCache = doubleArrayOf(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

    private var checkValues = intArrayOf(-1, -1, -1, -1, -1, -1, -1, -1)

    // 설정값에 따른 헤드셋 세팅
    private var isPowerSaveModeEnabled: Boolean = true
    private var signalStabilization: Int = 30

    // 원하는 위치에 맞게 지정이 되어야하는가???
    override fun init(context: Context) {
        if (HeadsetConfiguration.isRegistered(context)) {
            HeadsetConfiguration.get(context).also { configuration ->
                isPowerSaveModeEnabled = configuration.powerSaveMode
                signalStabilization = configuration.signalStabilization
            }.set(context)
        }
    }

    override fun getName(): String {
        if (bluetoothSocket == null) {
            throw RuntimeException("No bluetoothSocket yet.")
        }
        return bluetoothSocket!!.remoteDevice.name
    }

    override fun setBluetoothSocket(socket: BluetoothSocket) {
        bluetoothSocket = socket
        if (delayConfirmMeasurableTimer != null) {
            delayConfirmMeasurableTimer?.cancel()
            delayConfirmMeasurableTimer = null
        }
        Arrays.fill(pcdCache, 0.0)
        Arrays.fill(checkValues, -1)
        // 신호 안정화 처리
        try {
            write(
                byteArrayOf(
                    255.toByte(),
                    254.toByte(),
                    32,
                    7,
                    1,
                    0,
                    signalStabilization.toByte()
                )
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }
        reader = NNFx2Reader(NNFx2Frame(), this@NNFx2Impl)
        NNFx2Reader(NNFx2Frame(), this@NNFx2Impl).read(bluetoothSocket)
    }

    fun NNFx2Reader.read(socket: BluetoothSocket?) {
        read(socket?.inputStream)
    }

    private val transferDataQueue = SynchronousQueue<ByteArray>(true)
    private var dataTransferExecutor: ExecutorService? = null
    private var transferData: ByteArray? = null

    private fun write(source: ByteArray): Int {
        if (bluetoothSocket == null) {
            throw RuntimeException("No bluetoothSocket yet.")
        }
        transferData = source
        dataTransferExecutor = Executors.newSingleThreadExecutor()
        dataTransferExecutor?.execute {
            try {
                transferDataQueue.take()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            } finally {
                if (dataTransferExecutor != null) {
                    dataTransferExecutor?.shutdownNow()
                    dataTransferExecutor = null
                }
            }
        }

        val w = NNFx2Writer(bluetoothSocket!!.outputStream)
        return w.write(source)
    }

    override fun parse(source: ByteArray) {
        checkPowerMode(source)
        if (PowerMode.UNKNOWN != powerMode) {
            val op = OperationMode[source[FIELD_PPD].toUInt()]
            updateOperationMode(op)
            when (op) {
                OperationMode.WAITING -> onAfterParse(parseToWaitingMode(source))
                OperationMode.MEASURING -> onAfterParse(parseToMeasuringMode(source))
                OperationMode.CHARGING -> onAfterParse(parseToChargingMode(source))
            }
        }
    }

    // 바이트 원시데이터 파싱
    private fun parseToWaitingMode(source: ByteArray): NNFx2Data {
        val data = NNFx2Data(OperationMode.WAITING)
        // @formatter:off
        data.set(FIELD_OPERATION_MODE, OperationMode.WAITING.ordinal.toDouble())
        data.set(FIELD_WAIT_MODE_MAINTENANCE_TIME, source[FIELD_PUD_0].toUInt().toDouble())
        data.set(FIELD_PACKET_COUNT, source[FIELD_PC].toUInt().toDouble())
        when (data.get(FIELD_PACKET_COUNT).toInt()) {
            PCD_FIRMWARE_REVISION -> pcdCache[3] = source[FIELD_PCD].toUInt().toDouble()
            PCD_FIRMWARE_ID -> pcdCache[4] = source[FIELD_PCD].toUInt().toDouble()
            PCD_DEVICE_ID -> pcdCache[5] = source[FIELD_PCD].toUInt().toDouble()
        }
        data.set(FIELD_FIRMWARE_REVISION, pcdCache[3])
        data.set(FIELD_FIRMWARE_ID, pcdCache[4])
        data.set(FIELD_DEVICE_ID, pcdCache[5])
        data.set(FIELD_BATTERY_LEVEL, source[FIELD_PUD_1].toUInt().toDouble())
        data.set(
            FIELD_RX_DATA_RECEIVED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x40, BitCount.COUNT_6).toDouble()
        )
        data.set(
            FIELD_CH_1_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x20, BitCount.COUNT_5).toDouble()
        )
        data.set(
            FIELD_CH_2_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x10, BitCount.COUNT_4).toDouble()
        )
        data.set(
            FIELD_REF_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x08, BitCount.COUNT_3).toDouble()
        )
        // @formatter:on
        return data
    }

    private fun parseToMeasuringMode(source: ByteArray): NNFx2Data {
        val data = NNFx2Data(OperationMode.MEASURING)
        // @formatter:off
        data.set(FIELD_OPERATION_MODE, OperationMode.MEASURING.ordinal.toDouble())
        data.set(
            FIELD_HEARTBEAT_STATE,
            source[FIELD_PUD_0].toUIntWith(0x80, BitCount.COUNT_7).toDouble()
        )
        data.set(
            FIELD_SENSORS_ATTACHED_STATE,
            source[FIELD_PUD_0].toUIntWith(0x40, BitCount.COUNT_6).toDouble()
        )
        data.set(
            FIELD_EARLOBE_ELECTRODES_STATE,
            source[FIELD_PUD_0].toUIntWith(0x20, BitCount.COUNT_5).toDouble()
        )
        data.set(
            FIELD_LOW_BATTERY_STATE,
            source[FIELD_PUD_0].toUIntWith(0x10, BitCount.COUNT_4).toDouble()
        )
        data.set(
            FIELD_EYES_CLOSED_STATE,
            source[FIELD_PUD_0].toUIntWith(0x08, BitCount.COUNT_3).toDouble()
        )
        data.set(
            FIELD_PPG_SIGNAL_NORMALITY_STATE,
            source[FIELD_PUD_0].toUIntWith(0x04, BitCount.COUNT_2).toDouble()
        )
        data.set(
            FIELD_EEG_SIGNAL_STABILITY_STATE,
            source[FIELD_PUD_0].toUIntWith(0x02, BitCount.COUNT_1).toDouble()
        )
        data.set(FIELD_DATA_UPDATE_STATE, source[FIELD_PUD_0].toUIntWith(0x01).toDouble())
        data.set(FIELD_PACKET_COUNT, source[FIELD_PC].toUInt().toDouble())
        when (data.get(FIELD_PACKET_COUNT).toInt()) {
            PCD_EEG_STABILIZATION_VALUE -> pcdCache[0] = source[FIELD_PCD].toUInt().toDouble()
            PCD_BATTERY_LEVEL -> pcdCache[1] = source[FIELD_PCD].toUInt().toDouble()
            PCD_PPG_ABNORMAL_PULSE_COUNT -> pcdCache[2] = source[FIELD_PCD].toUInt().toDouble()
            PCD_FIRMWARE_REVISION -> pcdCache[3] = source[FIELD_PCD].toUInt().toDouble()
            PCD_FIRMWARE_ID -> pcdCache[4] = source[FIELD_PCD].toUInt().toDouble()
            PCD_DEVICE_ID -> pcdCache[5] = source[FIELD_PCD].toUInt().toDouble()
        }
        data.set(FIELD_EEG_SIGNAL_STABILITY_VALUE, pcdCache[0])
        data.set(FIELD_BATTERY_LEVEL, pcdCache[1])
        data.set(FIELD_PPG_ABNORMAL_PULSE_COUNT, pcdCache[2])
        data.set(FIELD_FIRMWARE_REVISION, pcdCache[3])
        data.set(FIELD_FIRMWARE_ID, pcdCache[4])
        data.set(FIELD_DEVICE_ID, pcdCache[5])
        data.set(FIELD_HEART_RATE, source[FIELD_PUD_1].toUInt().toDouble())
        data.set(FIELD_PACKET_CYCLE_DATA, source[FIELD_PCD].toUInt().toDouble())
        data.set(
            FIELD_RX_DATA_RECEIVED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x40, BitCount.COUNT_6).toDouble()
        )
        data.set(
            FIELD_CH_1_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x20, BitCount.COUNT_5).toDouble()
        )
        data.set(
            FIELD_CH_2_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x10, BitCount.COUNT_4).toDouble()
        )
        data.set(
            FIELD_REF_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x08, BitCount.COUNT_3).toDouble()
        )
        data.set(
            FIELD_PPG_AGC_DC_COMPLETED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x04, BitCount.COUNT_2).toDouble()
        )
        data.set(
            FIELD_PPG_AGC_AC_COMPLETED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x02, BitCount.COUNT_1).toDouble()
        )
        data.set(
            FIELD_EEG_L_WAVEFORM,
            (source[FIELD_CHANN_1_H].toUIntWith(0x7F) * 256 + source[FIELD_CHANN_1_L].toUInt()).toDouble()
        )
        data.set(
            FIELD_EEG_R_WAVEFORM,
            (source[FIELD_CHANN_2_H].toUIntWith(0x7F) * 256 + source[FIELD_CHANN_2_L].toUInt()).toDouble()
        )
        data.set(
            FIELD_EEG_POWER_SPECTRUM,
            ((source[FIELD_CHANN_3_H].toUIntWith(0x7F) * 256 + source[FIELD_CHANN_3_L].toUInt()) / 10.0)
        )
        data.set(
            FIELD_PPG_WAVEFORM,
            (source[FIELD_CHANN_4_H].toUInt() * 256 + source[FIELD_CHANN_4_L].toUInt()).toDouble()
        )
        data.set(
            FIELD_SDPPG_WAVEFORM,
            (source[FIELD_CHANN_5_H].toUInt() * 256 + source[FIELD_CHANN_5_L].toUInt()).toDouble()
        )
        data.set(
            FIELD_RR_INTERVAL,
            (source[FIELD_CHANN_6_H].toUInt() * 256 + source[FIELD_CHANN_6_L].toUInt()).toDouble()
        )
        // @formatter:off
        return data
    }

    private fun parseToChargingMode(source: ByteArray): NNFx2Data {
        val data = NNFx2Data(OperationMode.CHARGING)
        // @formatter:off
        data.set(FIELD_OPERATION_MODE, OperationMode.CHARGING.ordinal.toDouble())
        data.set(FIELD_CHARGING_PROGRESS_TIME, source[FIELD_PUD_0].toUInt().toDouble())
        data.set(FIELD_PACKET_COUNT, source[FIELD_PC].toUInt().toDouble())
        when (data.get(FIELD_PACKET_COUNT).toInt()) {
            PCD_FIRMWARE_REVISION -> pcdCache[3] = source[FIELD_PCD].toUInt().toDouble()
            PCD_FIRMWARE_ID -> pcdCache[4] = source[FIELD_PCD].toUInt().toDouble()
            PCD_DEVICE_ID -> pcdCache[5] = source[FIELD_PCD].toUInt().toDouble()
        }
        data.set(FIELD_FIRMWARE_REVISION, pcdCache[3])
        data.set(FIELD_FIRMWARE_ID, pcdCache[4])
        data.set(FIELD_DEVICE_ID, pcdCache[5])
        data.set(FIELD_CHARGED_STATE, source[FIELD_PUD_1].toUIntWith(0x01).toDouble())
        data.set(
            FIELD_RX_DATA_RECEIVED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x40, BitCount.COUNT_6).toDouble()
        )
        data.set(
            FIELD_CH_1_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x20, BitCount.COUNT_5).toDouble()
        )
        data.set(
            FIELD_CH_2_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x10, BitCount.COUNT_4).toDouble()
        )
        data.set(
            FIELD_REF_ELECTRODE_ATTACHED_STATE,
            source[FIELD_CRD_PUD_2_PCDT].toUIntWith(0x08, BitCount.COUNT_3).toDouble()
        )
        // @formatter:on
        return data
    }

    //------------------------------------------------------------------------------------------------------------------

    private fun onAfterParse(data: NNFx2Data) {
        checkBatteryLevel(data)

        checkSensorsAttached(data)

        checkCrdInverted(data)

        onBeforeModeChange(data)

        when (OperationMode[data.get(FIELD_OPERATION_MODE).toInt()]) {
            OperationMode.WAITING -> {
                if (PowerMode.BATTERY == powerMode) {
                    val time = data.get(FIELD_WAIT_MODE_MAINTENANCE_TIME).toInt()
                    if (!isPowerSaveModeEnabled && time < 30) {
                        try {
                            write(byteArrayOf(255.toByte(), 254.toByte(), 32, 6, 14, 3))
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }
            OperationMode.MEASURING -> {
                updateBrainWave(data)
                if (measuringFinalizer != null) updateMeasurementMode(data)
            }
            OperationMode.CHARGING -> doseNothing()
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    // 데이터 값 변화 체크
    private fun isValueChanged(cv: Int, iv: Int): Boolean {
        val bool = checkValues[cv] xor iv != 0
        if (bool) {
            checkValues[cv] = iv
        }
        return bool
    }

    // crd invert check
    private fun checkCrdInverted(data: NNFx2Data): Int {
        val invert = data.get(FIELD_RX_DATA_RECEIVED_STATE).toInt()
        if (isValueChanged(CHECK_VALUE_CRD_INVERTED, invert)) {
            if (dataTransferExecutor != null) {
                try {
                    transferDataQueue.put(transferData)
                } catch (e: InterruptedException) {
                    dataTransferExecutor?.shutdownNow()
                    dataTransferExecutor = null
                    e.printStackTrace()
                }

            }
        }
        return invert
    }

    // 이전모드 변경
    private fun onBeforeModeChange(data: NNFx2Data) {
        when (OperationMode[data.get(FIELD_OPERATION_MODE).toInt()]) {
            OperationMode.WAITING -> doseNothing()
            OperationMode.MEASURING -> doseNothing()
            OperationMode.CHARGING -> doseNothing()
        }
    }

    // 펌웨어 버전 체크
    private fun getFirmwareVersion(): String {
        return String.format(
            Locale.getDefault(),
            "D%dF%dR%d",
            pcdCache[5].toInt(),
            pcdCache[4].toInt(),
            pcdCache[3].toInt()
        )
    }

    // PPG 이상신호 체크
    val LIMIT_ABNORMAL_PULSE_COUNT = 5

    private var ppgAbnormalPulseCount: Int = 0

    private fun isAvailablePpgAbnormalPulseCount(data: NNFx2Data): Boolean {
        val count = data.get(FIELD_PPG_ABNORMAL_PULSE_COUNT).toInt()
        if (isValueChanged(CHECK_VALUE_PPG_ABNORMAL_PULSE_COUNT_CHANGED, count)) {
            ppgAbnormalPulseCount = count
        }
        return count <= LIMIT_ABNORMAL_PULSE_COUNT
    }

    // EEG 신호 안정화 체크
    private fun isEegSignalStabilized(data: NNFx2Data): Boolean {
        val state = data.get(FIELD_EEG_SIGNAL_STABILITY_STATE).toInt()
        return state == STATE_EEG_SIGNAL_STABILIZED
    }

    //------------------------------------------------------------------------------------------------------------------

    override fun isMeasuring(): Boolean {
        return status is MeasurementStatus.Start
                || status is MeasurementStatus.Measuring
    }

    @SuppressLint("CheckResult")
    override fun startMeasurement(
        owner: LifecycleOwner,
        limit: Int,
        eyesState: EyesStatus,
        onChange: (MeasurementStatus) -> Unit

    ) {
        Timber.d("--> 측정 종료 기준 시간 $limit 초")
        if (isMeasuring()) return
        // 측정 시 모드만 처리
        measuringMode(owner, limit, eyesState, onChange)
    }

    private var measuringFinalizer: Disposable? = null
    private var measuringTrigger: ConnectableObservable<NNFx2Data>? = null

    private fun measuringMode(
        owner: LifecycleOwner,
        limit: Int,
        eyesState: EyesStatus,
        onChange: (MeasurementStatus) -> Unit
    ) {
        var elapsedTime: Int = 0
        var signalStabilityStatus: SensorsSignalStability = SensorsSignalStability.UNSTABILIZED
        var sensorsAttachmentStatus: SensorsAttachmentStatus = SensorsAttachmentStatus.DETACHED

        fun NNFx2Data.eegValueOfEyesState(eyesState: EyesStatus): Double {
            return when (eyesState) {
//                EyesStatus.BOTH -> {
//                    if (this.get(FIELD_EYES_CLOSED_STATE).toInt() == STATE_EYES_CLOSED)
//                        this.get(FIELD_PACKET_CYCLE_DATA).toInt().and(0xF0).ushr(4).toDouble()
//                    else
//                        this.get(FIELD_PACKET_CYCLE_DATA).toInt().and(0x0F).toDouble()
//                }
                EyesStatus.CLOSED -> {
                    this.get(FIELD_PACKET_CYCLE_DATA).toInt().and(0xF0).ushr(4).toDouble()
                }
                else -> {
                    this.get(FIELD_PACKET_CYCLE_DATA).toInt().and(0x0F).toDouble()
                }
            }
        }

        measuringFinalizer = MEASUREMENT_MODE_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .skipWhile { p ->
                p.get(FIELD_DATA_UPDATE_STATE).toInt() != STATE_DATA_UPDATED
            }
            .publish()
            .also { trigger ->
                measuringTrigger = trigger
            }
            .compose { source ->
                Observable.mergeArray(
                    // 측정진행시간
                    source
                        .map { p ->
                            p.get(FIELD_PACKET_COUNT).toInt()
                        }
                        .scan { o, _ ->
                            o.inc()
                        }
                        .map { n ->
                            n / PACKET_PER_SECOND
                        }
                        .distinctUntilChanged { o, n ->
                            o == n
                        }
                        .map { n ->
                            RecursiveData.ElapsedTime(n)
                        },
                    // 신호부착상태
                    source
                        .map { p ->
                            if (checkSensorsAttached(p)) SensorsAttachmentStatus.ATTACHED else SensorsAttachmentStatus.DETACHED
                        }
                        .distinctUntilChanged { o, n ->
                            o == n
                        }
                        .map { s ->
                            RecursiveData.SensorsAttachment(s)
                        },
                    // 신호안정상태
                    source
                        .map { p ->
                            if (checkSensorsStableState(p)) SensorsSignalStability.STABILIZED else SensorsSignalStability.UNSTABILIZED
                        }
                        .distinctUntilChanged { o, n ->
                            o == n
                        }
                        .map { s ->
                            RecursiveData.SignalStability(s)
                        },
                    // EEG 데이터
                    Observable.zipArray(
                        // Zipping
                        Function<Array<Any>, DoubleArray> { a ->
                            a.asTypedArray<DoubleArray>().reduce { o, n -> o.plus(n) }
                        },
                        false,
                        Observable.bufferSize(),
                        // EEG 리듬파워
                        source
                            .filter { p ->
                                p.get(FIELD_PACKET_COUNT).toInt().let { n ->
                                    n in PCD_R_EEG_THETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE..PCD_EEG_BALANCE_INDICATOR_VALUE
                                            && n != PCD_RESERVED_1
                                            && n != PCD_RESERVED_2
                                }
                            }
                            .scanWith(
                                {
                                    DoubleArray(15).also {
                                        doubleArrayOf()
                                    }
                                },
                                { buffer, p ->
                                    //                                    Timber.i("--> EEG 리듬 : ${buffer.contentToString()}")
                                    buffer.apply {
                                        measureEEGBuffer(
                                            p.get(FIELD_PACKET_COUNT).toInt(),
                                            p.eegValueOfEyesState(eyesState)
                                        )
                                    }
                                }
                            )
                            .sample(
                                source.filter { p ->
                                    p.get(FIELD_DATA_UPDATE_STATE).toInt() == STATE_DATA_UPDATED
                                }
                            )
                            .filter { it.isNotEmpty() }
                            .map { buffer ->
                                //                                Timber.i("--> 2초 갱신 EEG 리듬 : ${buffer.contentToString()}")
//                                Timber.i("----------------------------------------------------------")
                                buffer
                            },
                        // EEG 파워스펙트럼
                        source
                            .filter { p ->
                                p.get(FIELD_DATA_UPDATE_STATE).toInt() != STATE_DATA_UPDATED
                            }
                            .map { p ->
                                p.get(FIELD_EEG_POWER_SPECTRUM)
                            }
                            .buffer(
                                source.filter { p ->
                                    p.get(FIELD_DATA_UPDATE_STATE).toInt() == STATE_DATA_UPDATED
                                }
                            )
                            .filter { it.isNotEmpty() }
                            .map { buffer ->
                                //                                Timber.i("--> 2초 갱신 EEG 파워 : ${it.toDoubleArray().contentToString()}")
                                buffer.take(256).toDoubleArray()
                            }

                    )
                        .filter { zip ->
                            //                            Timber.d("--> 2초 갱신 데이터 출력 ---------------------------------------------")
                            zip.size == EEG_VALUE_COUNT // 271
                        }
                        .map { zip ->
                            RecursiveData.Electroencephalography(zip)
                        }

                )
            }
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(Schedulers.io())
            ?.serialize()
            ?.doOnSubscribe {
                measuringTrigger?.connect()
            }
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doFinally {
                updateMeasurementStatus(
                    cancelCause?.let { cause ->
                        MeasurementStatus.Cancel(elapsedTime, cause)
                    } ?: MeasurementStatus.Stop(
                        elapsedTime,
                        sensorsAttachmentStatus,
                        Pair(electroencephalographyValueCache, powerSpectrumValueCache)
                    ),
                    onChange
                )
            }
            ?.bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            ?.subscribe(
                { r ->
                    when (r) {
                        is RecursiveData.ElapsedTime -> {
                            elapsedTime = r.seconds
                            if (elapsedTime == 0) initCacheValues()
                            updateMeasurementStatus(
                                MeasurementStatus.Start(
                                    elapsedTime,
                                    sensorsAttachmentStatus
                                ),
                                onChange
                            )
                        }
                        is RecursiveData.SensorsAttachment -> sensorsAttachmentStatus = r.status
                        is RecursiveData.SignalStability -> signalStabilityStatus = r.status
                        is RecursiveData.Electroencephalography -> {
                            r.values.toCaching()
                            updateMeasurementStatus(
                                MeasurementStatus.Measuring(
                                    elapsedTime,
                                    sensorsAttachmentStatus,
                                    signalStabilityStatus,
                                    when {
                                        sensorsAttachmentStatus != SensorsAttachmentStatus.ATTACHED -> DoubleArray(
                                            EEG_VALUE_COUNT
                                        ).apply { fill(-2.0) }
                                        signalStabilityStatus != SensorsSignalStability.STABILIZED -> DoubleArray(
                                            EEG_VALUE_COUNT
                                        ).apply { fill(-1.0) }
                                        else -> r.values
                                    }
                                ),
                                onChange
                            )
                            // 기준 시간(limit > 0)에 종료
                            if (limit in 1..elapsedTime) {
                                Timber.i("---> 측정 완료 : $limit")
                                stopMeasurement()
                            }
                        }
                    }
                },
                { e -> Timber.e(e) }
            )
    }

    private var cancelCause: CancelCause? = null

    override fun stopMeasurement(cause: CancelCause?) {
        cancelCause = cause
        measuringFinalizer?.dispose()
        measuringFinalizer = null

    }

    var status: MeasurementStatus =
        MeasurementStatus.Stop(
            0,
            SensorsAttachmentStatus.DETACHED,
            Pair(SparseArray(), SparseArray())
        )

    private inline fun updateMeasurementStatus(
        status: MeasurementStatus,
        onChange: (MeasurementStatus) -> Unit = {}
    ) {
        this.status = status
        onChange(status)
    }

    // 측정 데이터 처리
    private val powerSpectrumValueCache: SparseArray<MutableList<DoubleArray>> by lazy {
        SparseArray<MutableList<DoubleArray>>().initPowerSpectrumValueCache()
    }
    private val electroencephalographyValueCache: SparseArray<MutableList<Double>> by lazy {
        SparseArray<MutableList<Double>>().initElectroencephalographyValueCache()
    }

    private fun initCacheValues() {
        powerSpectrumValueCache.clear()
        powerSpectrumValueCache.initPowerSpectrumValueCache()
        electroencephalographyValueCache.clear()
        electroencephalographyValueCache.initElectroencephalographyValueCache()
    }

    private fun SparseArray<MutableList<DoubleArray>>.initPowerSpectrumValueCache() = apply {
        put(RESULT_ITEM_L_POWER_SPECTRUM, mutableListOf())
        put(RESULT_ITEM_R_POWER_SPECTRUM, mutableListOf())
    }

    private fun SparseArray<MutableList<Double>>.initElectroencephalographyValueCache() = apply {
        put(RESULT_ITEM_L_THETA, mutableListOf())
        put(RESULT_ITEM_R_THETA, mutableListOf())
        put(RESULT_ITEM_L_ALPHA, mutableListOf())
        put(RESULT_ITEM_R_ALPHA, mutableListOf())
        put(RESULT_ITEM_L_LBETA, mutableListOf())
        put(RESULT_ITEM_R_LBETA, mutableListOf())
        put(RESULT_ITEM_L_MBETA, mutableListOf())
        put(RESULT_ITEM_R_MBETA, mutableListOf())
        put(RESULT_ITEM_L_HBETA, mutableListOf())
        put(RESULT_ITEM_R_HBETA, mutableListOf())
        put(RESULT_ITEM_L_GAMMA, mutableListOf())
        put(RESULT_ITEM_R_GAMMA, mutableListOf())
        put(RESULT_ITEM_CONCENTRATION, mutableListOf())
        put(RESULT_ITEM_RELAXATION, mutableListOf())
        put(RESULT_ITEM_BALANCE, mutableListOf())
    }

    private fun DoubleArray.toCaching() {
        //@formatter:off
        powerSpectrumValueCache[RESULT_ITEM_L_POWER_SPECTRUM].add(
            splitPowerSpectrum(
                15,
                143
            )
        ) // L:128개
        powerSpectrumValueCache[RESULT_ITEM_R_POWER_SPECTRUM].add(
            splitPowerSpectrum(
                143,
                size
            )
        ) // R:128개

        electroencephalographyValueCache[RESULT_ITEM_L_THETA].add(this[RESULT_ITEM_L_THETA])
        electroencephalographyValueCache[RESULT_ITEM_R_THETA].add(this[RESULT_ITEM_R_THETA])
        electroencephalographyValueCache[RESULT_ITEM_L_ALPHA].add(this[RESULT_ITEM_L_ALPHA])
        electroencephalographyValueCache[RESULT_ITEM_R_ALPHA].add(this[RESULT_ITEM_R_ALPHA])
        electroencephalographyValueCache[RESULT_ITEM_L_LBETA].add(this[RESULT_ITEM_L_LBETA])
        electroencephalographyValueCache[RESULT_ITEM_R_LBETA].add(this[RESULT_ITEM_R_LBETA])
        electroencephalographyValueCache[RESULT_ITEM_L_MBETA].add(this[RESULT_ITEM_L_MBETA])
        electroencephalographyValueCache[RESULT_ITEM_R_MBETA].add(this[RESULT_ITEM_R_MBETA])
        electroencephalographyValueCache[RESULT_ITEM_L_HBETA].add(this[RESULT_ITEM_L_HBETA])
        electroencephalographyValueCache[RESULT_ITEM_R_HBETA].add(this[RESULT_ITEM_R_HBETA])
        electroencephalographyValueCache[RESULT_ITEM_L_GAMMA].add(this[RESULT_ITEM_L_GAMMA])
        electroencephalographyValueCache[RESULT_ITEM_R_GAMMA].add(this[RESULT_ITEM_R_GAMMA])
        electroencephalographyValueCache[RESULT_ITEM_CONCENTRATION].add(this[RESULT_ITEM_CONCENTRATION])
        electroencephalographyValueCache[RESULT_ITEM_RELAXATION].add(this[RESULT_ITEM_RELAXATION])
        electroencephalographyValueCache[RESULT_ITEM_BALANCE].add(this[RESULT_ITEM_BALANCE])
        //@formatter:on
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════════════════════════
    ║
    ║ 상태 변화 값
    ║
    ╚═══════════════════════════════════════════════════════════════════════════════════════════════*/

    // 측정데이터 확인
    private val MEASUREMENT_MODE_SUBJECT: PublishSubject<NNFx2Data> by lazy {
        PublishSubject.create<NNFx2Data>()
    }

    private val MEASUREMENT_MODE_SHARE: Observable<NNFx2Data> by lazy {
        MEASUREMENT_MODE_SUBJECT.share()
    }

    private fun updateMeasurementMode(data: NNFx2Data) {
        MEASUREMENT_MODE_SUBJECT.onNext(data)
    }

    //------------------------------------------------------------------------------------------------------------------

    // 파형그래프
    private val BRAIN_WAVE_SUBJECT: PublishSubject<NNFx2Data> by lazy {
        PublishSubject.create<NNFx2Data>()
    }

    private val BRAIN_WAVE_SHARE: Observable<NNFx2Data> by lazy {
        BRAIN_WAVE_SUBJECT.share()
    }

    private fun updateBrainWave(data: NNFx2Data) {
        BRAIN_WAVE_SUBJECT.onNext(data)
    }

    @SuppressLint("CheckResult")
    override fun observeBrainWaveChange(
        owner: LifecycleOwner,
        onChange: (Double) -> Unit
    ) {
        BRAIN_WAVE_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .map { p ->
                (p.get(FIELD_EEG_L_WAVEFORM).toInt() + p.get(FIELD_EEG_R_WAVEFORM)) / 2
            }
            .distinctUntilChanged { o, n ->
                o == n
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { s -> onChange(s) },
                { e -> Timber.e(e) }
            )
    }


    //------------------------------------------------------------------------------------------------------------------

    // 헤드셋 파워모드 확인
    private fun checkPowerMode(source: ByteArray) {
        val op = OperationMode[source[FIELD_PPD].toUInt()]
        if (OperationMode.WAITING == op) {
            val pcd = source[FIELD_PCD].toUInt()
            if (pcd == MeasurementEquipment.OCW_H11.deviceId()) updatePowerMode(PowerMode.ADAPTER) else updatePowerMode(
                PowerMode.BATTERY
            )
        } else if (OperationMode.MEASURING == op) {
            if (source[FIELD_PC].toUInt() == PCD_DEVICE_ID) {
                val pcd = source[FIELD_PCD].toUInt()
                when (pcd) {
                    MeasurementEquipment.NEURO_NICLE_FX2.deviceId() -> updatePowerMode(PowerMode.BATTERY)
                    MeasurementEquipment.OCW_H11.deviceId() -> updatePowerMode(PowerMode.ADAPTER)
                    else -> updatePowerMode(PowerMode.UNKNOWN)
                }
            }
        }
    }

    private var powerMode: PowerMode = PowerMode.UNKNOWN

    override fun powerMode(): PowerMode {
        return powerMode
    }

    private fun updatePowerMode(mode: PowerMode) {
        powerMode = mode
    }
    //------------------------------------------------------------------------------------------------------------------

    // 오페레이션모드 확인
    override fun operationMode(): OperationMode {
        return operationMode
    }

    private val OPERATION_MODE_SUBJECT: Subject<OperationMode> by lazy {
        BehaviorSubject
            .createDefault(OperationMode.WAITING)
            .toSerialized()
    }

    private val OPERATION_MODE_SHARE: Observable<OperationMode> by lazy {
        OPERATION_MODE_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    private var operationMode: OperationMode by Delegates.observable(OperationMode.WAITING) { _, o, n ->
        OPERATION_MODE_SUBJECT.onNext(n)
    }

    private fun updateOperationMode(mode: OperationMode) {
        operationMode = mode
    }

    @SuppressLint("CheckResult")
    override fun observeOperationModeChange(
        owner: LifecycleOwner,
        onChange: (OperationMode) -> Unit
    ) {
        OPERATION_MODE_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .distinctUntilChanged { o, n ->
                o == n
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { s ->
                    run {
                        //                        Timber.i("--> 디바이스 오퍼레이션 변화 : $s")
                        onChange(s)
                    }
                },
                { e -> Timber.e(e) }
            )
    }
    //------------------------------------------------------------------------------------------------------------------

    // 밧데리 잔량 확인
    private fun checkBatteryLevel(data: NNFx2Data) {
        updateBatteryLevel(data.get(FIELD_BATTERY_LEVEL).toInt())
    }

    override fun batteryLevel(): Int {
        return batteryLevel
    }

    private val BATTERY_LEVEL_SUBJECT: Subject<Int> by lazy {
        BehaviorSubject
            .createDefault(0)
            .toSerialized()
    }

    private val BATTERY_LEVEL_SHARE: Observable<Int> by lazy {
        BATTERY_LEVEL_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    private var batteryLevel: Int by Delegates.observable(0) { _, o, n ->
        BATTERY_LEVEL_SUBJECT.onNext(n)
    }

    private fun updateBatteryLevel(level: Int) {
        batteryLevel = level
    }

    @SuppressLint("CheckResult")
    override fun observeBatteryLevelChange(
        owner: LifecycleOwner,
        onChange: (Int) -> Unit
    ) {
        BATTERY_LEVEL_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .distinctUntilChanged { o, n ->
                o == n
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { s ->
                    run {
                        //                        Timber.i("--> 디바이스 밧데리 변화 : $s")
                        onChange(s)
                    }
                },
                { e -> Timber.e(e) }
            )
    }
    //------------------------------------------------------------------------------------------------------------------

    // 센서 부착 상태 확인
    private fun checkSensorsAttached(data: NNFx2Data): Boolean {
        val states = intArrayOf(
            data.get(FIELD_CH_1_ELECTRODE_ATTACHED_STATE).toInt(),
            data.get(FIELD_CH_2_ELECTRODE_ATTACHED_STATE).toInt(),
            data.get(FIELD_REF_ELECTRODE_ATTACHED_STATE).toInt()
        )

        val isSensorsAttached = Arrays.equals(
            states,
            intArrayOf(
                STATE_ELECTRODE_CH_1_ATTACHED,
                STATE_ELECTRODE_CH_2_ATTACHED,
                STATE_ELECTRODE_REF_ATTACHED
            )
        )

        if (isValueChanged(
                CHECK_VALUE_SENSORS_ATTACHED_STATE,
                if (isSensorsAttached) STATE_SENSORS_ATTACHED else STATE_SENSORS_DETACHED
            )
        ) {
            updateSensorsAttachmentStatus(if (isSensorsAttached) SensorsAttachmentStatus.ATTACHED else SensorsAttachmentStatus.DETACHED)
        }

        return isSensorsAttached
    }

    override fun isSensorsAttached(): Boolean {
        return sensorsAttachmentStatus == SensorsAttachmentStatus.ATTACHED
    }

    private val SENSORS_ATTACHMENT_STATUS_SUBJECT: Subject<SensorsAttachmentStatus> by lazy {
        BehaviorSubject
            .createDefault<SensorsAttachmentStatus>(SensorsAttachmentStatus.DETACHED)
            .toSerialized()
    }

    private val SENSORS_ATTACHMENT_STATUS_SHARE: Observable<SensorsAttachmentStatus> by lazy {
        SENSORS_ATTACHMENT_STATUS_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    private var sensorsAttachmentStatus: SensorsAttachmentStatus by Delegates.observable(
        SensorsAttachmentStatus.DETACHED
    ) { _, o, n ->
        SENSORS_ATTACHMENT_STATUS_SUBJECT.onNext(n)
    }

    @Synchronized
    private fun updateSensorsAttachmentStatus(status: SensorsAttachmentStatus) {
        Timber.i("---> updateSensorsAttachmentStatus : $status")
        sensorsAttachmentStatus = status
    }

    @SuppressLint("CheckResult")
    override fun observeSensorsAttachmentStatusChange(
        owner: LifecycleOwner,
        onChange: (SensorsAttachmentStatus) -> Unit
    ) {
        SENSORS_ATTACHMENT_STATUS_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .distinctUntilChanged { o, n ->
                o == n
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { s ->
                    run {
                        //                        Timber.i("--> 디바이스 부착상태 변화 : $s")
                        onChange(s)
                    }
                },
                { e -> Timber.e(e) }
            )
    }
    //------------------------------------------------------------------------------------------------------------------

    // 센서 신호안정화 체크
    private fun checkSensorsStableState(data: NNFx2Data): Boolean {
//        val isSensorsStabilized = isAvailablePpgAbnormalPulseCount(data) && isEegSignalStabilized(data)
        val isSensorsStabilized = isEegSignalStabilized(data)
        if (isValueChanged(
                CHECK_VALUE_SENSORS_STABILIZED_STATE,
                if (isSensorsStabilized) STATE_SENSORS_STABILIZED else STATE_SENSORS_UNSTABILIZED
            )
        ) {
            updateSensorsStableStatus(if (isSensorsStabilized) SensorsSignalStability.STABILIZED else SensorsSignalStability.UNSTABILIZED)
        } else {
            sensorsStableState = SensorsSignalStability.UNSTABILIZED
        }
        return isSensorsStabilized
    }

//    private val SENSORS_STABLE_STATUS_SUBJECT: Subject<SensorsSignalStability> by lazy {
//        BehaviorSubject
//            .createDefault<SensorsSignalStability>(SensorsSignalStability.UNSTABILIZED)
//            .toSerialized()
//    }
//
//    private val SENSORS_STABLE_STATUS_SHARE: Observable<SensorsSignalStability> by lazy {
//        SENSORS_STABLE_STATUS_SUBJECT
//            .subscribeOn(Schedulers.io())
//            .observeOn(Schedulers.io())
//    }

//    private var sensorsStableState: SensorsSignalStability by Delegates.observable(SensorsSignalStability.UNSTABILIZED) { _, o, n ->
//        SENSORS_STABLE_STATUS_SUBJECT.onNext(n)
//    }

    private var sensorsStableState: SensorsSignalStability = SensorsSignalStability.UNSTABILIZED

    @Synchronized
    private fun updateSensorsStableStatus(status: SensorsSignalStability) {
        sensorsStableState = status
    }

//    @SuppressLint("CheckResult")
//    fun observeSensorsStableStatusChange(
//        owner: LifecycleOwner,
//        onChange: (SensorsSignalStability) -> Unit
//    ) {
//        SENSORS_STABLE_STATUS_SHARE
//            .subscribeOn(Schedulers.io())
//            .observeOn(Schedulers.io())
//            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
//            .distinctUntilChanged { o, n ->
//                o == n
//            }
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(
//                { s ->
//                    run {
//                        //                        Timber.i("--> 디바이스 안정화 변화 : $s")
//                        onChange(s)
//                    }
//                },
//                { e -> Timber.e(e) }
//            )
//    }
}