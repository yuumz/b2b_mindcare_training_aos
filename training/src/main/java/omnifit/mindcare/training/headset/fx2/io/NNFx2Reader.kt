package omnifit.mindcare.training.headset.fx2.io

import timber.log.Timber
import java.io.Closeable
import java.io.IOException
import java.io.InputStream
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import java.nio.channels.Channels
import java.util.*
import kotlin.experimental.and

interface Reader : Closeable {

    @Throws(IOException::class)
    fun read(input: InputStream?)

    @Throws(IOException::class)
    fun read(b: ByteArray)
}

class NNFx2Reader(private val frame: Frame, private val parser: Frame.Parser): Reader {

    var frame_: Frame? = null
    var frameParser: Frame.Parser? = null

    private var inputStream: InputStream? = null

    init {
        frame_ = frame
        frameParser = parser
    }

    private fun isFoundFrameFlag(b: ByteArray): Boolean {
        return Arrays.equals(frame_?.flag, b)
    }

    @Throws(IOException::class)
    override fun read(ins: InputStream?) {
        inputStream = ins
        synchronized(this) {
            if (inputStream == null) {
                throw IOException("InputStream is closed.")
            }

//            Channels.newChannel(inputStream).use { rch ->
//                ByteBuffer.allocateDirect(DEFAULT_STREAM_BUFFER_SIZE).let { rbuf ->
//                    val chkv = byteArrayOf(-1, -2)
//                    val extbuf = ByteArray(frame.length)
//                    val chkbuf = ByteArray(frame.flag.size)
//                    val chks = extbuf.size + chkv.size
//                    var fromInclusive = EMPTY_INDEX
//                    var toExclusive = EMPTY_INDEX
//
//                    var ppc = EMPTY_INDEX
//
//                    fun rangeIndex(): Int {
//                        return rbuf.position() - chkv.size
//                    }
//
//                    fun initExtRange() {
//                        fromInclusive = EMPTY_INDEX
//                        toExclusive = EMPTY_INDEX
//                    }
//
//                    Timber.i("---> rch.isOpen : ${rch.isOpen}")
//                    while (rch.isOpen) {
//                        try {
//                            rch.read(rbuf)
//                            rbuf.flip()
//                            while (rbuf.remaining() >= chks) {
//                                rbuf[chkbuf]
//                                if (chkbuf.contentEquals(chkv)) {
//                                    if (fromInclusive == EMPTY_INDEX) {
//                                        fromInclusive = rangeIndex()
//                                        continue
//                                    }
//                                    if (toExclusive == EMPTY_INDEX) {
//                                        toExclusive = rangeIndex()
//                                        if ((toExclusive - fromInclusive) >= extbuf.size) {
//                                            rbuf.position(fromInclusive)
//                                            rbuf[extbuf]
//
//                                            extbuf.copyOf().let { p ->
//
//                                                //- 패킷 유실 여부 확인 코드 -///////////////////////////////////////////
//                                                val pc = p[4].toInt()
//                                                if (ppc != EMPTY_INDEX) {
//                                                    val _r = ppc - pc
//                                                    if (_r != COMPARE_VALUE_1 && _r != COMPARE_VALUE_2) {
//                                                        // TODO : 패킷 유실 처리.
//                                                        Timber.e("--> 유실 패킷 발생[$ppc]-[?]-[$pc]")
//                                                    }
//                                                }
//                                                ppc = pc
//                                                ////////////////////////////////////////////////////////////////////////
//
//                                                parser.parse(p)
//                                            }
//                                        } else rbuf.position(toExclusive)
//                                        initExtRange()
//                                    }
//                                } else rbuf.position(rbuf.position() - 1)
//                            }
//                            if (fromInclusive != EMPTY_INDEX) {
//                                rbuf.position(fromInclusive)
//                            }
//                            initExtRange()
//                            rbuf.compact()
//                        } catch (e: Exception) {
//                            Timber.e(e)
//                        }
//                    }
//                }
//            }

            Channels.newChannel(inputStream).use { rch ->
                ByteBuffer.allocateDirect(DEFAULT_STREAM_BUFFER_SIZE).let { rbuf ->
                    val extbuf = ByteArray(frame.length)
                    var chkbuf = ByteArray(frame.flag.size)

                    var isFoundFrameStart = false

                    var ppc = EMPTY_INDEX

                    while (inputStream != null) {
                        try {
                            rch.read(rbuf)
                            rbuf.flip()
                            while (rbuf.hasRemaining()) {
                                if (!isFoundFrameStart) {
                                    rbuf.get(chkbuf)
                                    isFoundFrameStart = isFoundFrameFlag(chkbuf)
                                    if (isFoundFrameStart) {
                                        val np = rbuf.position() - chkbuf.size
                                        rbuf.position(np)
                                        if (np > 0) {
                                            rbuf.compact()
                                            rbuf.flip()
                                        }
                                    } else {
                                        rbuf.position(rbuf.position() - 1)
                                    }
                                } else {
                                    rbuf.get(extbuf)
                                    chkbuf = extbuf.copyOf(chkbuf.size)
                                    isFoundFrameStart = isFoundFrameFlag(chkbuf)
                                    if (!isFoundFrameStart) {
                                        val np = rbuf.position() - extbuf.size
                                        rbuf.position(np)
                                        continue
                                    } else {
                                        var isFound: Boolean
                                        var index = chkbuf.size
                                        do {
                                            index = Arrays.binarySearch(extbuf, index, extbuf.size - 1, 0xFF.toByte())
                                            isFound = index > 0
                                            if (isFound) {
                                                if (isFoundFrameFlag(byteArrayOf(extbuf[index], extbuf[index + 1]))) {
                                                    val np = index
                                                    rbuf.position(np)
                                                    rbuf.compact()
                                                    rbuf.flip()
                                                    break
                                                } else {
                                                    index++
                                                }
                                            }
                                        } while (isFound)
                                        if (isFound) {
                                            continue
                                        }
                                    }

                                    //- 패킷 유실 여부 확인 코드 -////////////////////////////////////////////
                                    val pc = extbuf[4].toInt().and(0xFF).toByte()
                                    if (ppc != EMPTY_INDEX) {
                                        val r = ppc - pc
                                        if (r != COMPARE_VALUE_1 && r != COMPARE_VALUE_2) {
                                            // TODO : 패킷 유실 처리.
                                            Timber.e("--> 유실 패킷 발생[$ppc]-[?]-[$pc]")
                                        }
                                    }
                                    ppc = pc.toInt()
                                    ////////////////////////////////////////////////////////////////////

                                    frameParser!!.parse(extbuf)
                                }
                            }
                            rbuf.clear()
                        } catch (e: BufferUnderflowException) {
                            rbuf.compact()
                        }
                    }
                }
            }
        }
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray) {
        throw UnsupportedOperationException("Not supported")
    }

    @Throws(IOException::class)
    override fun close() {
        synchronized(this) {
            if (inputStream != null) {
                inputStream?.close()
                inputStream = null
            }
        }
    }

    // TODO : 2019.05.17 JHK 추가
    companion object {
        const val EMPTY_INDEX = -1
        const val COMPARE_VALUE_1 = -1
        const val COMPARE_VALUE_2 = 31
        const val DEFAULT_STREAM_BUFFER_SIZE = 1024
//        const val DEFAULT_FRAME_SIZE = 20
//        const val DEFAULT_FRAME_CHECK_SIZE = 2
    }
}