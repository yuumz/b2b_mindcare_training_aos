package omnifit.mindcare.training.headset.spp

import android.bluetooth.BluetoothDevice
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import omnifit.mindcare.training.headset.ConnectionStatus
import omnifit.mindcare.training.headset.ScanStatus

/**
 * @author hym@omnicns.com on 2019.04.23
 *
 * 블루투스 관련 인터페이스
 */
interface BTSpp {

    fun init(context: Context)

    fun startDiscovery(
        context: Context,
        onChange: (ScanStatus) -> Unit
    )

    fun stopDiscovery(): Boolean

    fun isConnected(): Boolean

    fun connectTo(
        deviceAddress: String,
        isAutoConnection: Boolean = false
    )

    fun connectTo(
        device: BluetoothDevice,
        isAutoConnection: Boolean = false
    )

    fun disconnectTo(): Boolean

    fun connectDeviceIfNeeded(context: Context)

    fun observeConnectionStatusChange(
        owner: LifecycleOwner,
        onChange: (ConnectionStatus) -> Unit
    )
}