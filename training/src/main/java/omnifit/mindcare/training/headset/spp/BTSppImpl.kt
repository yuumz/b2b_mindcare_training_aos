package omnifit.mindcare.training.headset.spp

import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import omnifit.mindcare.training.configuration.HeadsetConfiguration
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.jakewharton.rx.replayingShare
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import omnifit.mindcare.training.headset.*
import timber.log.Timber
import java.io.IOException
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.RejectedExecutionException
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

/**
 * @author hym@omnicns.com on 2019.04.23
 *
 * 블루투스 관련 구현 클래스
 */
class BTSppImpl : BTSpp {

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ init
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private lateinit var bluetoothAdapter: BluetoothAdapter

    override fun init(context: Context) {
        var bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ Discovery - start / stop
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */
    private val measurementEquipments = listOf(
        MeasurementEquipment.NEURO_NICLE_FX2,
        MeasurementEquipment.OCW_H11
    )

    fun discoveryFilter(bluetoothDevice: BluetoothDevice): Boolean {
        if (bluetoothDevice.name != null) {
            for (measurementEquipment in measurementEquipments) {
                if (bluetoothDevice.name == measurementEquipment.deviceName()) return true
            }
        }
        return false
    }

    override fun startDiscovery(
        context: Context,
        onChange: (ScanStatus) -> Unit
    ) {
        if (!bluetoothAdapter.isEnabled) return

        if (!revokeIfExistsRunningDiscovering()) {
            revokeIfExistsRunningAutoConnection()
        }

        context.registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(_context: Context, intent: Intent) {
                intent.action?.let { action ->
                    when (action) {
                        BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                            onChange(ScanStatus.Start)
//                            Timber.d("--> DISCOVERY Start($this)")
                        }
                        BluetoothDevice.ACTION_FOUND -> {
                            intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                                ?.let { foundDevice ->
                                    if (discoveryFilter(foundDevice)) {
//                                        Timber.d("--> Found DEVICE -> $foundDevice")
                                        onChange(ScanStatus.Found(foundDevice))
                                    }
                                }
                        }
                        BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                            onChange(ScanStatus.Finished)
                            context.unregisterReceiver(this)
//                            Timber.d("--> DISCOVERY Finished($this)")
                            return
                        }
                        else -> {
                            Timber.d("--> DISCOVERY 그외")
                        }
                    }
                }
            }
        }, IntentFilter().apply {
            addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
            addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
            addAction(BluetoothDevice.ACTION_FOUND)
            addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        })

        bluetoothAdapter.startDiscovery()
    }

    private fun revokeIfExistsRunningDiscovering(): Boolean {
        return if (bluetoothAdapter.isDiscovering) {
            stopDiscovery()
        } else false
    }

    private fun revokeIfExistsRunningAutoConnection() {
        if (autoConnection && (connectionStatus == ConnectionStatus.CONNECTING || connectionStatus == ConnectionStatus.CONNECT_FAILURE || connectionStatus == ConnectionStatus.POWER_OFF)
        ) {
            disconnectTo()
        }
    }

    override fun stopDiscovery(): Boolean {
        return bluetoothAdapter.cancelDiscovery()
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ Connect / DisConnect
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */
    private val sppUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")

    @Volatile
    private var bluetoothSocket: BluetoothSocket? = null

    private var autoConnection: Boolean = false

    override fun isConnected(): Boolean {
        return connectionStatus == ConnectionStatus.CONNECTED
    }

    override fun connectTo(
        deviceAddress: String,
        isAutoConnection: Boolean
    ) {
        var device: BluetoothDevice = bluetoothAdapter.getRemoteDevice(deviceAddress)
        connectTo(device, isAutoConnection)
    }

    override fun connectTo(
        device: BluetoothDevice,
        isAutoConnection: Boolean
    ) {
        autoConnection = isAutoConnection

        if (confirmConnectable(device)) {

            revokeIfExistsRunningDiscovering()

            revokeIfExistsDeferredConnectionJob()

            var service = Executors.newSingleThreadExecutor()
            try {
                service.execute {
                    try {
                        device.createInsecureRfcommSocketToServiceRecord(sppUUID).use { btSocket ->
                            Timber.e("--> BT SOC -> $bluetoothSocket , BT SOC -> $btSocket")
                            bluetoothSocket = btSocket
                            if (connectionStatus != ConnectionStatus.POWER_OFF) {
                                updateConnectionStatus(ConnectionStatus.CONNECTING)
                            }
                            bluetoothSocket?.connect()
                            updateConnectionStatus(ConnectionStatus.CONNECTED)
                            try {
                                socketOpened(bluetoothSocket)
                            } catch (e: IOException) {
                                if (connectionStatus != ConnectionStatus.FORCE_DISCONNECTED) {
                                    updateConnectionStatus(ConnectionStatus.DISCONNECTED)
                                }
                                e.printStackTrace()
                            }
                        }
                    } catch (e: IOException) {
                        if (connectionStatus != ConnectionStatus.FORCE_DISCONNECTED) {
                            val causePowerOff = e.message!!.contains("socket might closed or timeout")
                            updateConnectionStatus(if (causePowerOff) ConnectionStatus.POWER_OFF else ConnectionStatus.CONNECT_FAILURE)
                        }
                        e.printStackTrace()
                    } finally {
                        socketClosed(bluetoothSocket)
                        if (bluetoothSocket != null) {
                            bluetoothSocket = null
                        }
                        if (service != null) {
                            service.shutdown()
                            try {
                                if (!service.awaitTermination(500, TimeUnit.MILLISECONDS)) {
                                    service.shutdownNow()
                                }
                            } catch (e: InterruptedException) {
                                service.shutdownNow()
                            }

                            service = null
                        }
                        checkIfNeedRetry(device)
                    }
                }
            } catch (e: RejectedExecutionException) {
                service = null
                connectTo(device)
            }

        }
    }

    private fun confirmConnectable(device: BluetoothDevice): Boolean {
        if (bluetoothSocket != null) {
            val btDevice = bluetoothSocket!!.remoteDevice
            return btDevice != device && disconnectTo()
        }
        return true
    }

    private fun revokeIfExistsDeferredConnectionJob() {
        if (deferredConnectionJob != null) {
            deferredConnectionJob = null
        }
    }

    @Volatile
    private var deferredConnectionJob: Runnable? = null

    private fun checkIfNeedRetry(device: BluetoothDevice) {
        if ((connectionStatus == ConnectionStatus.DISCONNECTED || connectionStatus == ConnectionStatus.CONNECT_FAILURE)
            || connectionStatus == ConnectionStatus.POWER_OFF
            && autoConnection
        ) {
            if (bluetoothAdapter.isEnabled) {
                Timber.e("--> 즉시 연결 재시도 (${device.name} : ${device.address}), ADAPTER ENABLED: ${bluetoothAdapter.isEnabled}")
                connectTo(device, autoConnection)
            } else {
                Timber.e("--> 연결 재시도 보류 (${device.name} : ${device.address}), ADAPTER ENABLED: ${bluetoothAdapter.isEnabled}")
                deferredConnectionJob.let {
                    Timber.e("--> 지연된 연결 재시도 (${device.name} : ${device.address}), ADAPTER ENABLED: ${bluetoothAdapter.isEnabled}")
                    connectTo(device, autoConnection)
                }
            }
        }
    }

    override fun disconnectTo(): Boolean {
        updateConnectionStatus(ConnectionStatus.FORCE_DISCONNECTED)
        try {
            if (bluetoothSocket != null) {
                bluetoothSocket?.close()
                bluetoothSocket = null
            }
        } catch (e: IOException) {
            bluetoothSocket = null
            e.printStackTrace()
        }
        return true
    }

    override fun connectDeviceIfNeeded(context: Context) {
        if (HeadsetConfiguration.isRegistered(context)) {
            HeadsetConfiguration.get(context).also { configuration ->
                if (configuration.isNotEmpty()) {
                    if (connectionStatus == ConnectionStatus.DISCONNECTED || connectionStatus == ConnectionStatus.UNKNOWN) {
                        var device: BluetoothDevice = bluetoothAdapter.getRemoteDevice(configuration.headsetDevice.address)
                        connectTo(device, true)
                    }

                    var power: PowerMode = configuration.powerMode
                    if (power == PowerMode.UNKNOWN) {
                        configuration.powerMode = Headset.powerMode()
                        configuration.set(context)
                    }
                }
            }
        }
    }

    @Throws(IOException::class)
    fun socketOpened(socket: BluetoothSocket?) {
        if (socket != null)
            Headset.setBluetoothSocket(socket)
    }

    private fun socketClosed(socket: BluetoothSocket?) {
//        Timber.e("--> 소켓 닫기")
    }


    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ Observer
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */
    private val CONNECTION_STATUS_SUBJECT: Subject<ConnectionStatus> by lazy {
        BehaviorSubject
            .createDefault<ConnectionStatus>(ConnectionStatus.UNKNOWN)
            .toSerialized()
    }

    private val CONNECTION_STATUS_SHARE: Observable<ConnectionStatus> by lazy {
        CONNECTION_STATUS_SUBJECT
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    var connectionStatus: ConnectionStatus by Delegates.observable(ConnectionStatus.UNKNOWN) { _, o, n ->
        CONNECTION_STATUS_SUBJECT.onNext(n)
    }

    private fun updateConnectionStatus(status: ConnectionStatus) {
        connectionStatus = status
    }

    @SuppressLint("CheckResult")
    override fun observeConnectionStatusChange(
        owner: LifecycleOwner,
        onChange: (ConnectionStatus) -> Unit
    ) {
        CONNECTION_STATUS_SHARE
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
            .distinctUntilChanged { o, n ->
                o == n
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { s ->
                    run {
                        Timber.i("--> 디바이스 연결상태 변화 : $s")
                        onChange(s)
                    }
                },
                { e -> Timber.e(e) }
            )

    }
}