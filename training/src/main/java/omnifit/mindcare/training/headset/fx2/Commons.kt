package omnifit.mindcare.training.headset.fx2

import omnifit.commons.core.BitCount
import omnifit.commons.core.downToDecimalPlaces
import timber.log.Timber

fun Byte.toUInt(): Int {
    return toInt().and(0XFF)
}

fun Byte.toUIntWith(
    other: Int
): Int {
    val ot: Int = other
    return toInt().and(ot)
}

fun Byte.toUIntWith(
    other: Int,
    bitCount: BitCount = BitCount.COUNT_7
): Int {
    val ot: Int = other
    val bc: Int = bitCount.count
    return toInt().and(ot).ushr(bc)
}

@Suppress("UNCHECKED_CAST")
internal inline fun <reified T> Array<Any>.asTypedArray(): Array<T> = (toList() as List<T>).toTypedArray()

//fun DoubleArray.measureEEGBuffer(pc: Int, v: Double, dummy: Double) {
fun DoubleArray.measureEEGBuffer(pc: Int, v: Double) {
//    if(pc == PCD_EEG_CONCENTRATION_INDICATOR_VALUE) Timber.i("---> 집중도 데이터 폐안 : $v, 개안 : $dummy")
    when (pc) {
        PCD_R_EEG_THETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_THETA      ] = v
        PCD_L_EEG_THETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_THETA      ] = v
        PCD_R_EEG_ALPHA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_ALPHA      ] = v
        PCD_L_EEG_ALPHA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_ALPHA      ] = v
        PCD_R_EEG_LBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_LBETA      ] = v
        PCD_L_EEG_LBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_LBETA      ] = v
        PCD_R_EEG_MBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_MBETA      ] = v
        PCD_L_EEG_MBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_MBETA      ] = v
        PCD_R_EEG_HBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_HBETA      ] = v
        PCD_L_EEG_HBETA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_HBETA      ] = v
        PCD_R_EEG_GAMMA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_R_GAMMA      ] = v
        PCD_L_EEG_GAMMA_RHYTHM_ABSOLUTE_POWER_INDICATOR_VALUE -> this[RESULT_ITEM_L_GAMMA      ] = v
        PCD_EEG_CONCENTRATION_INDICATOR_VALUE                 -> this[RESULT_ITEM_CONCENTRATION] = v
        PCD_EEG_RELAXATION_INDICATOR_VALUE                    -> this[RESULT_ITEM_RELAXATION   ] = v
        else                                                  -> this[RESULT_ITEM_BALANCE      ] = v
    }
}

fun DoubleArray.splitPowerSpectrum(from: Int, to: Int): DoubleArray {
    val min = 9
    val max = 82
    return DoubleArray(to - from).apply {
        this@splitPowerSpectrum.average().let { avg ->
            if (avg >= 0.0) {
                fill(0.0)
//                System.arraycopy(this@splitPowerSpectrum, from, this, 0, 81) // 128 기준 1 ~ 81
                System.arraycopy(this@splitPowerSpectrum, from + min, this, min, max - min + 1)
            } else fill(avg)
        }
    }
}

fun Double.isAvailable(): Boolean {
    return this >= 0.0
}

fun DoubleArray.isAvailable(): Boolean {
    return average() >= 0.0
}

fun DoubleArray.computeSef90Hz(): Double {
    return run find@{
        if (isNotEmpty() && sum() > 0.0) {
            val sum = sum()
            var acc = this[0]
            forEachIndexed { hzi, pow ->
                if ((acc / sum * 100.0) < 90.0) acc += pow
                else return@find (hzi + 1).toDouble()
            }
        }
        return@find 0.0
    }
        .times(0.4883)
        .downToDecimalPlaces(4)
}

const val DELTA: Int = 0
const val THETA: Int = 1
const val ALPHA: Int = 2
const val L_BETA: Int = 3
const val M_BETA: Int = 4
const val H_BETA: Int = 5
const val GAMMA: Int = 6

fun DoubleArray.computeRhythmDistribution(): DoubleArray {
    // DELTA  : 0  ~ 4Hz 미만  (L/RPS_[1 ~ 8])
    // THETA  : 4  ~ 8Hz 미만  (L/RPS_[9  ~ 16])
    // ALPHA  : 8  ~ 12Hz 미만 (L/RPS_[17 ~ 24])
    // L-BETA : 12 ~ 15Hz 미만 (L/RPS_[25 ~ 30])
    // M-BETA : 15 ~ 20Hz 미만 (L/RPS_[31 ~ 40])
    // H-BETA : 20 ~ 30Hz 미만 (L/RPS_[41 ~ 61])
    // GAMMA  : 30 ~ 40Hz 미만 (L/RPS_[62 ~ 81])
    return DoubleArray(7) { 100.0.div(size) }.also { rhythm ->
        if (isNotEmpty() && sum() > 0.0) {
            // 0 ~ 4Hz 미만 [0 ~ 7](8)
            rhythm[DELTA] = DoubleArray(8).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 0, buff, 0, buff.size)
            }.sum()
            // 4 ~ 8Hz 미만 [8 ~ 15](8)
            rhythm[THETA] = DoubleArray(8).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 8, buff, 0, buff.size)
            }.sum()
            // 8 ~ 12Hz 미만 [16 ~ 23](8)
            rhythm[ALPHA] = DoubleArray(8).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 16, buff, 0, buff.size)
            }.sum()
            // 12 ~ 15Hz 미만 [24 ~ 29](6)
            rhythm[L_BETA] = DoubleArray(6).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 24, buff, 0, buff.size)
            }.sum()
            // 15 ~ 20Hz 미만 [30 ~ 39](10)
            rhythm[M_BETA] = DoubleArray(10).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 30, buff, 0, buff.size)
            }.sum()
            // 20 ~ 30Hz 미만 [40 ~ 60](21)
            rhythm[H_BETA] = DoubleArray(21).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 40, buff, 0, buff.size)
            }.sum()
            // 30 ~ 40Hz 미만 [61 ~ 80](20)
            rhythm[GAMMA] = DoubleArray(20).also { buff ->
                System.arraycopy(this@computeRhythmDistribution, 61, buff, 0, buff.size)
            }.sum()
        }
    }
}