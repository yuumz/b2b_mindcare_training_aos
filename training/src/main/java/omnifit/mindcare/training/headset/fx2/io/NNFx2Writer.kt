package omnifit.mindcare.training.headset.fx2.io

import java.io.Closeable
import java.io.IOException
import java.io.OutputStream
import java.nio.ByteBuffer
import java.nio.channels.Channels

interface Writer : Closeable {

    @Throws(IOException::class)
    fun write(b: ByteArray): Int
}

class NNFx2Writer(outs: OutputStream): Writer {

    var outputStream: OutputStream? = null

    init {
        outputStream = outs
    }

    override fun write(b: ByteArray): Int {
        synchronized(this) {
            if (outputStream == null) {
                throw IOException("OutputStream is closed.")
            }
            return Channels.newChannel(outputStream).write(ByteBuffer.wrap(b))
        }
    }

    override fun close() {
        synchronized(this) {
            if (outputStream != null) {
                outputStream?.close()
                outputStream = null
            }
        }
    }

}