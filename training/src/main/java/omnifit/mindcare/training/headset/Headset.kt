package omnifit.mindcare.training.headset

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import androidx.lifecycle.LifecycleOwner
import omnifit.mindcare.training.configuration.HeadsetConfiguration
import omnifit.mindcare.training.headset.fx2.MEASUREMENT_DURATION_INFINITE
import omnifit.mindcare.training.headset.spp.BTSppImpl
import omnifit.mindcare.training.headset.fx2.NNFx2
import omnifit.mindcare.training.headset.fx2.NNFx2Impl
import omnifit.mindcare.training.headset.spp.BTSpp
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import timber.log.Timber

object Headset : KodeinAware {

    override val kodein = Kodein.lazy {
        bind<BTSpp>() with instance(BTSppImpl())
        bind<NNFx2>() with instance(NNFx2Impl())
    }

    val btSpp: BTSpp by kodein.instance()

    val nnfX2: NNFx2 by kodein.instance()

    fun init(context: Context) {
        btSpp.init(context)
        nnfX2.init(context)
    }

    fun startDiscovery(
        context: Context,
        onChange: (ScanStatus) -> Unit
    ) {
        btSpp.startDiscovery(context = context, onChange = onChange)
    }

    fun stopDiscovery() {
        btSpp.stopDiscovery()
    }

    fun isConnected(): Boolean {
        return btSpp.isConnected()
    }

    fun connectTo(
        deviceAddress: String
    ) {
        btSpp.connectTo(deviceAddress = deviceAddress, isAutoConnection = true)
    }

    fun connectTo(device: BluetoothDevice) {
        btSpp.connectTo(device = device, isAutoConnection = true)
    }

    fun disconnectTo() {
        btSpp.disconnectTo()
    }

    fun connectDeviceIfNeeded(context: Context) {
        btSpp.connectDeviceIfNeeded(context = context)
    }

    @SuppressLint("CheckResult")
    fun observeConnectStatusChange(
        owner: LifecycleOwner,
        onChange: (ConnectionStatus) -> Unit
    ) {
        btSpp.observeConnectionStatusChange(owner = owner, onChange = onChange)
    }

    fun setBluetoothSocket(socket: BluetoothSocket) {
        nnfX2.setBluetoothSocket(socket)
    }

    fun close(
        on: () -> Unit
    ) {
        disconnectTo()
    }

    fun isMeasuring(): Boolean {
        return nnfX2.isMeasuring()
    }

    fun startMeasurement(
        owner: LifecycleOwner,
        limit: Int = MEASUREMENT_DURATION_INFINITE,
        eyesState: EyesStatus = EyesStatus.CLOSED,
        onChange: (MeasurementStatus) -> Unit = {}
    ) {
        nnfX2.startMeasurement(owner = owner, limit = limit, eyesState = eyesState, onChange = onChange)
    }

    fun stopMeasurement(cause: CancelCause? = null): Boolean {
        if (isMeasuring()) {
            nnfX2.stopMeasurement(cause = cause)
            return true
        }
        return false
    }

    fun powerMode(): PowerMode {
        return nnfX2.powerMode()
    }

    fun operationMode(): OperationMode {
        return nnfX2.operationMode()
    }

    @SuppressLint("CheckResult")
    fun observeOperationModeChange(
        owner: LifecycleOwner,
        onChange: (OperationMode) -> Unit
    ) {
        nnfX2.observeOperationModeChange(owner = owner, onChange = onChange)
    }


    @SuppressLint("CheckResult")
    fun observeBatteryLevelChange(
        owner: LifecycleOwner,
        onChange: (Int) -> Unit
    ) {
        nnfX2.observeBatteryLevelChange(owner = owner, onChange = onChange)
    }

    fun isSensorsAttached(): Boolean {
        return nnfX2.isSensorsAttached()
    }

    @SuppressLint("CheckResult")
    fun observeSensorsAttachmentStatusChange(
        owner: LifecycleOwner,
        onChange: (SensorsAttachmentStatus) -> Unit
    ) {
        nnfX2.observeSensorsAttachmentStatusChange(owner = owner, onChange = onChange)
    }

    fun observeBrainWaveChange(
        owner: LifecycleOwner,
        onChange: (Double) -> Unit
    ) {
        nnfX2.observeBrainWaveChange(owner = owner, onChange = onChange)
    }
}