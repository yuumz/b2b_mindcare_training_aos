package omnifit.mindcare.training.headset.fx2.io

import omnifit.mindcare.training.headset.OperationMode
import omnifit.mindcare.training.headset.fx2.FIELD_CHARGING_PROGRESS_TIME
import omnifit.mindcare.training.headset.fx2.FIELD_RR_INTERVAL
import omnifit.mindcare.training.headset.fx2.FIELD_WAIT_MODE_MAINTENANCE_TIME

interface Data {

    fun get(field: Int): Double

    fun set(field: Int, value: Double)
}

class NNFx2Data(op: OperationMode) : Data {

    /*
    ┌───────────────────────────────────────────────────────────────────────────────────────────
    │
    │
    │
    └───────────────────────────────────────────────────────────────────────────────────────────*/

    private var values: DoubleArray = doubleArrayOf()

    init {
        values = when (op) {
            OperationMode.WAITING -> DoubleArray(FIELD_WAIT_MODE_MAINTENANCE_TIME + 1)
            OperationMode.MEASURING -> DoubleArray(FIELD_RR_INTERVAL + 1)
            OperationMode.CHARGING -> DoubleArray(FIELD_CHARGING_PROGRESS_TIME + 1)
        }
    }

    override fun get(field: Int): Double {
        return values[field]
    }

    override fun set(field: Int, value: Double) {
        values[field] = value
    }
}





