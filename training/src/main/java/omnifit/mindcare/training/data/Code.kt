package omnifit.mindcare.training.data

import com.squareup.moshi.Json
import java.util.*

data class Code(
    // @formatter:off
    @Json(name = "cd")        var signature: String = "",
    @Json(name = "cdNm")      var label: String = "",
    @Json(name = "cdNmEn")    var labelEn: String = "",
    @Json(name = "cdXpln")    var explanation: String = "",
    @Json(name = "cdOrd")     var order: Int = -1,
    @Json(name = "mappingCd") var indicator: Int = -1,
    @Json(name = "prntCd")    var groupSignature: String = ""
    // @formatter:on
)