package omnifit.mindcare.training.data

import android.util.SparseArray
import com.google.common.primitives.Doubles
import omnifit.commons.core.*
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.mindcare.training.headset.fx2.*
import org.joda.time.DateTime
import java.util.*

data class Electroencephalography(
    var id: String = "", // yyyyMMddHHmmss
    var begin: Date = Date(),
    var end: Date = Date(),
    var targetSignature: String = "", // 측정데이터의 대상 ID
    var lThetaIndicatorValues: String = "[]",
    var rThetaIndicatorValues: String = "[]",
    var lAlphaIndicatorValues: String = "[]",
    var rAlphaIndicatorValues: String = "[]",
    var lLBetaIndicatorValues: String = "[]",
    var rLBetaIndicatorValues: String = "[]",
    var lMBetaIndicatorValues: String = "[]",
    var rMBetaIndicatorValues: String = "[]",
    var lHBetaIndicatorValues: String = "[]",
    var rHBetaIndicatorValues: String = "[]",
    var lGammaIndicatorValues: String = "[]",
    var rGammaIndicatorValues: String = "[]",
    var concentrationIndicatorValues: String = "[]",
    var relaxationIndicatorValues: String = "[]",
    var balanceIndicatorValues: String = "[]",
    var spectrumValues: String = "[[]]",
    var lSpectrumValues: String = "[[]]",
    var rSpectrumValues: String = "[[]]"
) {

    companion object {
        fun create(
            elapsedTime: Int,
            electroencephalography: SparseArray<MutableList<Double>>,
            spectrum: SparseArray<MutableList<DoubleArray>>,
            onChange: (Electroencephalography?) -> Unit = {}
        ) = with(Electroencephalography()) {
            DateTime.now().let { end ->
                end.minusSeconds(elapsedTime).let { begin ->
                    this.id = begin.toString(DTP_YYYYMMDDHHMMSS)
                    this.begin = begin.toDate()
                    this.end = end.toDate()
                }
            }

            //@formatter:off
            this.lThetaIndicatorValues        = electroencephalography[RESULT_ITEM_L_THETA      ].toDoubleArray().toJson()
            this.rThetaIndicatorValues        = electroencephalography[RESULT_ITEM_R_THETA      ].toDoubleArray().toJson()
            this.lAlphaIndicatorValues        = electroencephalography[RESULT_ITEM_L_ALPHA      ].toDoubleArray().toJson()
            this.rAlphaIndicatorValues        = electroencephalography[RESULT_ITEM_R_ALPHA      ].toDoubleArray().toJson()
            this.lLBetaIndicatorValues        = electroencephalography[RESULT_ITEM_L_LBETA      ].toDoubleArray().toJson()
            this.rLBetaIndicatorValues        = electroencephalography[RESULT_ITEM_R_LBETA      ].toDoubleArray().toJson()
            this.lMBetaIndicatorValues        = electroencephalography[RESULT_ITEM_L_MBETA      ].toDoubleArray().toJson()
            this.rMBetaIndicatorValues        = electroencephalography[RESULT_ITEM_R_MBETA      ].toDoubleArray().toJson()
            this.lHBetaIndicatorValues        = electroencephalography[RESULT_ITEM_L_HBETA      ].toDoubleArray().toJson()
            this.rHBetaIndicatorValues        = electroencephalography[RESULT_ITEM_R_HBETA      ].toDoubleArray().toJson()
            this.lGammaIndicatorValues        = electroencephalography[RESULT_ITEM_L_GAMMA      ].toDoubleArray().toJson()
            this.rGammaIndicatorValues        = electroencephalography[RESULT_ITEM_R_GAMMA      ].toDoubleArray().toJson()
            this.concentrationIndicatorValues = electroencephalography[RESULT_ITEM_CONCENTRATION].toDoubleArray().toJson()
            this.relaxationIndicatorValues    = electroencephalography[RESULT_ITEM_RELAXATION   ].toDoubleArray().toJson()
            this.balanceIndicatorValues       = electroencephalography[RESULT_ITEM_BALANCE      ].toDoubleArray().toJson()
            this.lSpectrumValues              = spectrum[RESULT_ITEM_L_POWER_SPECTRUM].toTypedArray().toJson()
            this.rSpectrumValues              = spectrum[RESULT_ITEM_R_POWER_SPECTRUM].toTypedArray().toJson()
            //@formatter:on

            this.spectrumValues = spectrum[RESULT_ITEM_L_POWER_SPECTRUM].toTypedArray().plus(spectrum[RESULT_ITEM_R_POWER_SPECTRUM].toTypedArray()).toJson()
            onChange(this)
        }
    }

    fun thetaIndicatorValue(): Double = lThetaIndicatorValues.computeRhythmIndicatorValueWith(rThetaIndicatorValues)
    fun alphaIndicatorValue(): Double = lAlphaIndicatorValues.computeRhythmIndicatorValueWith(rAlphaIndicatorValues)
    fun lbetaIndicatorValue(): Double = lLBetaIndicatorValues.computeRhythmIndicatorValueWith(rLBetaIndicatorValues)
    fun mbetaIndicatorValue(): Double = lMBetaIndicatorValues.computeRhythmIndicatorValueWith(rMBetaIndicatorValues)
    fun lHBetaIndicatorValue(): Double = lHBetaIndicatorValues.computeRhythmIndicatorValue()
    fun hBetaIndicatorValue(): Double = lHBetaIndicatorValues.computeRhythmIndicatorValueWith(rHBetaIndicatorValues)
    fun gammaIndicatorValue(): Double = lGammaIndicatorValues.computeRhythmIndicatorValueWith(rGammaIndicatorValues)
    fun concentrationIndicatorValue(): Double = concentrationIndicatorValues.computeRhythmIndicatorValue()
    fun relaxationIndicatorValue(): Double = relaxationIndicatorValues.computeRhythmIndicatorValue()
    fun balanceIndicatorValue(): Double = balanceIndicatorValues.computeRhythmIndicatorValue()
    fun sef90HzValues(): DoubleArray = spectrumValues.computeSef90HzValues()
    fun sef90Hz(): Double = spectrumValues.computeSef90Hz()
    fun lSef90HzValues(): DoubleArray = lSpectrumValues.computeSef90HzValues()
    fun lSef90Hz(): Double = lSpectrumValues.computeSef90Hz()
    fun rSef90HzValues(): DoubleArray = rSpectrumValues.computeSef90HzValues()
    fun rSef90Hz(): Double = rSpectrumValues.computeSef90Hz()

    fun brainStressIndicator(): BrainStressIndicator =
        BrainStressIndicator.of(AnalysisAlgorithm.convertIndicatorValueTo5pValue(brainStressIndicatorValue()))
    fun brainStressIndicatorValue(): Double = hBetaIndicatorValue()

    fun brainActivityIndicator(): BrainActivityIndicator = BrainActivityIndicator.of(brainActivityIndicatorValue())
    fun brainActivityIndicatorValue(): Double = AnalysisAlgorithm.transformSef90HzTo5pValue(sef90Hz())

    fun concentrationIndicator(): ConcentrationIndicator =
        ConcentrationIndicator.of(AnalysisAlgorithm.convertIndicatorValueTo5pValue(concentrationIndicatorValue()))
    fun concentrationTypeValue(): Int = concentrationIndicatorValues.computeConcentrationType()
    fun concentrationTypeIndicator(): ConcentrationTypeIndicator =
        ConcentrationTypeIndicator.of(concentrationTypeValue())

    fun relaxationIndicator(): RelaxationIndicator =
        RelaxationIndicator.of(AnalysisAlgorithm.convertIndicatorValueTo5pValue(relaxationIndicatorValue()))

    fun balanceIndicator(): BalanceIndicator =
        BalanceIndicator.of(AnalysisAlgorithm.convertIndicatorValueTo5pValue(balanceIndicatorValue()))

    fun rhythmDistribution(): DoubleArray = lSpectrumValues.computeRhythmDistribution()

    private fun String.computeRhythmIndicatorValueWith(jsonArray: String): Double {
        return arrayOf(
            doubleArrayOf().fromJson(this),
            doubleArrayOf().fromJson(jsonArray)
        )
            .columnAverage()
            .filter { v ->
                v.isAvailable()
            }
            .run {
                if (isNotEmpty()) {
                    average()
                        .roundToDecimalPlaces()
                } else 0.0
            }
    }

    private fun String.computeRhythmIndicatorValue(): Double {
        return doubleArrayOf()
            .fromJson(this)
            .filter { v ->
                v.isAvailable()
            }
            .run {
                if (isNotEmpty()) {
                    average()
                        .roundToDecimalPlaces()
                } else 0.0
            }
    }

    private fun String.computeSef90HzValues(): DoubleArray {
        return arrayOf(doubleArrayOf())
            .fromJson(this)
            .filter { v ->
                v.isAvailable()
            }
            .toTypedArray()
            .map { spectrum ->
                spectrum.computeSef90Hz()
            }
            .toDoubleArray()
    }

    private fun String.computeSef90Hz(): Double {
        return arrayOf(doubleArrayOf())
            .fromJson(this)
            .filter { v ->
                v.isAvailable()
            }
            .toTypedArray()
            .columnAverage()
            .computeSef90Hz()
    }

    private fun String.computeConcentrationType(): Int {
        return AnalysisAlgorithm.computeConcentrationType(
            doubleArrayOf()
                .fromJson(this)
                .filter { it >= 0.0 }
                .toDoubleArray()
        )
    }

    private fun String.computeRhythmDistribution(): DoubleArray {
        return arrayOf(doubleArrayOf())
            .fromJson(this)
            .filter { v ->
                v.isAvailable()
            }
            .toTypedArray()
            .columnAverage()
            .computeRhythmDistribution()
    }
}