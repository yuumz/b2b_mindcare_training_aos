package omnifit.mindcare.training.data

import com.squareup.moshi.Json

data class GameContent(
    // @formatter:off
    @Json(name = "fitBrainSeq")   var sequence: Int = -1,
    @Json(name = "appNm")         var title: String = "",
    @Json(name = "smry")          var explanation: String = "",
    @Json(name = "ord")           var order: Int = 0,
    @Json(name = "useYn")         var useContents: String = "",
    @Json(name = "regDt")         var registDate: String = "",
    @Json(name = "updDt")         var updateDate: String = "",
    @Json(name = "iconFileSeq")   var iconFileNo: Int = -1,
    @Json(name = "imgFileSeq")    var imgFileNo: Int = -1,
    @Json(name = "gameFileSeq")   var gameFileNo: Int = -1,
    @Json(name = "imgFile")       var imageFile: ContentFileInfo? = null,
    @Json(name = "iconFileKo")    var iconFile: ContentFileInfo? = null,
    @Json(name = "gameFile")      var gameFile: ContentFileInfo? = null
    // @formatter:on
)