package omnifit.mindcare.training

import android.graphics.drawable.Drawable
import android.text.Spanned
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.AnimRes
import androidx.core.text.parseAsHtml
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.commitNow
import androidx.lifecycle.LifecycleOwner
import omnifit.mindcare.training.common.MEASUREMENT_ITEM_BRAIN_ACTIVITY
import omnifit.mindcare.training.common.MEASUREMENT_ITEM_BRAIN_STRESS
import omnifit.mindcare.training.common.MEASUREMENT_ITEM_CONCENTRATION
import omnifit.mindcare.training.common.MeasurementSimpleLevel
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.screen.UIFrame
import omnifit.mindcare.training.screen.UIScreen
import omnifit.mindcare.training.view.BrainWave4Visualizer
import omnifit.mindcare.training.view.BrainWave4Real
import omnifit.mindcare.training.view.HeadsetStatusVisualizer
import org.jetbrains.anko.AnkoContext

fun UIFrame.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = supportFragmentManager.commitNow(allowStateLoss = true) {
    setCustomAnimations(enter, exit)
    replace(R.id.frame_container, screen, screen::class.java.simpleName)
}.run { true }

fun UIScreen.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = (requireActivity() as? UIFrame)?.screenTo(screen, enter, exit) ?: false

fun AnkoContext<Fragment>.screenTo(
    screen: UIScreen,
    @AnimRes enter: Int = android.R.anim.fade_in,
    @AnimRes exit: Int = android.R.anim.fade_out
) = (owner as? UIScreen)?.screenTo(screen, enter, exit) ?: false

fun UIFrame.identifier(
    name: String,
    defType: String,
    defPackage: String
): Int = resources.getIdentifier(name, defType, defPackage)

fun ProgressBar.deviceConnectChangeObserver(owner: LifecycleOwner) {
    Headset.observeConnectStatusChange(owner) { connectionStatus ->
        isVisible = when (connectionStatus) {
            ConnectionStatus.CONNECTED, ConnectionStatus.FORCE_DISCONNECTED -> false
            else -> true
        }
    }
}

fun TextView.batteryLevelChangeObserver(owner: LifecycleOwner) {
    Headset.observeConnectStatusChange(owner) { connectionStatus ->
        when (connectionStatus) {
            ConnectionStatus.CONNECTED -> {
                Headset.observeBatteryLevelChange(owner) { level ->
                    when (Headset.powerMode()) {
                        PowerMode.ADAPTER -> text = context.getString(R.string.main_screen_010)
                        PowerMode.BATTERY -> Headset.observeOperationModeChange(owner) { mode ->
                            text = if (mode == OperationMode.CHARGING) context.getString(R.string.main_screen_020)
                            else "$level%"
                        }
                        else -> text = "-------"
                    }
                }
            }
            else -> {
                text = "-------"
            }
        }
    }
}

fun HeadsetStatusVisualizer.bindSensorsAttachmentStatusChangeObserver(owner: LifecycleOwner) {
    Headset.observeSensorsAttachmentStatusChange(owner) { status ->
        when (status) {
            SensorsAttachmentStatus.ATTACHED -> start()
            else -> stop()
        }
    }
    isVisible = DebugOptions.HEADSET_STATUS_VISUALIZER_VISIBLE
}

fun BrainWave4Real.bindBrainWaveChangeObserver(owner: LifecycleOwner) {
    Headset.observeBrainWaveChange(owner) { wave ->
        putLineData(wave)
    }
}

fun BrainWave4Visualizer.bindBrainWaveChangeObserver(owner: LifecycleOwner) {
    Headset.observeSensorsAttachmentStatusChange(owner) { status ->
        when (status) {
            SensorsAttachmentStatus.ATTACHED -> start()
            else -> stop()
        }
    }
    isVisible = DebugOptions.HEADSET_STATUS_VISUALIZER_VISIBLE
}

inline fun Boolean.whenTrue(block: () -> Unit): Boolean {
    if (this) block()
    return this
}

fun ImageView.into(drawable: Drawable?) {
    setImageDrawable(drawable)
}

fun String.replaceNewLineAsSpanned(): Spanned {
    return replace("\\n", "<br>").parseAsHtml()
}

fun String.replaceNewLineAsSpace(): String {
    return replace("\\n", " ")
}

fun String.replaceNewLineAs(): String {
    return replace("\n", "")
}

fun ImageView.measurementItemGraphPinBg(level: MeasurementSimpleLevel) {
    when (level) {
        MeasurementSimpleLevel.GOOD -> setBackgroundResource(R.drawable.img_bg_pin_01_01)
        MeasurementSimpleLevel.NORMAL -> setBackgroundResource(R.drawable.img_bg_pin_01_02)
        else -> setBackgroundResource(R.drawable.img_bg_pin_01_03)
    }
}

fun ImageView.measurementItemGraphPin(item: Int) {
    when (item) {
        MEASUREMENT_ITEM_BRAIN_STRESS -> setBackgroundResource(R.drawable.ic_pin_01_01)
        MEASUREMENT_ITEM_CONCENTRATION -> setBackgroundResource(R.drawable.ic_pin_01_02)
        MEASUREMENT_ITEM_BRAIN_ACTIVITY -> setBackgroundResource(R.drawable.ic_pin_01_03)
        else -> setBackgroundResource(R.drawable.ic_pin_01_04)
    }
}

fun measurementItemLabel(item: Int): Array<Int> {
    return when (item) {
        MEASUREMENT_ITEM_BRAIN_STRESS, MEASUREMENT_ITEM_CONCENTRATION -> {
            arrayOf(
                R.string.default_level_01,
                R.string.default_level_02,
                R.string.default_level_03,
                R.string.default_level_04,
                R.string.default_level_05
            )
        }
        MEASUREMENT_ITEM_BRAIN_ACTIVITY -> {
            arrayOf(
                R.string.brain_activity_level_01,
                R.string.brain_activity_level_02,
                R.string.brain_activity_level_03,
                R.string.brain_activity_level_04,
                R.string.brain_activity_level_05
            )
        }
        else -> {
            arrayOf(
                R.string.brain_balance_level_05,
                R.string.brain_balance_level_04,
                R.string.brain_balance_level_03,
                R.string.brain_balance_level_02,
                R.string.brain_balance_level_01
            )
        }
    }
}