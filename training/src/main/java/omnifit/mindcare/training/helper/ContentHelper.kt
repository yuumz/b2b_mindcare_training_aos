package omnifit.mindcare.training.helper

import android.content.ContentResolver
import android.content.Context
import android.content.res.AssetFileDescriptor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.provider.MediaStore
import android.util.SparseArray
import androidx.annotation.RawRes
import at.favre.lib.dali.Dali
import com.squareup.moshi.Types
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.mindcare.training.common.*
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.core.json.JsonConverters
import omnifit.commons.core.secondsToMillis
import omnifit.mindcare.training.BuildConfig
import omnifit.mindcare.training.data.*
import org.apache.commons.io.FilenameUtils
import org.jetbrains.anko.collections.asSequence
import timber.log.Timber
import kotlin.math.roundToInt
import kotlin.properties.Delegates

object ContentHelper {

    @Suppress("CheckResult")
    fun requireBaseData(
        context: Context,
        on: (Float) -> Unit
    ) {

        context.assets.open("json/code.json").use { stream ->

            ByteArray(stream.available()).also { buffer ->
                stream.read(buffer)

            }.let { buffer ->
                ContentDataHelper.codeList = JsonConverters.moshi()
                    .adapter<List<CodeGroup>>(Types.newParameterizedType(List::class.java, CodeGroup::class.java))
                    .fromJson(String(buffer))

            }
        }
        on(35.0f)

        context.assets.open("json/playback.json").use { stream ->

            ByteArray(stream.available()).also { buffer ->
                stream.read(buffer)

            }.let { buffer ->
                ContentDataHelper.playbackList = JsonConverters.moshi()
                    .adapter<List<PlaybackContent>>(
                        Types.newParameterizedType(
                            List::class.java,
                            PlaybackContent::class.java
                        )
                    )
                    .fromJson(String(buffer))

            }
        }
        on(70.0f)

        context.assets.open("json/game.json").use { stream ->

            ByteArray(stream.available()).also { buffer ->
                stream.read(buffer)

            }.let { buffer ->
                ContentDataHelper.gameList = JsonConverters.moshi()
                    .adapter<List<GameContent>>(Types.newParameterizedType(List::class.java, GameContent::class.java))
                    .fromJson(String(buffer))

            }
        }
        on(100.0f)
    }

    @Suppress("CheckResult")
    fun initLoad(
        context: Context,
        onLoad: (Int) -> Unit
    ) {
        clearCaches()
        // 게임
        ContentDataHelper.findGameListAll()
            .let { gc ->
                ContentDataHelper.findGameListAllAsFlowable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .map { gc ->
                        GAME_LIST_CACHE.put(
                            gc.sequence,
                            GameSource(
                                sequence = gc.sequence,
                                thumbnail = gc.getDrawableFromImage(context),
                                sourceTitle = gc.title,
                                explanation = gc.explanation,
                                order = gc.order
                            )
                        )
                    }
                    .scan(0) { count, _ ->
                        count.inc()
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { count ->
                            count.toFloat().div(gc.size.toFloat()).times(100.0f).roundToInt().let(onLoad)
                        },
                        { e -> Timber.e(e) },
                        { onLoad(101) }
                    )
            }
        // 명상 힐링
//        var meditationContentOrder: Int = 0
        ContentDataHelper.findPlaybackListAll()
            .sumBy { pc ->
                pc.audioContentList?.count() ?: 0
            }
            .let { totalCount ->
                ContentDataHelper.findPlaybackListAllAsFlowable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .flatMapIterable { pc ->
                        pc.getNonIntroContentFilteredIterable()
                    }
                    .map { (pc, ac) ->
                        when (ac.groupSignature) {
                            SIGNATURE_MGC_HEALING_MUSIC -> {
                                ac.putToHealingMusicBlurDrawableCache(context)
                                HEALING_MUSIC_PLAYLIST_CACHE.put(
                                    ac.sequence,
                                    AudioPlayer.AudioSource(
                                        sequence = ac.sequence,
                                        thumbnail = ac.getDrawableFromImage(context),
                                        albumTitle = pc.title,
                                        sourceTitle = ac.title,
                                        explanation = ac.explanation,
                                        source = ac.getAudioSourceUri(),
                                        order = ac.order
                                    )
                                )
                            }
                            else -> {
                                MEDITATION_PLAYLIST_CACHE.put(
                                    ac.sequence,
                                    MeditationSource(
                                        sequence = ac.sequence,
                                        thumbnail = when (pc.utsSignature) {
                                            SIGNATURE_UTS_MEDITATION -> ac.getThumbnailBitmapFromVideo(context)
                                            else -> ac.getThumbnailBitmapFromImage(context)
                                        },
                                        introSource = pc.getIntroContentSource(context),
                                        albumTitle = pc.title,
                                        sourceTitle = ac.title,
                                        explanation = ac.explanation,
                                        playTime = ac.getDuration(),
                                        eyeType = if (pc.utsSignature == SIGNATURE_UTS_MEDITATION) 1 else 0,
                                        source = when (pc.utsSignature) {
                                            SIGNATURE_UTS_MEDITATION -> ac.getAudioSourceUri()
                                            else -> {
                                                AudioPlayer.AudioSource(
                                                    sequence = ac.sequence,
                                                    thumbnail = ac.getDrawableFromImage(context),
                                                    albumTitle = pc.title,
                                                    sourceTitle = ac.title,
                                                    explanation = ac.explanation,
                                                    source = ac.getAudioSourceUri(),
                                                    isLooping = pc.utsSignature == SIGNATURE_UTS_MEDITATION_TRAINING,
                                                    order = ac.order
                                                )
                                            }
                                        },
                                        utsSignature = pc.utsSignature,
                                        micSignature = pc.micSignature,
                                        useEffectSound = pc.utsSignature == SIGNATURE_UTS_MEDITATION_TRAINING,
                                        order = meditationSourceOrder++
                                    )
                                )
//                                specificKeyFrom(pc).let { key ->
//                                    MEDITATION_CATEGORY_CACHE[key]?.let { mc ->
//                                        mc.meditationSources.add(createMeditationSource(context, Pair(pc, ac)))
//                                    } ?: run {
//                                        MEDITATION_CATEGORY_CACHE.put(
//                                            key,
//                                            MeditationCategory(
//                                                sequence = key,
//                                                thumbnail = pc.getDrawableFromImage(context),
//                                                background = pc.getBlurBackgroundDrawable(context),
//                                                title = pc.title,
//                                                explanation = pc.explanation
//                                            ).apply {
//                                                meditationSources.add(createMeditationSource(context, Pair(pc, ac)))
//                                            }
//                                        )
//                                    }
//                                }
                            }
                        }
                    }
                    .scan(0) { count, _ ->
                        count.inc()
                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                        { count ->
                            count.toFloat().div(totalCount.toFloat()).times(101.0f).roundToInt().let(onLoad)
                        },
                        { e -> Timber.e(e) },
                        { onLoad(102) }
                    )
            }
    }

    fun meditationPlaylist(): List<MeditationSource> {
        return MEDITATION_PLAYLIST_CACHE
            .asSequence()
            .sortedBy { ms ->
                ms.order
            }
            .toList()
    }

    fun healingMusicPlaylist(): List<AudioPlayer.AudioSource> {
        return HEALING_MUSIC_PLAYLIST_CACHE.asSequence().toList()
    }

    fun gamelist(): List<GameSource> {
        return GAME_LIST_CACHE.asSequence().toList()
    }

    private val MEDITATION_CATEGORY_CACHE: SparseArray<MeditationCategory> = SparseArray()
    private val MEDITATION_PLAYLIST_CACHE: SparseArray<MeditationSource> = SparseArray()
    private val HEALING_MUSIC_BLUR_DRAWABLE_CACHE: SparseArray<Drawable> = SparseArray()
    private val HEALING_MUSIC_PLAYLIST_CACHE: SparseArray<AudioPlayer.AudioSource> = SparseArray()
    private val GAME_LIST_CACHE: SparseArray<GameSource> = SparseArray()

    private var specificKey: Int by Delegates.observable(0) { _, o, n ->
        if (o != n) {
            meditationSourceOrder = 0
        }
    }

    private var meditationSourceOrder: Int = 0

    /**
     * 같은 카테고리 그룹에 속하면서 앨범이 나뉘어져 공통 키를 구할때 정렬기준 1번에 맞춰 키를 고정
     * ex: sequence:94, order:1 -> 94 - (1 - 1) = 94
     *     sequence:95, order:2 -> 95 - (2 - 1) = 94
     */
    private fun specificKeyFrom(pc: PlaybackContent): Int {
        return pc.sequence - (pc.order - 1)
    }

    private fun createMeditationSource(
        context: Context,
        src: Pair<PlaybackContent, AudioContent>
    ): MeditationSource {
        return src.let { (pc, ac) ->
            specificKey = specificKeyFrom(pc)
            MeditationSource(
                sequence = ac.sequence,
                thumbnail = when (pc.utsSignature) {
                    SIGNATURE_UTS_MEDITATION -> ac.getThumbnailBitmapFromVideo(context)
                    else                     -> ac.getThumbnailBitmapFromImage(context)
                },
//                introSource = pc.getIntroContentSource(),
                categorySequence = specificKey,
                albumTitle = pc.title,
                sourceTitle = ac.title,
                explanation = ac.explanation,
                playTime = ac.getDuration(),
                eyeType = if (pc.utsSignature == SIGNATURE_UTS_MEDITATION) 1 else 0,
                source = when (pc.utsSignature) {
                    SIGNATURE_UTS_MEDITATION -> ac.getAudioSourceUri()
                    else                     -> {
                        AudioPlayer.AudioSource(
                            sequence = ac.sequence,
                            thumbnail = ac.getDrawableFromImage(context),
                            albumTitle = pc.title,
                            sourceTitle = ac.title,
                            explanation = ac.explanation,
                            source = ac.getAudioSourceUri(),
                            isLooping = pc.utsSignature == SIGNATURE_UTS_MEDITATION_TRAINING,
                            order = ac.order
                        )
                    }
                },
                utsSignature = pc.utsSignature,
                micSignature = pc.micSignature,
                useEffectSound = pc.utsSignature == SIGNATURE_UTS_MEDITATION_TRAINING,
                order = meditationSourceOrder++
            )
        }
    }

    private fun GameContent.getGameSourceUri(): Uri {
        return gameFile?.getContentSourceUri() ?: Uri.EMPTY
    }

    private fun GameContent.getDrawableFromImage(context: Context): Drawable? {
        var bitmap =
            BitmapFactory.decodeStream(imageFile?.getContentSourceToFileDescriptor(context)?.createInputStream())
        return BitmapDrawable(context.resources, bitmap)
    }

    private fun PlaybackContent.getNonIntroContentFilteredIterable(): Iterable<Pair<PlaybackContent, AudioContent>> {
        return audioContentList
            ?.filter { ac ->
                !ac.introducable
            }
            ?.map { ac ->
                Pair(this, ac)
            }
            ?: mutableListOf()
    }

    private fun AudioContent.putToHealingMusicBlurDrawableCache(context: Context) {
        HEALING_MUSIC_BLUR_DRAWABLE_CACHE.put(
            sequence,
            Dali.create(context)
                .load(getThumbnailBitmapFromImage(context))
                .blurRadius(25)
                .skipCache()
                .get()
        )
    }

    private fun AudioContent.getDrawableFromImage(context: Context): Drawable? {
        var bitmap =
            BitmapFactory.decodeStream(imageFile?.getContentSourceToFileDescriptor(context)?.createInputStream())
        return BitmapDrawable(context.resources, bitmap)
    }

    private fun AudioContent.getAudioSourceUri(): Uri {
        return audioFile?.getContentSourceUri() ?: Uri.EMPTY
    }

    private fun AudioContent.getThumbnailBitmapFromVideo(context: Context): Bitmap? {
        return with(MediaMetadataRetriever()) {
            try {
                audioFile?.let { cfi ->
                    if (cfi.filSignature != SIGNATURE_FIL_VIDEO) {
                        throw IllegalArgumentException("Thumbnails can only be extracted from video files.")
                    }

                    setDataSource(context, cfi.getContentSourceUri())
                    getFrameAtTime(1L)
                }
            } catch (e: IllegalArgumentException) {
                null
            } finally {
                release()
            }
        }
    }

    private fun AudioContent.getThumbnailBitmapFromImage(context: Context): Bitmap? {
        return imageFile?.let { cfi ->
            try {
                if (cfi.filSignature != SIGNATURE_FIL_IMAGE) {
                    throw IllegalArgumentException("Thumbnails can only be extracted from image files.")
                }

                BitmapFactory.decodeStream(cfi.getContentSourceToFileDescriptor(context).createInputStream())
            } catch (e: Exception) {
                e.printStackTrace()
                null
            }
        }
    }

    private fun PlaybackContent.getIntroContentSource(context: Context): AudioPlayer.AudioSource? {
        return try {
            audioContentList
                ?.first { ac ->
                    ac.introducable
                }
                ?.let { ac ->
                    AudioPlayer.AudioSource(
                        sequence = ac.sequence,
                        thumbnail = ac.getDrawableFromImage(context),
                        albumTitle = title,
                        sourceTitle = ac.title,
                        explanation = ac.explanation,
                        source = ac.getAudioSourceUri(),
                        order = ac.order
                    )
                }
        } catch (e: NoSuchElementException) {
            null
        }
    }

    private fun AudioContent.getDuration(): Long {
        return audioFile?.duration?.secondsToMillis() ?: 0L
    }

//    private fun PlaybackContent.getDrawableFromImage(context: Context): Drawable? {
//        var bitmap =
//            BitmapFactory.decodeStream(imageFileInfo?.getContentSourceToFileDescriptor(context)?.createInputStream())
//        return BitmapDrawable(context.resources, bitmap)
//    }
//
//    private fun PlaybackContent.getBlurBackgroundDrawable(context: Context): Drawable? {
//        return getThumbnailBitmapFromImage(context)?.let { bmp ->
//            Dali.create(context)
//                .load(bmp)
//                .blurRadius(25)
//                .skipCache()
//                .get()
//        }
//    }
//
//    private fun PlaybackContent.getThumbnailBitmapFromImage(context: Context): Bitmap? {
//        return imageFileInfo?.let { cfi ->
//            try {
//                if (cfi.filSignature != SIGNATURE_FIL_IMAGE) {
//                    throw IllegalArgumentException("Thumbnails can only be extracted from image files.")
//                }
//
//                BitmapFactory.decodeStream(cfi.getContentSourceToFileDescriptor(context).createInputStream())
//            } catch (e: Exception) {
//                e.printStackTrace()
//                null
//            }
//        }
//    }

    private fun clearCaches() {
        MEDITATION_PLAYLIST_CACHE.clear()
        HEALING_MUSIC_BLUR_DRAWABLE_CACHE.clear()
        HEALING_MUSIC_PLAYLIST_CACHE.clear()
        GAME_LIST_CACHE.clear()
    }
}

fun durationOf(
    context: Context,
    @RawRes src: Int
): Long {
    return with(MediaMetadataRetriever()) {
        setDataSource(context, Uri.parse("${ContentResolver.SCHEME_ANDROID_RESOURCE}://${context.packageName}/$src"))
        try {
            extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
        }
        finally {
            release()
        }
    }
}

fun ContentFileInfo.getContentSourceToFileDescriptor(context: Context): AssetFileDescriptor {
    return context.assets.openFd("${assetDirectoryName(downloadUrl)}/${FilenameUtils.getName(downloadUrl)}")
}

fun ContentFileInfo.getContentSourceUri(): Uri {
    var fileName = assetDirectoryName(downloadUrl) + "_" + name
    Timber.e("--> content : $fileName")
    return Uri.parse("android.resource://${BuildConfig.APPLICATION_ID}/raw/$fileName")
}

private fun assetDirectoryName(filename: String): String {
    return when (FilenameUtils.getExtension(filename)) {
        "mp3" -> "audio"
        "mp4" -> "video"
        else -> "image"
    }
}

data class MeditationCategory(
    val sequence: Int = -1,
    val thumbnail: Any? = null,
    val background: Any? = null,
    val title: String = "",
    val explanation: String = "",
    val meditationSources: MutableList<MeditationSource> = mutableListOf()
)

data class MeditationSource(
    val sequence: Int = -1,
    val thumbnail: Bitmap? = null,
    val introSource: AudioPlayer.AudioSource? = null,
    val categorySequence: Int = -1,
    val albumTitle: String = "",
    val sourceTitle: String = "",
    val explanation: String = "",
    val playTime: Long = 0L,
    val eyeType: Int = -1,
    val source: Any? = null,
    val utsSignature: String = "",
    val micSignature: String = "",
    var useEffectSound: Boolean = false,
    var isLooping: Boolean = false,
    var order: Int = 0
)

data class GameSource(
    val sequence: Int = -1,
    val thumbnail: Drawable? = null,
    val sourceTitle: String = "",
    val explanation: String = "",
    var order: Int = 0
)