package omnifit.mindcare.training.helper

import android.content.Context
import android.util.SparseArray
import omnifit.commons.core.millisToFormatString
import omnifit.commons.core.realm.save
import omnifit.mindcare.training.common.SIGNATURE_MIC_BRAIN_STRESS
import omnifit.mindcare.training.configuration.HealingMusicConfiguration
import omnifit.mindcare.training.configuration.MeasurementConfiguration
import omnifit.mindcare.training.data.Electroencephalography
import omnifit.mindcare.training.configuration.MeditationsConfiguration
import org.jetbrains.anko.doAsync
import timber.log.Timber

object ContentResultHelper {

    /**
     * 뇌파측정 결과 생성
     */
    fun createMeasurementResult(
        context: Context,
        elapsedTime: Int,
        gathered: Pair<SparseArray<MutableList<Double>>, SparseArray<MutableList<DoubleArray>>>,
        cb: (MeasurementConfiguration?) -> Unit
    ) {
        doAsync {
            gathered.let { (electroencephalography, spectrum) ->
                Electroencephalography.create(elapsedTime, electroencephalography, spectrum) { e ->
                    MeasurementConfiguration.create(e!!) { ms ->
                        ms!!.save(context).let {
                            Timber.d("--> $ms")
                            cb(ms)
                        }
                    }
                }
            }
        }
    }

    /**
     * 명상 결과 생성
     */
    fun createMeditationResult(
        context: Context,
        elapsedTime: Int,
        sequence: Int,
        micSignature: String,
        deepMeditationCount: Int,
        rewardPoint: Int,
        usedProgramTitle: String,
        gathered: Pair<SparseArray<MutableList<Double>>, SparseArray<MutableList<DoubleArray>>>,
        cb: (MeditationsConfiguration?) -> Unit
    ) {
        val checkTimeBegin: Long = System.currentTimeMillis()
        doAsync {
            gathered.let { (electroencephalography, spectrum) ->
                Electroencephalography.create(elapsedTime, electroencephalography, spectrum) { e ->
                    var checkTimeBegin2: Long = System.currentTimeMillis()
                    Timber.d("--> 명상 EEG 데이터 생성 소요 시간 : ${(checkTimeBegin2 - checkTimeBegin).millisToFormatString("mm:ss:SSS")}")
                    MeditationsConfiguration.create(
                        elapsedTime,
                        sequence,
                        micSignature,
                        deepMeditationCount,
                        rewardPoint,
                        usedProgramTitle,
                        e!!
                    ) { mt ->
                        var checkTimeBegin3: Long = System.currentTimeMillis()
                        Timber.d("--> 명상 데이터 생성 소요 시간 : ${(checkTimeBegin3 - checkTimeBegin2).millisToFormatString("mm:ss:SSS")}")
                        mt!!.save(context).let {
                            cb(mt)
                        }
                    }
                }
            }
        }
    }

    fun createHealingMusicResult(
        context: Context,
        elapsedTime: Int,
        sequence: Int,
        micSignature: String,
        deepHealingCount: Int,
        usedProgramTitle: String,
        gathered: Pair<SparseArray<MutableList<Double>>, SparseArray<MutableList<DoubleArray>>>,
        cb: (HealingMusicConfiguration?) -> Unit
    ) {
        val checkTimeBegin: Long = System.currentTimeMillis()
        doAsync {
            gathered.let { (electroencephalography, spectrum) ->
                Electroencephalography.create(elapsedTime, electroencephalography, spectrum) { e ->
                    var checkTimeBegin2: Long = System.currentTimeMillis()
                    Timber.d("--> 힐링뮤직 EEG 데이터 생성 소요 시간 : ${(checkTimeBegin2 - checkTimeBegin).millisToFormatString("mm:ss:SSS")}")
                    HealingMusicConfiguration.create(
                        elapsedTime,
                        sequence,
                        micSignature,
                        deepHealingCount,
                        usedProgramTitle,
                        e!!
                    ) { hm ->
                        var checkTimeBegin3: Long = System.currentTimeMillis()
                        Timber.d("--> 힐링뮤직 데이터 생성 소요 시간 : ${(checkTimeBegin3 - checkTimeBegin2).millisToFormatString("mm:ss:SSS")}")
                        hm!!.save(context).let {
                            cb(hm)
                        }
                    }
                }
            }
        }
    }
}