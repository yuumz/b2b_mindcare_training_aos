package omnifit.mindcare.training.helper

import io.reactivex.Flowable
import io.reactivex.rxkotlin.toFlowable
import omnifit.mindcare.training.common.SIGNATURE_UTS_HEALING_MUSIC
import omnifit.mindcare.training.common.SIGNATURE_UTS_MEDITATION
import omnifit.mindcare.training.common.SIGNATURE_UTS_MEDITATION_MBSR
import omnifit.mindcare.training.common.SIGNATURE_UTS_MEDITATION_TRAINING
import omnifit.mindcare.training.data.*

object ContentDataHelper {

    var codeList: List<CodeGroup>? = listOf()
    var playbackList: List<PlaybackContent>? = listOf()
    var gameList: List<GameContent>? = listOf()

    fun findPlaybackListAll(): List<PlaybackContent> {
        return playbackList!!
            .filter { pc ->
                pc.utsSignature in arrayOf(
                    SIGNATURE_UTS_MEDITATION,
                    SIGNATURE_UTS_MEDITATION_MBSR,
                    SIGNATURE_UTS_MEDITATION_TRAINING,
                    SIGNATURE_UTS_HEALING_MUSIC
                )

            }
            .sortedBy { pc ->
                pc.order
            }
    }

    fun findPlaybackListAllAsFlowable(): Flowable<PlaybackContent> {
        return playbackList!!
            .sortedBy { pc ->
                pc.order
            }
            .toFlowable()
    }

    fun findPlaybackContent(micSignature: String, sequence: Int): PlaybackContent {
        playbackList!!.forEach { playbackContent ->
            if(playbackContent.micSignature == micSignature) {
                playbackContent.audioContentList!!.forEach { audio ->
                    if (audio.sequence == sequence) {
                        return playbackContent
                    }
                }
            }
        }
        return PlaybackContent()
    }

    fun findAudioContent(micSignature: String, sequence: Int): AudioContent {
        playbackList!!.forEach { playbackContent ->
            if(playbackContent.micSignature == micSignature) {
                playbackContent.audioContentList!!.forEach { audio ->
                    if (audio.sequence == sequence) {
                        return audio
                    }
                }
            }
        }
        return AudioContent()
    }

    fun findGameListAll(): List<GameContent> {
        return gameList!!
            .sortedBy { pc ->
                pc.order
            }
    }

    fun findGameListAllAsFlowable(): Flowable<GameContent> {
        return gameList!!
            .sortedBy { pc ->
                pc.order
            }
            .toFlowable()
    }

    fun findConcentrationType(signature: String): Code {
        codeList!!.forEach { codeGroup ->
            codeGroup.codeList!!.forEach { code ->
                if (code.groupSignature == "CET000" && code.signature == signature) {
                    return code
                }
            }
        }
        return Code()
    }
}