package omnifit.mindcare.training.common

interface OnFocusListenable {
    fun onWindowFocusChanged(hasFocus: Boolean)
}