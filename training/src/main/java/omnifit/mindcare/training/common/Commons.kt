package omnifit.mindcare.training.common

import android.content.Context
import android.graphics.Color
import androidx.annotation.ColorInt
import omnifit.commons.core.Indicatable
import omnifit.mindcare.training.R
import omnifit.mindcare.training.helper.durationOf

enum class ServiceCategory {
    MEDITATIONS,
    HEALING_MUSIC,
    MEASUREMENT;

    companion object {
        fun of(n: Int): ServiceCategory = values()[n]
    }
}

enum class MeditationIndicator(
    val label: String,
    @ColorInt
    val color: Int,
    val scoreRange: IntRange
) : Indicatable<MeditationIndicator> {
    /* 매우 낮음 */
    BAD("Bad", Color.parseColor("#ffff5f5f"), 0..20),
    /* 낮음 */
    NOT_GOOD("Not good", Color.parseColor("#ffffa800"), 21..40),
    /* 보통 */
    GOOD("Good", Color.parseColor("#ffa3c535"), 41..60),
    /* 높음 */
    VERY_GOOD("Very good", Color.parseColor("#ff29bfd0"), 61..80),
    /* 매우 높음 */
    EXCELLENT("Excellent", Color.parseColor("#ff5a98ed"), 81..100);

    companion object {
        fun of(name: String): MeditationIndicator = valueOf(name)
        fun of(score: Int): MeditationIndicator = values().first { indicator ->
            score in indicator.scoreRange
        }
    }
}

enum class MeasurementSimpleLevel(
    val labelOfEn: String,
    val labelOfKr: String,
    val verdictValue: Int,
    @ColorInt
    val color: Int,
    val scroreRange: LongRange
) {
    BAD("Bad", "힐링필요", 1, R.color.x_fb1b5a, 1..1.5.toLong()),
    NORMAL("Normal", "양호", 2, R.color.x_fe793f, 1.75.toLong()..2.25.toLong()),
    GOOD("Good", "좋음",3, R.color.x_006cff, 2.5.toLong()..3);

    companion object {
        fun of(verdict: Int): MeasurementSimpleLevel = values().first { indicator ->
            verdict == indicator.verdictValue
        }

        fun of(score: Long): MeasurementSimpleLevel = values().first { indicator ->
            score in indicator.scroreRange
        }
    }
}

enum class EffectSound(val resource: Int) {
    SCAN_START(R.raw.effect_eeg_1),
    SCAN_FINISH(R.raw.effect_eeg_2),
    SCAN_DETACHED(R.raw.effect_eeg_3);

    fun duration(context: Context): Long {
        return durationOf(context, resource)
    }
}