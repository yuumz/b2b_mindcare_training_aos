package omnifit.mindcare.training.configuration

import android.content.Context
import androidx.core.content.edit
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.mindcare.training.requireSharedPreferences
import timber.log.Timber

data class UserConfiguration(
    var latestMeasurementdate: String = "",
    var brainScore: Int = 0,
    var recentlyHealingMusicContentSequence: Int = 0
) {

    fun set(context: Context) {
        toJson()
            .let { pref ->
                Timber.d("--> 저장될 유저 설정:$pref")
                requireSharedPreferences(context)
                    .edit {
                        putString(PREF_NAME, pref)
                    }
            }
    }


    companion object {
        const val PREF_NAME: String = "User"

        fun isRegistered(context: Context): Boolean {
            return with(get(context)) {
                latestMeasurementdate.isNotEmpty()
            }
        }

        fun set(
            context: Context,
            run: UserConfiguration.() -> Unit
        ) {
            get(context).apply(run).set(context)
        }

        fun get(context: Context): UserConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    UserConfiguration().fromJson(json)
                }
        }
    }
}