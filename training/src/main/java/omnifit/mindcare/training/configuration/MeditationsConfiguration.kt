package omnifit.mindcare.training.configuration

import android.annotation.SuppressLint
import android.content.Context
import androidx.core.content.edit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.jakewharton.rx.replayingShare
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.commons.core.secondsToMillis
import omnifit.mindcare.training.common.SIGNATURE_MIC_BRAIN_ACTIVITY
import omnifit.mindcare.training.common.SIGNATURE_MIC_BRAIN_STRESS
import omnifit.mindcare.training.common.SIGNATURE_MIC_RELAXATION
import omnifit.mindcare.training.data.Electroencephalography
import omnifit.mindcare.training.requireSharedPreferences
import timber.log.Timber
import java.util.*

data class MeditationsConfiguration(
    var id: String = "", // yyyyMMddHHmmss
    var begin: Date = Date(),
    var end: Date = Date(),
    var sequence:Int = 0,
    var micSignature: String = SIGNATURE_MIC_RELAXATION,
    var micGraphValues: String = "[]",
    var totalMeditationTime: Long = 0L,
    var deepMeditationTime: Long = 0L,
    var deepMeditationCount: Int = 0,
    var point: Int = 0,
    // yuumz: b2b에 추가된 사항
    var usedProgramTitle: String = "" // 디비로 전환 시 위치 확인
) {

    companion object {
        const val PREF_NAME: String = "MT"

        fun create(
            elapsedTime: Int,
            sq:Int,
            ms: String,
            dntc: Int,
            rewardPoint:Int,
            programTitle: String,
            electroencephalography: Electroencephalography,
            onChange: (MeditationsConfiguration?) -> Unit = {}
        ) = with(MeditationsConfiguration()) {
            id = "$PREF_NAME${electroencephalography.id}"
            begin = electroencephalography.begin
            end = electroencephalography.end
            sequence = sq
            micSignature = ms
            micGraphValues = when (ms) {
                SIGNATURE_MIC_BRAIN_STRESS   -> electroencephalography.lHBetaIndicatorValues
                SIGNATURE_MIC_RELAXATION     -> electroencephalography.relaxationIndicatorValues
                SIGNATURE_MIC_BRAIN_ACTIVITY -> electroencephalography.sef90HzValues().toJson()
                else                         -> "[]"
            }
            totalMeditationTime = elapsedTime.secondsToMillis()
            deepMeditationTime = dntc.times(2).secondsToMillis()
            deepMeditationCount = dntc
            point = rewardPoint
            usedProgramTitle = programTitle

            onChange(this)
        }

//        @SuppressLint("CheckResult")
//        fun observePointChange(
//            owner: LifecycleOwner,
//            cb: (Int) -> Unit
//        ) {
//            POINT_SHARE
//                .bindUntilEvent(owner, Lifecycle.Event.ON_DESTROY)
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(
//                    { s -> cb(s) },
//                    { e -> Timber.e(e) }
//                )
//        }
//
//        private val POINT_SUBJECT: Subject<Int> by lazy {
//            BehaviorSubject
//                .createDefault(0)
//                .toSerialized()
//        }
//
//        private val POINT_SHARE: Observable<Int> by lazy {
//            POINT_SUBJECT
//                .subscribeOn(Schedulers.io())
//                .observeOn(Schedulers.io())
//                .replayingShare()
//        }

        fun find(context: Context): MeditationsConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    MeditationsConfiguration().fromJson(json)
                }
        }
    }

    fun save(context: Context) {
        toJson()
            .let { pref ->
                Timber.d("--> 저장될 명상 설정:$pref")
                requireSharedPreferences(context)
                    .edit {
                        putString(PREF_NAME, pref)
                    }
            }
    }
}