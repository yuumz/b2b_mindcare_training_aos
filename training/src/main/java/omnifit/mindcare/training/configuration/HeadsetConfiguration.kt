package omnifit.mindcare.training.configuration

import android.content.Context
import androidx.core.content.edit
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.mindcare.training.headset.PowerMode
import omnifit.mindcare.training.requireSharedPreferences
import timber.log.Timber

data class HeadsetConfiguration(
    var headsetDevice: DeviceConfiguration = DeviceConfiguration(),
    var firmwareVersion: Int = 0,
    var powerMode: PowerMode = PowerMode.UNKNOWN,
    var powerSaveMode: Boolean = false,
    var signalStabilization: Int = 100
) {
    val productName: String
        get() = if (headsetDevice.name.isNotEmpty()) {
            "${headsetDevice.name}|${headsetDevice.address}"
        }
        else ""

    fun isNotEmpty(): Boolean {
        return headsetDevice.isNotEmpty()
    }

    fun set(context: Context) {
        toJson()
            .let { pref ->
//                Timber.d("--> 저장될 헤드셋 설정:$pref")
                requireSharedPreferences(context)
                    .edit {
                        putString(PREF_NAME, pref)
                    }
            }
    }

    companion object {
        const val PREF_NAME: String = "Headset"

        fun isRegistered(context: Context): Boolean {
            return with(get(context)) {
                productName.isNotEmpty()
                        && headsetDevice.isNotEmpty()
            }
        }

        fun set(
            context: Context,
            run: HeadsetConfiguration.() -> Unit
        ) {
            get(context).apply(run).set(context)
        }

        fun get(context: Context): HeadsetConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    HeadsetConfiguration().fromJson(json)
                }
        }
    }
}

data class DeviceConfiguration(
    var name: String = "",
    var address: String = ""
) {
    fun isNotEmpty(): Boolean {
        return name.isNotEmpty() && address.isNotEmpty()
    }
}