package omnifit.mindcare.training.configuration

import android.content.Context
import androidx.core.content.edit
import omnifit.commons.core.*
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.mindcare.training.data.Electroencephalography
import omnifit.mindcare.training.requireSharedPreferences
import timber.log.Timber
import java.util.*

data class MeasurementConfiguration(
    var id: String = "", // yyyyMMddHHmmss
    var begin: Date = Date(),
    var end: Date = Date(),
    var brainStressIndicatorValue: Double = 0.0,
    var brainStressSignature: String = BrainStressIndicator.NORMAL.name,
    var brainStressScore: Int = 0,
    var brainActivityValue: Double = 0.0,
    var brainActivitySignature: String = BrainActivityIndicator.ADEQUATE.name,
    var brainActivityScore: Int = 0,
    var concentrationIndicatorValue: Double = 0.0,
    var concentrationSignature: String = ConcentrationIndicator.NORMAL.name,
    var concentrationScore: Int = 0,
    var concentrationTypeValue: Int = 0,
    var concentrationTypeSignature: String = ConcentrationTypeIndicator.NORMAL.name,
    var balanceIndicatorValue: Double = 0.0,
    var balanceSignature: String = BalanceIndicator.BALANCE.name,
    var balanceScore: Int = 0,
    var brainHealthScore: Int = 0,
    var rhythmDistribution: String = "[]",
    var recommendedContentSequence: Int = 0
) {

    companion object {
        const val PREF_NAME: String = "MS"

        fun create(
            electroencephalography: Electroencephalography,
            onChange: (MeasurementConfiguration?) -> Unit = {}
        ) = with(MeasurementConfiguration()) {
            id = "$PREF_NAME${electroencephalography.id}"
            begin = electroencephalography.begin
            end = electroencephalography.end
            brainStressIndicatorValue = electroencephalography.brainStressIndicatorValue()
            brainStressIndicator = electroencephalography.brainStressIndicator()
            brainStressScore = brainStressIndicator.score
            brainActivityValue = electroencephalography.sef90Hz()
            brainActivityIndicator = electroencephalography.brainActivityIndicator()
            brainActivityScore = brainActivityIndicator.score
            concentrationIndicatorValue = electroencephalography.concentrationIndicatorValue()
            concentrationIndicator = electroencephalography.concentrationIndicator()
            concentrationScore = concentrationIndicator.score
            concentrationTypeValue = electroencephalography.concentrationTypeValue()
            concentrationTypeIndicator = electroencephalography.concentrationTypeIndicator()
            balanceIndicatorValue = electroencephalography.balanceIndicatorValue()
            balanceIndicator = electroencephalography.balanceIndicator()
            balanceScore = balanceIndicator.score
            brainHealthScore = brainStressScore + brainActivityScore + concentrationScore + balanceScore
            rhythmDistribution = electroencephalography.rhythmDistribution().toJson()
            recommendedContentSequence = omnifit.mindcare.training.algorithm.ServiceAlgorithm.recommendedContentSequence(electroencephalography)

            onChange(this)
        }

        fun find(context: Context): MeasurementConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    MeasurementConfiguration().fromJson(json)
                }
        }
    }

    fun save(context: Context) {
        toJson()
            .let { pref ->
                Timber.d("--> 저장될 측정 설정:$pref")
                requireSharedPreferences(context)
                    .edit {
                        putString(PREF_NAME, pref)
                    }
            }
    }

    var brainStressIndicator: BrainStressIndicator
        get() = BrainStressIndicator.of(brainStressSignature)
        set(indicator) {
            brainStressSignature = indicator.name
        }
    var brainActivityIndicator: BrainActivityIndicator
        get() = BrainActivityIndicator.of(brainActivitySignature)
        set(indicator) {
            brainActivitySignature = indicator.name
        }
    var concentrationIndicator: ConcentrationIndicator
        get() = ConcentrationIndicator.of(concentrationSignature)
        set(indicator) {
            concentrationSignature = indicator.name
        }
    var concentrationTypeIndicator: ConcentrationTypeIndicator
        get() = ConcentrationTypeIndicator.of(concentrationTypeSignature)
        set(indicator) {
            concentrationTypeSignature = indicator.name
        }
    var balanceIndicator: BalanceIndicator
        get() = BalanceIndicator.of(balanceSignature)
        set(indicator) {
            balanceSignature = indicator.name
        }
}