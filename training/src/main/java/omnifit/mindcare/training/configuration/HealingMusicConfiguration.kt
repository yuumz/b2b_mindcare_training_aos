package omnifit.mindcare.training.configuration

import android.content.Context
import androidx.core.content.edit
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.json.moshi.toJson
import omnifit.commons.core.secondsToMillis
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.SIGNATURE_MIC_BRAIN_STRESS
import omnifit.mindcare.training.data.Electroencephalography
import omnifit.mindcare.training.requireSharedPreferences
import timber.log.Timber
import java.util.*

class HealingMusicConfiguration(
    var id: String = "", // yyyyMMddHHmmss
    var begin: Date = Date(),
    var end: Date = Date(),
    var sequence:Int = 0,
    var micSignature: String = SIGNATURE_MIC_BRAIN_STRESS,
    var micGraphValues: String = "[]",
    var beforeScore: Int = 0,
    var afterScore: Int = 0,
    var rateOfChange: Int = 0,
    // yuumz: b2b에 추가된 사항
    var totalHealingTime: Long = 0L,
    var deepHealingTime: Long = 0L,
    var deepHealingCount: Int = 0,
    var usedProgramTitle: String = "" // 디비로 전환 시 위치 확인
) {
    companion object {
        const val PREF_NAME: String = "HM"

        fun create(
            elapsedTime: Int,
            sq:Int,
            ms: String,
            dntc: Int,
            programTitle: String,
            electroencephalography: Electroencephalography,
            onChange: (HealingMusicConfiguration?) -> Unit = {}
        ) = with(HealingMusicConfiguration()) {
            id = "$PREF_NAME${electroencephalography.id}"
            begin = electroencephalography.begin
            end = electroencephalography.end
            sequence = sq
            micSignature = ms
            micGraphValues = electroencephalography.lHBetaIndicatorValues
            doubleArrayOf().fromJson(micGraphValues)
                .filter { it >= 0 }
                .run {
                    beforeScore = ServiceAlgorithm.calcHealingMusicScore(subList(0, 10).average())
                    afterScore = ServiceAlgorithm.calcHealingMusicScore(subList(size - 10, size).average())
                    rateOfChange = afterScore - beforeScore
                }
            totalHealingTime = elapsedTime.secondsToMillis()
            deepHealingTime = dntc.times(2).secondsToMillis()
            deepHealingCount = dntc
            usedProgramTitle = programTitle

            onChange(this)
        }

        fun find(context: Context): HealingMusicConfiguration {
            return requireSharedPreferences(context)
                .getString(PREF_NAME, "{}")!!
                .let { json ->
                    HealingMusicConfiguration().fromJson(json)
                }
        }
    }

    fun save(context: Context) {
        toJson()
            .let { pref ->
                Timber.d("--> 저장될 힐링 설정:$pref")
                requireSharedPreferences(context)
                    .edit {
                        putString(PREF_NAME, pref)
                    }
            }
    }
}