package omnifit.mindcare.training.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.view.Gravity
import android.view.ViewManager
import androidx.appcompat.widget.AppCompatTextView
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko.custom.ankoView

class NonSpacingTextView constructor(context: Context) : AppCompatTextView(context) {

    private val textPaint: TextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG)
    private val textBounds: Rect = Rect()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        createTextLayout(calcTextParams(), textPaint).let { textLayout ->
            setMeasuredDimension(
                when (MeasureSpec.getMode(widthMeasureSpec)) {
                    MeasureSpec.AT_MOST     -> calcWidth(textLayout)
                    MeasureSpec.EXACTLY     -> MeasureSpec.getSize(widthMeasureSpec)
                    MeasureSpec.UNSPECIFIED -> widthMeasureSpec
                    else                    -> widthMeasureSpec
                },
                calcHeight(textLayout)
            )
        }
    }

    override fun onDraw(c: Canvas) {
        c.drawColor(Color.TRANSPARENT)
        layout?.run {
            textBounds.offset(-textBounds.left, -textBounds.top)
            createTextLayout(calcTextParams(), textPaint).let { textLayout ->
                c.save()
                c.translate(-textBounds.left.toFloat(), -calcTopSpacing().toFloat())
                textLayout.draw(c)
                c.restore()
            }
        } ?: super.onDraw(c)
    }

    private fun createTextLayout(
        src: CharSequence,
        paint: TextPaint
    ): StaticLayout {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            StaticLayout.Builder.obtain(src, 0, src.length, paint, layout.width - paddingLeft - paddingRight)
                .setAlignment(if (gravity == Gravity.CENTER) Layout.Alignment.ALIGN_CENTER else Layout.Alignment.ALIGN_NORMAL)
                .setLineSpacing(lineSpacingExtra, lineSpacingMultiplier)
                .setMaxLines(maxLines)
                .setIncludePad(false)
                .build()
        }
        else {
            StaticLayout(
                src,
                textPaint,
                layout.width - paddingLeft - paddingRight,
                if (gravity == Gravity.CENTER) Layout.Alignment.ALIGN_CENTER else Layout.Alignment.ALIGN_NORMAL,
                lineSpacingMultiplier,
                lineSpacingExtra,
                false
            )
        }
    }

    private fun calcWidth(l: StaticLayout): Int {
        return l.width - if (lineCount == 1) {
            (l.width - textBounds.width())
        }
        else 0
    }

    private fun calcHeight(l: StaticLayout): Int {
        return l.height - (calcTopSpacing() + calcBottomSpacing())
    }

    private fun calcTopSpacing(): Int {
        return Math.abs(textPaint.fontMetricsInt.ascent - textBounds.top)
    }

    private fun calcBottomSpacing(): Int {
        return Math.abs(textPaint.fontMetricsInt.descent - textBounds.bottom)
    }

    private fun calcTextParams(): CharSequence {
        return text
            .also { src ->
                textPaint.style = Paint.Style.FILL_AND_STROKE
                textPaint.letterSpacing = letterSpacing
                textPaint.textSize = textSize
                textPaint.typeface = typeface
                textPaint.shader = paint.shader
                textPaint.color = currentTextColor
                textPaint.getTextBounds(src.toString(), 0, src.length, textBounds)
                if (src.isEmpty()) textBounds.right = textBounds.left
            }
    }
}

/*
╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
║
║ ⥥
║
╚═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
*/

inline fun ViewManager.nonSpacingTextView(init: (@AnkoViewDslMarker NonSpacingTextView).() -> Unit): NonSpacingTextView {
    return ankoView({ NonSpacingTextView(it) }, theme = 0) {
        init()
    }
}

inline fun ViewManager.nonSpacingTextView(text: CharSequence?, init: (@AnkoViewDslMarker NonSpacingTextView).() -> Unit): NonSpacingTextView {
    return ankoView({ NonSpacingTextView(it) }, theme = 0) {
        init()
        setText(text)
    }
}

inline fun ViewManager.nonSpacingTextView(text: Int, init: (@AnkoViewDslMarker NonSpacingTextView).() -> Unit): NonSpacingTextView {
    return ankoView({ NonSpacingTextView(it) }, theme = 0) {
        init()
        setText(text)
    }
}