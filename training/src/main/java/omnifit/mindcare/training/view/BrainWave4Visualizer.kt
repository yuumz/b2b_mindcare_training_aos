package omnifit.mindcare.training.view

import android.content.Context
import android.graphics.*
import android.view.View
import android.view.ViewManager
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip
import java.nio.DoubleBuffer
import kotlin.math.sin

class BrainWave4Visualizer(
    context: Context
) : View(context) {

    init {
        isEnabled = false
        isActivated = false
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(
            when (MeasureSpec.getMode(widthMeasureSpec)) {
                MeasureSpec.AT_MOST     -> widthMeasureSpec
                MeasureSpec.EXACTLY     -> MeasureSpec.getSize(widthMeasureSpec)
                MeasureSpec.UNSPECIFIED -> widthMeasureSpec
                else                    -> widthMeasureSpec
            },
            heightMeasureSpec
        )
    }

    override fun onDraw(canvas: Canvas) {
        rendering(canvas)
        invalidate()
    }

    fun start() {
        isEnabled = true
    }

    fun stop() {
        isEnabled = false
    }

    companion object {
        const val DEFAULT_AMPLITUDE_SIZE: Double = 8.0
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */
    private val drawingWaveBitmap: Bitmap by lazy {
        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
    }

    private val drawingWavePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                strokeWidth = dip(2.0f).toFloat()
                color = Color.parseColor("#ffffffff")
                style = Paint.Style.STROKE
            }
    }

    private val drawingWavePath: Path by lazy {
        Path()
    }

    private var amplitudeSize: Double = height.toDouble()
        set(value) {
            field = when {
                value > height                 -> height.toDouble()
                value < DEFAULT_AMPLITUDE_SIZE -> DEFAULT_AMPLITUDE_SIZE
                else                           -> value
            }
        }

    /**
     * 진폭
     */
    private val drawingWaveAmplitude: Double
        get() = height / amplitudeSize

    /**
     * 주기
     */
    private val drawingWavePeriod: Int = 2

    /**
     * 기준선
     */
    private val drawingWaveV0: Int
        get() = height / 2

    /**
     * 0 ~ 360도 정현파 변위
     */
    private val referenceSineWave: DoubleBuffer by lazy {
        DoubleBuffer.wrap(
            DoubleArray(361) { angle ->
                sin(Math.toRadians(angle.toDouble()))
            }.apply {
                reverse()
            }
        )
    }

    /**
     *
     */
    private val drawingWaveVelocity: Int = 7

    private fun rendering(c: Canvas) {
        drawWave(c)
    }

    private var drawingWaveCache: IntArray? = null
    private fun drawWave(canvas: Canvas) {
        checkEnabled()

        drawingWaveCache = drawingWaveCache
            ?.rightShift(drawingWaveVelocity)
            ?.apply {
                for (e in drawingWaveVelocity - 1 downTo 0) {
                    if (referenceSineWave.position() == referenceSineWave.capacity()) {
                        referenceSineWave.rewind()
                    }
                    this[e] = drawingWaveV0 - (drawingWaveAmplitude * referenceSineWave.get()).toInt()
                }
            } ?: IntArray(width / drawingWavePeriod) { drawingWaveV0 }

        drawingWavePath.reset()
        drawingWavePath.let { path ->
            drawingWaveCache?.forEachIndexed { x, y ->
                if (x == 0) {
                    path.moveTo(x.toFloat() + x, y.toFloat())
                }
                else {
                    path.lineTo((x.toFloat() * drawingWavePeriod) + x, y.toFloat())
                }
            }

            drawingWaveBitmap.let { b ->
                b.eraseColor(Color.TRANSPARENT)
                Canvas(b).let { c ->
                    // 사인파 그리기
                    c.drawPath(path, drawingWavePaint)
                }
                canvas.drawBitmap(b, 0.0f, 0.0f, null)
            }
        }
    }

    private fun checkEnabled() {
        if (isEnabled) {
            if (amplitudeSize > DEFAULT_AMPLITUDE_SIZE) {
                amplitudeSize -= amplitudeSize * 0.08
            }
        }
        else {
            if (amplitudeSize < height) {
                amplitudeSize += 1
            }
        }
    }

    private fun IntArray.rightShift(n: Int): IntArray {
        return IntArray(n) { 0 } + sliceArray(0..size - (n + 1))
    }
}

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

inline fun ViewManager.brainWave4Visualizer(
    theme: Int = 0,
    init: BrainWave4Visualizer.() -> Unit
): BrainWave4Visualizer {
    return ankoView({ BrainWave4Visualizer(it) }, theme, init)
}
