package omnifit.mindcare.training.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.text.Spanned
import android.view.Gravity
import android.view.ViewManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.core.animation.addListener
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textResource

class CircularProgressBar constructor(context: Context) : FrameLayout(context) {
    init {
        setWillNotDraw(false)
    }

    @ColorInt
    var progressBackgroundColor: Int = Color.TRANSPARENT
    var progressBackgroundStroke: Float = 0.0f
    @ColorInt
    var progressColor: Int = Color.TRANSPARENT
    var progressStroke: Float = 0.0f
    var progressStrokeCap: Paint.Cap = Paint.Cap.ROUND
    var progressStartAngle: Float = -90.0f
    var progressChangeAngle: Float = 0.0f
        set(value) {
            field = value
            invalidate()
        }
    var progressMax: Int = 100
    var progress: Int = 0
    var isAnimatable: Boolean = false
    var progressInterpolator: Interpolator = AccelerateDecelerateInterpolator()
    var progressDuration: Long = 1000L
    var progressListener: ProgressListener? = null
    private val progressAnimator: ValueAnimator by lazy {
        ValueAnimator.ofFloat(0.0f, calcSweepAngle(progress))
            .apply {
                duration = progressDuration
                interpolator = progressInterpolator
                addListener(
                    onStart = {
                        progressListener?.onProgressStart()
                    },
                    onEnd = {
                        progressListener?.onProgressEnd()
                    },
                    onCancel = {

                    }
                )
                addUpdateListener { animator ->
                    progressChangeAngle = animator.animatedValue as Float
                }
            }
    }
    val progressMessage: NonSpacingTextView by lazy {
        NonSpacingTextView(context).apply {
            layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT).apply {
                gravity = Gravity.CENTER
            }
        }
    }
    private val progressBackgroundPaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isDither = true
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
            strokeJoin = Paint.Join.ROUND
            strokeWidth = progressBackgroundStroke
            color = progressBackgroundColor
        }
    }
    private val progressPaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isDither = true
            style = Paint.Style.STROKE
            strokeCap = progressStrokeCap
            strokeJoin = Paint.Join.ROUND
            strokeWidth = progressStroke
            color = progressColor
        }
    }
    private val progressRectF: RectF by lazy {
        Math.max(progressBackgroundPaint.strokeWidth, progressPaint.strokeWidth).let { stroke ->
            RectF(stroke / 2.0f, stroke / 2.0f, width.toFloat() - stroke / 2.0f, height.toFloat() - stroke / 2.0f)
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        addMessageViewIfNeeded()
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        run {
            when (MeasureSpec.getMode(widthMeasureSpec)) {
                MeasureSpec.EXACTLY -> MeasureSpec.getSize(widthMeasureSpec)
                MeasureSpec.AT_MOST,
                MeasureSpec.UNSPECIFIED -> widthMeasureSpec
                else -> widthMeasureSpec
            }
        }.let { size ->
            setMeasuredDimension(size, size)
        }
    }

    override fun onDraw(c: Canvas) {
        super.onDraw(c)
        drawProgressBackground(c)
        drawProgress(c)
    }

    fun startAnimation() {
        progressAnimator.start()
    }

    private fun addMessageViewIfNeeded() {
        if (progressMessage.text.isNotEmpty()) {
            if (childCount > 0) removeAllViews()
            addView(progressMessage)
        }
    }

    private fun drawProgressBackground(c: Canvas) {
        c.drawArc(progressRectF, progressStartAngle, 360.0f, false, progressBackgroundPaint)
    }

    // 애니메이션 적용
    private fun drawProgress(c: Canvas) {
        c.drawArc(progressRectF, progressStartAngle, if (isAnimatable) progressChangeAngle else calcSweepAngle(progress), false, progressPaint)
    }

    private fun calcSweepAngle(progress: Int): Float {
        return progress * 360.0f / progressMax.toFloat()
    }

    // TEST --------------------------------------------------------------------
    private val outRectPaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            isDither = true
            style = Paint.Style.STROKE
            strokeWidth = 1.0f
            color = Color.WHITE
        }
    }

    interface ProgressListener {
        fun onProgressStart()
        fun onProgressEnd()
        fun onProgressCancel()
    }
}

/*
╔═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
║
║ ⥥
║
╚═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
*/

inline fun ViewManager.circularProgressBar(init: (@AnkoViewDslMarker CircularProgressBar).() -> Unit): CircularProgressBar {
    return ankoView({ CircularProgressBar(it) }, theme = 0) {
        init()
    }
}