package omnifit.mindcare.training.view

import android.content.Context
import android.graphics.*
import android.view.TextureView
import android.view.ViewManager
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.dip
import java.nio.DoubleBuffer

class BrainWave4Real(
    context: Context
) : TextureView(context), TextureView.SurfaceTextureListener {

    var lineAxisMaximum: Float = 32767.0f

    init {
        isOpaque = false //i::Background Black Screen Solution !!!
        surfaceTextureListener = this
    }

    companion object {
        const val RENDER_FRAME = (1.0f / 60.0f * 1000L).toLong()
    }

    var renderWidth: Int = 0
    var renderHeight: Int = 0
    val renderer = Renderer()

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture?, width: Int, height: Int) {
        if (lineAxisMaximum < height) {
            lineAxisMaximum = height.toFloat()
        }

        renderWidth = width
        renderHeight = height

        // yuumz: 2019.06.03 - renderer가 이미 스타드가 되어 있을경
        if(renderer.state == Thread.State.NEW)
            renderer.start()
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        renderer.stopRender()
        return true
    }

    private val drawingCenterLinePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                strokeWidth = dip(1.0f).toFloat()
                color = Color.TRANSPARENT
                style = Paint.Style.STROKE
            }
    }

    private val drawingCenterLinePath: Path by lazy {
        Path().apply {
            moveTo(0.0f, renderHeight.toFloat() / 2.0f)
            lineTo(renderWidth.toFloat(), renderHeight.toFloat() / 2.0f)
        }
    }

    private val drawingWavePaint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                strokeWidth = dip(2.0f).toFloat()
                color = Color.parseColor("#ffffffff")
                style = Paint.Style.STROKE
            }
    }

    private val drawingWavePath: Path by lazy {
        Path()
    }

    private val referenceSineWave: DoubleBuffer by lazy {
        DoubleBuffer.allocate(renderWidth)
    }

    fun putLineData(data: Double) {
        if (referenceSineWave != null) {
            if (referenceSineWave.position() == referenceSineWave.capacity()) {
                referenceSineWave.position(1)
                referenceSineWave.compact()
            }
            referenceSineWave.put(renderHeight - ((data * renderHeight) / lineAxisMaximum))
        }
    }

    fun rendering(canvas: Canvas) {
        canvas.drawColor(0x00000000, PorterDuff.Mode.MULTIPLY)
        canvas.drawPath(drawingCenterLinePath, drawingCenterLinePaint)

        if (referenceSineWave.position() > 0) {
            val wave = referenceSineWave.array()
            drawingWavePath.reset()
            drawingWavePath.let { path ->
                wave.forEachIndexed { x, y ->
                    if(x == 0) {
                        path.moveTo(0.0f, wave[0].toFloat())
                    } else {
                        path.lineTo(x.toFloat(), if (y > 0) y.toFloat() else renderHeight.toFloat() / 2.0f)
                    }
                }

                canvas.drawPath(path, drawingWavePaint)
            }
        }
    }

    inner class Renderer : Thread() {

        @Volatile
        private var renderable = true

        init {
            isDaemon = true
        }

        override fun run() {
            while (renderable && !Thread.currentThread().isInterrupted) {
                val canvas = this@BrainWave4Real.lockCanvas()
                try {
                    if (canvas != null) {
                        rendering(canvas)
                    }
                } finally {
                    this@BrainWave4Real.unlockCanvasAndPost(canvas)
                    try {
                        sleep(RENDER_FRAME)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }

                }
            }
        }

        fun stopRender() {
            interrupt()
            renderable = false
            var retry = true
            while (retry) {
                try {
                    join()
                    retry = false
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

            }
        }
    }
}

/*
╔═══════════════════════════════════════════════════════════════════════════════
║
║ ⥥
║
╚═══════════════════════════════════════════════════════════════════════════════
*/

inline fun ViewManager.brainWave4Real(
    theme: Int = 0,
    init: BrainWave4Real.() -> Unit
): BrainWave4Real {
    return ankoView({ BrainWave4Real(it) }, theme, init)
}
