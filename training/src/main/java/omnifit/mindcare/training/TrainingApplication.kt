package omnifit.mindcare.training

import android.app.Application
import android.content.Context
import android.util.DisplayMetrics
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import io.fabric.sdk.android.Fabric
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.mindcare.training.configuration.UserConfiguration
import omnifit.mindcare.training.headset.Headset
import org.jetbrains.anko.windowManager
import timber.log.Timber
import java.lang.ref.WeakReference

class TrainingApplication : Application() {

    private var contextReference: WeakReference<Context>? = null

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        Timber.plant(Timber.DebugTree())
    }

    override fun onCreate() {
        super.onCreate()

        contextReference = WeakReference(this)
        contextReference?.get()?.let { ref ->
            initialize(ref)
        }
    }

    private fun initialize(context: Context) {
        requireDensity()
        Font.init(context)
        Headset.init(context)
        AudioPlayer.init(context)
        Fabric.with(context, Crashlytics())
    }

    var desiredDisplayMetrics: DisplayMetrics? = null

    /**
     * densityDpi :
     *
     * ldpi = 0.75 (120dpi)
     * mdpi = 1 (160dpi)
     * hdpi = 1.5 (240dpi)
     * xhdpi = 2.0 (320dpi)
     * xxhdpi = 3.0 (480dpi)
     * xxxhdpi = 4.0 (640dpi)
     */
    private fun requireDensity() {
        resources.displayMetrics.density.let { density ->
            when {
                density > 0.75f && density < 1.0f -> 1.0f
                density > 1.0f && density < 1.5f  -> 1.5f
                density > 1.5f && density < 2.0f  -> 2.0f
                density > 2.0f && density < 3.0f  -> 3.0f
                density > 3.0f && density < 4.0f  -> 4.0f
                else                              -> return
            }
        }.let(this::changeDensity)
    }

    private fun changeDensity(desiredDensity: Float) {
        DisplayMetrics().apply {
            windowManager.defaultDisplay.getMetrics(this)
            Timber.d("--> 화면 기본 밀도 변경 ($densityDpi) -> (${(desiredDensity * 160f).toInt()}")
            density = desiredDensity
            densityDpi = (desiredDensity * 160f).toInt()
            scaledDensity = desiredDensity
            xdpi = desiredDensity * 160f
            ydpi = desiredDensity * 160f
        }.let { metrics ->
            resources.displayMetrics.setTo(metrics)
            desiredDisplayMetrics = metrics
        }
    }

    fun close() {
        contextReference?.clear()
    }
}

@GlideModule
class RestGlideModule : AppGlideModule()