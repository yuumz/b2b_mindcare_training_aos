package omnifit.mindcare.training.algorithm

import omnifit.commons.core.BrainActivityIndicator
import omnifit.commons.core.BrainStressIndicator
import omnifit.commons.core.RelaxationIndicator
import omnifit.commons.core.roundToDecimalPlaces
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.data.Electroencephalography
import omnifit.mindcare.training.headset.fx2.DELTA
import omnifit.mindcare.training.headset.fx2.THETA
import timber.log.Timber
import kotlin.math.roundToInt
import kotlin.random.Random

object ServiceAlgorithm {

    /**
     * 알파 리듬 파워 크기 판정값과 세타 리듬 파워 크기 판정값의 우선 순위를 비교해
     * 적합한 수면 유도 바이노럴 비트 타입을 돌려준다.
     */
    fun binauralBeat(
        theta: Double,
        alpha: Double
    ): Int {
        return when {
            alpha > theta -> THETA
            else -> DELTA
        }
    }

    /**
     * 전체 진행 시간에서 1/4, 2/4, 3/4, 4/4 지점의 시작 시간을 돌려준다.
     */
    fun calcBinauralBeatCheckpointByQuarter(duration: Int): Quarter {
        return duration.div(4).let { quarter ->
            Quarter(
                0,
                quarter,
                quarter.times(2),
                quarter.times(3)
            )
        }
    }

    data class Quarter(
        val first: Int,
        val second: Int,
        val third: Int,
        val fourth: Int
    )

    fun checkDeepMeditationDuringMeditation(
        micSignature: String,
        indicatorValue: Double
    ): Boolean {
//        return when (micSignature) {
//            SIGNATURE_MIC_BRAIN_STRESS -> (10.0 - indicatorValue) >= 7.0
//            SIGNATURE_MIC_RELAXATION -> indicatorValue >= 7.0
//            SIGNATURE_MIC_BRAIN_ACTIVITY -> indicatorValue == 2.0
//            else -> false
//        }
        return if (indicatorValue > -1.0) {
            when (micSignature) {
                SIGNATURE_MIC_BRAIN_STRESS -> (10.0 - indicatorValue) >= 7.0
                SIGNATURE_MIC_RELAXATION -> (indicatorValue >= 7.0).also {
//                    if(it) Timber.e("---> 명상 값 : $indicatorValue")
                }
                SIGNATURE_MIC_BRAIN_ACTIVITY -> indicatorValue == 2.0
                else -> false
            }
        } else {
            false
        }
    }

    fun calcMeditationScore(
        deepMeditationTime: Long,
        totalMeditationTime: Long
    ): Int {
        return deepMeditationTime.toDouble()
            .div(totalMeditationTime.toDouble())
            .times(100.0)
            .roundToInt()
            .let { percent ->
                when {
                    percent <= 20 -> percent * 2
                    percent <= 60 -> percent + 20
                    percent in 61..62 -> 81
                    percent in 63..64 -> 82
                    percent in 65..66 -> 83
                    percent in 67..68 -> 84
                    percent in 69..70 -> 85
                    percent in 71..72 -> 86
                    percent in 73..74 -> 87
                    percent in 75..76 -> 88
                    percent in 77..78 -> 89
                    percent in 79..80 -> 90
                    percent in 81..82 -> 91
                    percent in 83..84 -> 92
                    percent in 85..86 -> 93
                    percent in 87..88 -> 94
                    percent in 89..90 -> 95
                    percent in 91..92 -> 96
                    percent in 93..94 -> 97
                    percent in 95..96 -> 98
                    percent in 97..98 -> 99
                    else -> 100
                }
            }
    }

    fun calcMeditationRewardPoint(
        micSignature: String,
        indicatorValue: Double
    ): Int {
        return when (micSignature) {
            SIGNATURE_MIC_BRAIN_STRESS -> (10.0 - indicatorValue) >= 9.0
            SIGNATURE_MIC_RELAXATION -> indicatorValue >= 9.0
            SIGNATURE_MIC_BRAIN_ACTIVITY -> indicatorValue == 2.0
            else -> false
        }.let { able ->
            if (able) 1 else 0
        }
    }

    fun checkDeepSleepDuringNap(
        theta: Double,
        alpha: Double
    ): Boolean {
        return theta > alpha
//                && theta >= 7.0
    }

    fun calcNapRewardPoint(
        totalSleepTime: Long,
        deepSleepTime: Long
    ): Int {
        return deepSleepTime.div(totalSleepTime.toDouble().times(0.05).toLong()).toInt()
    }

    // AudioContent sequences
    private val recommendedContentSequences: IntArray = intArrayOf(
        226, // CFI(517)
        230, // CFI(526)
        231, // CFI(528)
        229, // CFI(524)
        227, // CFI(520)
        228  // CFI(522)
    )

    fun recommendedContentSequence(electroencephalography: Electroencephalography): Int {
        return when {
            electroencephalography.brainStressIndicator() == BrainStressIndicator.VERY_HIGH -> recommendedContentSequences[0] // AC(226)
            electroencephalography.relaxationIndicator() == RelaxationIndicator.VERY_LOW -> recommendedContentSequences[1] // AC(230)
            electroencephalography.brainActivityIndicator() == BrainActivityIndicator.OVERLOAD -> recommendedContentSequences[2] // AC(231)
            electroencephalography.brainStressIndicator() == BrainStressIndicator.HIGH -> recommendedContentSequences[3] // AC(229)
            electroencephalography.relaxationIndicator() == RelaxationIndicator.LOW -> recommendedContentSequences[4] // AC(227)
            electroencephalography.brainActivityIndicator() == BrainActivityIndicator.LOAD -> recommendedContentSequences[5] // AC(228)
            else -> {
                recommendedContentSequences[Random.nextInt(
                    recommendedContentSequences.size
                )]
            }
        }
    }

    fun checkDeepHealingMusicDuringHealing(
        micSignature: String,
        indicatorValue: Double
    ): Boolean {
        return if (indicatorValue > -1.0) {
            when (micSignature) {
                SIGNATURE_MIC_BRAIN_STRESS -> ((10.0 - indicatorValue) >= 7.0).also {
//                    if(it) Timber.e("---> 힐링 값 : $indicatorValue")
                }
                SIGNATURE_MIC_RELAXATION -> indicatorValue >= 7.0
                SIGNATURE_MIC_BRAIN_ACTIVITY -> indicatorValue == 2.0
                else -> false
            }
        } else {
            false
        }
    }

    fun calcHealingMusicScore(meanIndicatorValue: Double) :Int {
        return (100.0 - (meanIndicatorValue.roundToDecimalPlaces() * 10.0)).toInt()
    }

    fun measurementStep(category: Int, result: Int): Int {
        return when (category) {
            MEASUREMENT_ITEM_BRAIN_STRESS -> {
                when (result) {
                    0, 1, 2 -> 3
                    3 -> 2
                    else -> 1
                }
            }
            MEASUREMENT_ITEM_CONCENTRATION -> {
                when (result) {
                    0 -> 1
                    1 -> 2
                    else -> 3
                }
            }
            else -> {
                when (result) {
                    0, 4 -> 1
                    1, 3 -> 2
                    else -> 3
                }
            }
        }
    }
}

