package omnifit.mindcare.training.component

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.UIPopScreen
import omnifit.mindcare.training.*
import omnifit.mindcare.training.common.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout

class ResultGraphPopComponent<in T : UIPopScreen>(
    context: Context,
    _item: Int,
    _result: Int,
    _simpleLevel: MeasurementSimpleLevel
) : AnkoComponent<T> {

    private var item: Int = MEASUREMENT_ITEM_BRAIN_STRESS
    private var result: Int = 0
    private var simpleLevel: MeasurementSimpleLevel = MeasurementSimpleLevel.GOOD
    private var label: Array<Int> = arrayOf()
    private var graphMargin: Float = 259.8f

    init {
        item = _item
        result = _result
        simpleLevel = _simpleLevel
        label = measurementItemLabel(item)
        graphMargin += (result * 142.7f)
    }

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            imageView {
                id = R.id.screen_inner_widget_id_01
                measurementItemGraphPinBg(simpleLevel)
            }.lparams(wrapContent, wrapContent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                marginStart = dip(graphMargin)
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
            }

            imageView {
                measurementItemGraphPin(item)
            }.lparams(wrapContent, wrapContent) {
                startToStart = R.id.screen_inner_widget_id_01
                topToTop = R.id.screen_inner_widget_id_01
            }

            imageView(R.drawable.img_bg_graphline_02) {
                id = R.id.screen_inner_widget_id_02
            }.lparams(wrapContent, wrapContent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToBottom = R.id.screen_inner_widget_id_01
                topMargin = dip(7.3f)
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            }
            horizontalProgressBar {
                progressDrawable = when (simpleLevel) {
                    MeasurementSimpleLevel.GOOD -> ContextCompat.getDrawable(
                        context,
                        R.drawable.layer_list_graph_good
                    )
                    MeasurementSimpleLevel.NORMAL -> ContextCompat.getDrawable(
                        context,
                        R.drawable.layer_list_graph_normal
                    )
                    else -> ContextCompat.getDrawable(context, R.drawable.layer_list_graph_bad)
                }
                max = 100
                progress = (result + 1) * 20
            }.lparams(dip(0.0f), dip(5.3f)) {
                startToStart = R.id.screen_inner_widget_id_02
                endToEnd = R.id.screen_inner_widget_id_02
                bottomToBottom = R.id.screen_inner_widget_id_02
            }

            linearLayout {
                textView {
                    text = resources.getString(label[0]).replaceNewLineAs()
                    typeface = Font.kopubDotumBold
                    textSize = 17.3f
                    textColorResource = if (result == 0) simpleLevel.color else R.color.x_2e2e2e_op60
                    includeFontPadding = false
                    lines = 1
                    gravity = Gravity.RIGHT
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[1]).replaceNewLineAs()
                    typeface = Font.kopubDotumBold
                    textSize = 17.3f
                    textColorResource = if (result == 1) simpleLevel.color else R.color.x_2e2e2e_op60
                    includeFontPadding = false
                    lines = 1
                    gravity = Gravity.RIGHT
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[2]).replaceNewLineAs()
                    typeface = Font.kopubDotumBold
                    textSize = 17.3f
                    textColorResource = if (result == 2) simpleLevel.color else R.color.x_2e2e2e_op60
                    includeFontPadding = false
                    lines = 1
                    gravity = Gravity.RIGHT
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[3]).replaceNewLineAs()
                    typeface = Font.kopubDotumBold
                    textSize = 17.3f
                    textColorResource = if (result == 3) simpleLevel.color else R.color.x_2e2e2e_op60
                    includeFontPadding = false
                    lines = 1
                    gravity = Gravity.RIGHT
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }

                textView {
                    text = resources.getString(label[4]).replaceNewLineAs()
                    typeface = Font.kopubDotumBold
                    textSize = 17.3f
                    textColorResource = if (result == 4) simpleLevel.color else R.color.x_2e2e2e_op60
                    includeFontPadding = false
                    lines = 1
                    gravity = Gravity.RIGHT
                }.lparams(dip(0.0f), wrapContent) {
                    weight = 1.0f
                }
            }.lparams(dip(0.0f), wrapContent) {
                orientation = ConstraintLayout.LayoutParams.HORIZONTAL
                startToStart = R.id.screen_inner_widget_id_02
                topToBottom = R.id.screen_inner_widget_id_02
                topMargin = dip(15.3f)
                endToEnd = R.id.screen_inner_widget_id_02
            }
        }.applyRecursively { v ->
            (v as? ConstraintLayout)?.layoutParams =
                ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
    }
}