package omnifit.mindcare.training.component

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.mindcare.training.into
import omnifit.mindcare.training.screen.UIScreen
import org.jetbrains.anko.*

class HealingMusicContentItemComponent<T : UIScreen>(
    context: Context,
    owner: T
) : AnkoBindableComponent<AudioPlayer.AudioSource, T>(context, owner) {

    private var thumbnail: ImageView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        imageView {
            scaleType = ImageView.ScaleType.FIT_CENTER
        }.applyRecursively { v ->
            thumbnail = v as ImageView
            (v as? ImageView)?.layoutParams = ConstraintLayout.LayoutParams(dip(514.7f), dip(500.0f))
        }
    }

    override fun bind(data: AudioPlayer.AudioSource, position: Int) {
        super.bind(data, position)
        thumbnail?.into(data.thumbnail as? Drawable)
        thumbnail?.tag = data
    }

    companion object {
        const val CORNER_RADIUS: Float = 20.5f
    }
}