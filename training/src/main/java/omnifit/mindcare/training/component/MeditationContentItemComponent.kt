package omnifit.mindcare.training.component

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import jp.wasabeef.glide.transformations.CropSquareTransformation
import jp.wasabeef.glide.transformations.RoundedCornersTransformation
import omnifit.commons.core.DTP_MM_SS
import omnifit.commons.core.millisToFormatString
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.GlideApp
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.SIGNATURE_UTS_MEDITATION_TRAINING
import omnifit.mindcare.training.helper.MeditationSource
import omnifit.mindcare.training.replaceNewLineAsSpanned
import omnifit.mindcare.training.screen.UIScreen
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.support.v4.dip
import top.defaults.drawabletoolbox.DrawableBuilder

class MeditationContentItemComponent<T : UIScreen>(context: Context, owner: T) :
    AnkoBindableComponent<MeditationSource, T>(context, owner) {
    var content: CardView? = null
    var contentPlay: ImageButton? = null

    private var contentThumbnail: ImageView? = null
    private var contentTitle: TextView? = null
    private var contentClock: ImageView? = null
    private var contentPlayTime: TextView? = null

    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        linearLayout {

            cardView {
                id = R.id.screen_inner_widget_id_root_container
                cardElevation = dip(5.0f).toFloat()
                radius = dip(CORNER_RADIUS).toFloat()
                preventCornerOverlap = false
                setCardBackgroundColor(Color.WHITE)
                foreground = context.getDrawable(R.drawable.selector_card_fg)
                isSoundEffectsEnabled = true
                isClickable = true

                constraintLayout {

                    // 콘텐츠 BG
                    contentThumbnail = imageView {
                        id = R.id.screen_inner_widget_id_01
                    }.lparams(matchConstraint, matchConstraint) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }

                    // 콘텐츠 BG 불투명 레이어 (Opacity 30%)
                    imageView {
                        id = R.id.screen_inner_widget_id_02
                        backgroundDrawable = DrawableBuilder()
                            .rectangle()
                            .cornerRadius(dip(CORNER_RADIUS))
                            .solidColor(Color.BLACK)
                            .build()
                        alpha = 0.3f
                    }.lparams(matchConstraint, matchConstraint) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }

                    // 콘텐츠 타이틀
                    contentTitle = nonSpacingTextView {
                        id = R.id.screen_inner_widget_id_03
                        typeface = Font.kopubDotumBold
                        textSize = 28.0f
                        textColorResource = R.color.x_ffffff
                        includeFontPadding = false
                    }.lparams(matchConstraint, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(34.0f)
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        marginEnd = dip(34.0f)
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomMargin = dip(90.0f)
                    }

                    contentClock = imageView(R.drawable.ic_clock) {
                        id = R.id.screen_inner_widget_id_04
                    }.lparams(matchConstraint, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(34.0f)
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomMargin = dip(40.0f)
                    }

                    // 콘텐츠 플레이 타임
                    contentPlayTime = nonSpacingTextView {
                        id = R.id.screen_inner_widget_id_05
                        typeface = Font.latoRegular
                        textSize = 20.0f
                        textColorResource = R.color.x_ffffff
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_04
                        marginStart = dip(7.3f)
                        topToTop = R.id.screen_inner_widget_id_04
                        bottomToBottom = R.id.screen_inner_widget_id_04
                    }

                    // 콘텐츠 플레이
                    contentPlay = imageButton(R.drawable.selector_meditation_play) {
                        id = R.id.screen_inner_widget_id_06
                        backgroundColor = Color.TRANSPARENT
                    }.lparams(wrapContent, wrapContent) {
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        marginEnd = dip(22.0f)
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomMargin = dip(26.0f)
                    }
                }.lparams(matchParent, matchParent)
            }.lparams(dip(313.3f), dip(438.0f)) {
                marginStart = dip(32.7f)
            }.applyRecursively { v ->
                    when (v) {
                        is CardView -> {
                            content = v
                        }
                    }
                }
        }.applyRecursively { v ->
            when (v) {
                is LinearLayout -> {
                    v.layoutParams = FrameLayout.LayoutParams(wrapContent, wrapContent)
                }
            }
        }
    }

    override fun bind(data: MeditationSource, position: Int) {
        super.bind(data, position)
        contentThumbnail?.let { thumbnail ->
            GlideApp.with(owner)
                .load(data.thumbnail)
                .transform(
                    MultiTransformation(
                        RoundedCornersTransformation(owner.dip(CORNER_RADIUS), 0),
                        CropSquareTransformation()
                    )
                )
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(thumbnail)
        }
        contentTitle?.text = data.sourceTitle.replaceNewLineAsSpanned()
        if (data.utsSignature != SIGNATURE_UTS_MEDITATION_TRAINING) {
            contentPlayTime?.text = data.playTime.millisToFormatString(DTP_MM_SS)
            contentClock?.isVisible = true
        } else {
            contentPlayTime?.isVisible = false
            contentClock?.isVisible = false
        }
    }

    companion object {
//        const val CORNER_RADIUS: Float = 10.5f
        const val CORNER_RADIUS: Float = 20.5f
    }
}