package omnifit.mindcare.training.component

import android.graphics.Paint
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.screen.*
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.CircularProgressBar
import omnifit.mindcare.training.view.circularProgressBar
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber

class AnalyzingViewComponent<in T : UIScreen>(val category: ServiceCategory) : AnkoComponent<T> {
    override fun createView(ui: AnkoContext<T>): View = with(ui) {
        constraintLayout {
            id = R.id.screen_inner_widget_id_root_container

            // 불투명도 60% 오버랩 레이어
            view {
                id = R.id.screen_inner_widget_id_01
                backgroundColorResource = R.color.x_000000_op60

            }.lparams(matchParent, matchParent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            nonSpacingTextView {
                id = R.id.screen_inner_widget_id_02
                typeface = Font.kopubDotumBold
                textResource = when (category) {
                    ServiceCategory.MEDITATIONS -> R.string.meditation_content_screen_010
                    ServiceCategory.HEALING_MUSIC -> R.string.healing_music_content_screen_010
                    ServiceCategory.MEASUREMENT -> R.string.measurement_screen_070
                }
                textSize = 43.0f
                textColorResource = R.color.x_ffffff
                letterSpacing = -0.01f
                includeFontPadding = false
                gravity = Gravity.CENTER
            }.lparams(wrapContent, wrapContent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                topMargin = dip(196.0f)
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
            }

            circularProgressBar {
                id = R.id.screen_inner_widget_id_03
                progressBackgroundColor = ContextCompat.getColor(context, R.color.x_616161)
                progressBackgroundStroke = dip(2.7f).toFloat()
                progressColor = ContextCompat.getColor(context, R.color.x_2da3f5)
                progressStroke = dip(10.8f).toFloat()
                progressStrokeCap = Paint.Cap.ROUND
                progressMax = 100
                progress = 100
                isAnimatable = true
                progressDuration = MEASUREMENT_ANALYZING_PROGRESS_DURATION
                progressInterpolator = LinearInterpolator()
                progressListener = object : CircularProgressBar.ProgressListener {
                    override fun onProgressStart() {
                    }

                    override fun onProgressEnd() {
                        when (category) {
                            ServiceCategory.MEDITATIONS -> {
//                                MeditationContentResultScreen().withArguments(
//                                    Pair(SCREEN_ARGUMENTS_RESULT_TYPE, RESULT_MEDITATION),
//                                    Pair(SCREEN_ARGUMENTS_MEDITATION_CONTENT_TITLE, (owner as MeditationContentScreen).meditationContent.sourceTitle)
//                                )
                                MeditationContentResultScreen().withArguments(
                                    Pair(SCREEN_ARGUMENTS_RESULT_TYPE, RESULT_MEDITATION)
                                )
                            }
                            ServiceCategory.HEALING_MUSIC -> {
//                                HealingMusicContentResultScreen().withArguments(
//                                    Pair(SCREEN_ARGUMENTS_HEALING_CONTENT_TITLE, (owner as HealingMusicContentScreen).playlist[(owner as HealingMusicContentScreen).selectedPosition].sourceTitle)
//                                )
                                HealingMusicContentResultScreen()
                            }
                            else -> MeasurementResultScreen().also {
                                (owner as MeasurementScreen).updateProgressStep(PROGRESS_STEP_MEASUREMENT_READY)
                            }
                        }.let { resultScreen ->
                            owner.screenTo(resultScreen)
                        }
                    }

                    override fun onProgressCancel() {
                    }
                }
                progressMessage.textResource = when (category) {
                    ServiceCategory.MEDITATIONS -> R.string.meditation_content_screen_020
                    ServiceCategory.HEALING_MUSIC -> R.string.healing_music_content_screen_020
                    ServiceCategory.MEASUREMENT -> R.string.measurement_screen_080
                }
                progressMessage.letterSpacing = -0.03f
                progressMessage.textSize = 25.3f
                progressMessage.typeface = Font.kopubDotumMedium
                progressMessage.textColorResource = R.color.x_ffffff
                progressMessage.gravity = Gravity.CENTER
            }.lparams(dip(295.3f), dip(295.3f)) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToBottom = R.id.screen_inner_widget_id_02
                topMargin = dip(56.7f)
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                horizontalBias = 0.5f
            }
        }.applyRecursively { v ->
            (v as? ConstraintLayout)?.layoutParams =
                ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
    }
}