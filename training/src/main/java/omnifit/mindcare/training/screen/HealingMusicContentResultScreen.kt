package omnifit.mindcare.training.screen

import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import omnifit.commons.core.AnalysisAlgorithm
import omnifit.commons.core.DTP_MM_SS
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.millisToFormatString
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.configuration.HealingMusicConfiguration
import omnifit.mindcare.training.helper.ContentDataHelper
import omnifit.mindcare.training.replaceNewLineAsSpace
import omnifit.mindcare.training.screen.pop.lineChart
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.DeepTrainingDurationView
import omnifit.mindcare.training.view.deepTrainingDurationGraph
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

class HealingMusicContentResultScreen : UIScreen() {

    val result: HealingMusicConfiguration? by lazy {
        HealingMusicConfiguration.find(requireContext())
    }

    private val meditationProgramTitle: String by lazy {
        ContentDataHelper.findPlaybackContent(result?.micSignature ?: "RLX000", result?.sequence ?: 0).title
    }

    private val healingTitle: String by lazy {
//        arguments?.run {
//            getString(SCREEN_ARGUMENTS_HEALING_CONTENT_TITLE)
//        } ?: ""

        ContentDataHelper.findAudioContent(result?.micSignature ?: "BSC000", result?.sequence ?: 0).title
    }

    private var deepTrainingDuration: DeepTrainingDurationView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.shape_bg_main

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_home_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        performBackPressed()
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                nonSpacingTextView(R.string.healing_music_content_result_screen_010) {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumMedium
                    textSize = 32.0f
                    textColorResource = R.color.x_2e2e2e
                    letterSpacing = -0.03f
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 힐링 중 뇌파변화
                cardView {
                    id = R.id.screen_widget_id_03
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_02

                    constraintLayout {

                        // 타이틀
                        nonSpacingTextView(R.string.healing_music_content_result_screen_010) {
                            id = R.id.screen_inner_widget_id_01
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_ffffff
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(38.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(38.0f)
                        }

                        // 깊은 뇌파 구간
                        view {
                            backgroundColorResource = R.color.x_4ab6ff
                        }.lparams(dip(22.7f), dip(4.7f)) {
                            topToTop = R.id.screen_inner_widget_id_02
                            endToStart = R.id.screen_inner_widget_id_02
                            marginEnd = dip(10.7f)
                            bottomToBottom = R.id.screen_inner_widget_id_02
                        }

                        nonSpacingTextView(R.string.meditation_content_result_screen_050) {
                            id = R.id.screen_inner_widget_id_02
                            typeface = Font.kopubDotumMedium
                            letterSpacing = -0.1f
                            textSize = 16.0f
                            textColorResource = R.color.x_ffffff
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_01
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(41.3f)
                            bottomToBottom = R.id.screen_inner_widget_id_01
                        }

                        lineChart {
                            backgroundResource = R.drawable.img_bg_graph_02

                            setViewPortOffsets(0.0f, 0.0f, 0.0f, 0.0f)
                            setDrawGridBackground(false)
                            description.isEnabled = false
                            legend.isEnabled = false
                            setPinchZoom(false)
                            setScaleEnabled(false)

                            setTouchEnabled(true)
                            isDragXEnabled = true
                            isDragYEnabled = false

                            axisRight.isEnabled = false

                            xAxis.setDrawAxisLine(false)
                            xAxis.setDrawLabels(false)
                            xAxis.setDrawGridLines(false)

                            axisLeft.setDrawAxisLine(false)
                            axisLeft.setDrawLabels(false)
                            axisLeft.setDrawGridLines(false)
                            axisLeft.labelCount = 11
                            axisLeft.axisMaximum = 10.0f
                            axisLeft.axisMinimum = 0.0f

                            result?.micGraphValues
                                ?.let { vs ->
                                    doubleArrayOf().fromJson(vs).apply {
                                        setScaleMinima(size.toFloat().div(90.0f), 0.0f)
                                    }
                                }?.mapIndexed { idx, v ->
                                    if (result?.micSignature == SIGNATURE_MIC_BRAIN_ACTIVITY) {
                                        Entry(
                                            idx.toFloat(), when (AnalysisAlgorithm.transformSef90HzTo11pValue(v)) {
                                                0.0 -> 0.0f
                                                1.0 -> 1.0f
                                                2.0 -> 2.0f
                                                3.0 -> 3.0f
                                                4.0 -> 8.0f
                                                5.0 -> 9.0f
                                                6.0 -> 10.0f
                                                7.0 -> 4.0f
                                                8.0 -> 5.0f
                                                9.0 -> 6.0f
                                                else -> 7.0f
                                            }
                                        )
                                    } else Entry(idx.toFloat(), v.toFloat())
                                }?.let {
                                    LineDataSet(it, null).apply {
                                        mode = LineDataSet.Mode.CUBIC_BEZIER
                                        cubicIntensity = 0.2f
                                        setDrawCircles(false)
                                        setDrawHighlightIndicators(false)
                                        setDrawValues(false)
                                    }
                                }?.let {
                                    LineData(it)
                                }?.let {
                                    data = it
                                }
                        }.lparams(dip(1069.3f), dip(141.3f)) {
                            startToStart = R.id.screen_inner_widget_id_01
                            topToBottom = R.id.screen_inner_widget_id_01
                            topMargin = dip(38.0f)
                            endToEnd = R.id.screen_inner_widget_id_02
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(1152.0f), dip(276.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(46.0f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                // 힐링결과
                cardView {
                    id = R.id.screen_widget_id_04
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.healing_music_content_result_screen_030) {
                            id = R.id.screen_inner_widget_id_03
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(32.0f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(36.7f)
                        }

                        // 측정 전 스트레스
                        nonSpacingTextView(R.string.healing_music_content_result_screen_040) {
                            id = R.id.screen_inner_widget_id_04
                            typeface = Font.kopubDotumMedium
                            textSize = 19.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(118.0f)
                            topToBottom = R.id.screen_inner_widget_id_03
                            topMargin = dip(21.3f)
                        }
                        // 스트레스 지수
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_05
                            text = if (result?.rateOfChange!! >= 0) result?.afterScore.toString() else result?.beforeScore.toString()
                            typeface = Font.latoBold
                            textSize = 30.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_04
                            marginStart = dip(16.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_04
                        }
                        nonSpacingTextView {
                            text = "%"
                            typeface = Font.latoRegular
                            textSize = 20.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_05
                            marginStart = dip(5.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_04
                        }

                        imageView(R.drawable.ic_result_arrow) {
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_04
                            topMargin = dip(6.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 측정 후 스트레스
                        nonSpacingTextView(R.string.healing_music_content_result_screen_050) {
                            id = R.id.screen_inner_widget_id_06
                            typeface = Font.kopubDotumMedium
                            textSize = 19.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_id_04
                            topToBottom = R.id.screen_inner_widget_id_04
                            topMargin = dip(37.3f)
                        }
                        // 스트레스 지수
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_07
                            text = result?.afterScore.toString()
                            typeface = Font.latoBold
                            textSize = 30.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_06
                            marginStart = dip(16.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_06
                        }
                        nonSpacingTextView {
                            text = "%"
                            typeface = Font.latoRegular
                            textSize = 20.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_07
                            marginStart = dip(5.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_06
                        }

                        // 힐링 결과
                        if (result?.rateOfChange!! >= 0) {
                            nonSpacingTextView {
                                textResource = R.string.healing_music_content_result_screen_080
                                typeface = Font.kopubDotumMedium
                                textSize = 23.0f
                                textColorResource = R.color.x_2e2e2e
                                letterSpacing = -0.04f
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_06
                                topMargin = dip(36.7f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            }
                        } else {
                            nonSpacingTextView(R.string.healing_music_content_result_screen_060) {
                                id = R.id.screen_inner_widget_id_08
                                typeface = Font.kopubDotumMedium
                                textSize = 23.0f
                                textColorResource = R.color.x_2e2e2e
                                letterSpacing = -0.04f
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_06
                                topMargin = dip(36.7f)
                                endToStart = R.id.screen_inner_widget_id_09
                                horizontalChainStyle = ConstraintLayout.LayoutParams.CHAIN_PACKED
                                horizontalBias = 0.5f
                            }

                            nonSpacingTextView {
                                id = R.id.screen_inner_widget_id_09
                                typeface = Font.latoBold
                                textSize = 48.0f
                                textColorResource = R.color.x_32a0f6
                                lines = 1
                                includeFontPadding = false
                                result?.rateOfChange?.toString()?.let { rate ->
                                    text = rate
                                    paint.shader = LinearGradient(
                                        0.0f,
                                        0.0f,
                                        paint.measureText(rate),
                                        textSize,
                                        Color.parseColor("#ff6c80ff"),
                                        Color.parseColor("#ff2ba4f5"),
                                        Shader.TileMode.CLAMP
                                    )
                                }
                            }.lparams(wrapContent, wrapContent) {
                                startToEnd = R.id.screen_inner_widget_id_08
                                marginStart = dip(12.0f)
                                endToStart = R.id.screen_inner_widget_id_10
                                marginEnd = dip(1.5f)
                                bottomToBottom = R.id.screen_inner_widget_id_08
                            }

                            nonSpacingTextView {
                                id = R.id.screen_inner_widget_id_10
                                text = "%"
                                typeface = Font.latoRegular
                                textSize = 34.0f
                                textColorResource = R.color.x_32a0f6
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToEnd = R.id.screen_inner_widget_id_09
                                marginStart = dip(5.0f)
                                endToStart = R.id.screen_inner_widget_id_11
                                bottomToBottom = R.id.screen_inner_widget_id_08
                            }

                            nonSpacingTextView(R.string.healing_music_content_result_screen_070) {
                                id = R.id.screen_inner_widget_id_11
                                typeface = Font.kopubDotumMedium
                                textSize = 23.0f
                                textColorResource = R.color.x_2e2e2e
                                letterSpacing = -0.04f
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToEnd = R.id.screen_inner_widget_id_10
                                marginStart = dip(11.3f)
                                topToTop = R.id.screen_inner_widget_id_08
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomToBottom = R.id.screen_inner_widget_id_08
                            }
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(408.7f), dip(244.7f)) {
                    startToStart = R.id.screen_widget_id_03
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(31.3f)
                }

                // 전체 명상시간
                cardView {
                    id = R.id.screen_widget_id_05
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.healing_music_content_result_screen_090) {
                            id = R.id.screen_inner_widget_id_12
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(32.0f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(36.7f)
                        }
                        // 명상시간
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_13
                            text = result?.totalHealingTime?.millisToFormatString(DTP_MM_SS)
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_12
                            topMargin = dip(35.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 프로그램 타이틀
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_14
                            text = resources.getString(R.string.healing_music_content_result_screen_120)
//                            text = meditationProgramTitle.replaceNewLineAsSpace()
                            typeface = Font.kopubDotumMedium
                            textSize = 17.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(36.0f)
                            topToBottom = R.id.screen_inner_widget_id_13
                            topMargin = dip(39.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(36.0f)
                            horizontalBias = 0.0f
                        }
                        // 콘텐츠 타이틀
                        nonSpacingTextView {
                            text = healingTitle.replaceNewLineAsSpace()
                            typeface = Font.kopubDotumMedium
                            textSize = 17.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_id_14
                            topToBottom = R.id.screen_inner_widget_id_14
                            topMargin = dip(9.3f)
                            endToEnd = R.id.screen_inner_widget_id_14
                            horizontalBias = 0.0f
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(300.0f), dip(244.7f)) {
                    startToEnd = R.id.screen_widget_id_04
                    endToStart = R.id.screen_widget_id_06
                    bottomToBottom = R.id.screen_widget_id_04
                }

                // 깊은 명상시간
                cardView {
                    id = R.id.screen_widget_id_06
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.healing_music_content_result_screen_100) {
                            id = R.id.screen_inner_widget_id_15
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(32.0f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(36.7f)
                        }
                        // 명상시간
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_16
                            text = result?.deepHealingTime?.millisToFormatString(DTP_MM_SS)
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_15
                            topMargin = dip(35.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 집중시간
                        deepTrainingDuration =
                            deepTrainingDurationGraph {
                                totalTime = result?.totalHealingTime ?: 0
                                deepTrainingTime = result?.deepHealingTime ?: 0
//                                deepTrainingTime = 60000
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_16
                                topMargin = dip(15.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(376.0f), dip(244.7f)) {
                    endToEnd = R.id.screen_widget_id_03
                    bottomToBottom = R.id.screen_widget_id_04
                }

                // 주의 사항
                imageView(R.drawable.ic_information) {
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_07
                    endToStart = R.id.screen_widget_id_07
                    marginEnd = dip(10.7f)
                    bottomToBottom = R.id.screen_widget_id_07
                }

                nonSpacingTextView(R.string.healing_music_content_result_screen_110) {
                    id = R.id.screen_widget_id_07
                    typeface = Font.kopubDotumMedium
                    textSize = 19.0f
                    textColorResource = R.color.x_9196a5
                    letterSpacing = -0.1f
                    includeFontPadding = false
                    lines = 1
                }.lparams(wrapContent, wrapContent) {
                    topToBottom = R.id.screen_widget_id_06
                    topMargin = dip(38.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(60.0f)
                }
            }
        }.view

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}