package omnifit.mindcare.training.screen

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import com.tedpark.tedpermission.rx2.TedRx2Permission
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.support.v4.UI
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

class SplashScreen : UIScreen() {
    init {
        isRootScreen = true
    }

    private var progressBaseDataToUpdate: ProgressBar? = null
    private var progressMessageToUpdate: NonSpacingTextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.layer_list_splash

                progressBaseDataToUpdate = horizontalProgressBar {
                    id = R.id.screen_widget_id_01
                    progressDrawable = ContextCompat.getDrawable(context, R.drawable.layer_list_horizontal_progress_bar)
                    max = 102
                }.lparams(dip(400.0f), dip(4.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomMargin = dip(100.0f)
                    horizontalBias = 0.5f
                }

                progressMessageToUpdate = nonSpacingTextView(R.string.splash_screen_010) {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumMedium
                    textSize = 13.0f
                    textColorResource = R.color.x_1c1c1c
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_01
                    endToEnd = R.id.screen_widget_id_01
                    bottomToTop = R.id.screen_widget_id_01
                    bottomMargin = dip(10.0f)
                    horizontalBias = 0.5f
                }
            }
        }.view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().window.setBackgroundDrawable(ColorDrawable(Color.WHITE))
        requirePermission()
    }

    override fun onBackPressed(): Boolean {
        alert(requireContext(), R.string.notifications_010)
        return appFinish()
    }

    @SuppressLint("CheckResult")
    private fun requirePermission() {
        if (TedRx2Permission.isGranted(
                context,
//                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        ) {
            onPermissionGranted()
        } else {
//            MaterialDialog(requireContext())
//                .show {
//                    customView(
//                        view = PermissionGuideComponent<MaterialDialog>()
//                            .createView(AnkoContext.create(requireContext(), this)),
//                        noVerticalPadding = true
//                    )
//                    cancelable(false)
//                    cancelOnTouchOutside(false)
//                    setOnDismissListener {
//                        TedRx2Permission.with(context)
//                            .setPermissions(
////                                Manifest.permission.READ_PHONE_STATE,
//                                Manifest.permission.ACCESS_COARSE_LOCATION,
//                                Manifest.permission.READ_EXTERNAL_STORAGE,
//                                Manifest.permission.WRITE_EXTERNAL_STORAGE
//                            )
//                            .request()
//                            .subscribeOn(Schedulers.io())
//                            .bindUntilEvent(this@SplashScreen, Lifecycle.Event.ON_DESTROY)
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(
//                                { r ->
//                                    if (r.deniedPermissions != null) onPermissionDenied(r.deniedPermissions)
//                                    else onPermissionGranted()
//                                },
//                                { e -> Timber.e(e) }
//                            )
//                    }
//                }
            TedRx2Permission.with(context)
                .setPermissions(
//                                Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                .request()
                .subscribeOn(Schedulers.io())
                .bindUntilEvent(this@SplashScreen, Lifecycle.Event.ON_DESTROY)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { r ->
                        if (r.deniedPermissions != null) onPermissionDenied(r.deniedPermissions)
                        else onPermissionGranted()
                    },
                    { e -> Timber.e(e) }
                )
        }
    }

    private fun onPermissionGranted() {
        checkBaseData()
    }

    private fun onPermissionDenied(permissions: List<String>) {
    }

    private var finalizer: Disposable? = null

    private fun checkBaseData() {
        ContentHelper.requireBaseData(
            requireContext()
        ) { progress ->
            progressBaseDataToUpdate?.progress = progress.roundToInt()

            Timber.e("---> json progress : ${progressBaseDataToUpdate?.progress}")

            if(progress == 100.0f) {
                progressMessageToUpdate?.textResource = R.string.splash_screen_020

                // json으로 부터 리소스 준비
                ContentHelper.initLoad(requireContext()) { _progress ->
                    progressBaseDataToUpdate?.progress = _progress
                    if (_progress == 102) {
                        finalizer?.dispose()
                        finalizer = Single.timer(1L, TimeUnit.SECONDS)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(
                                {
                                    screenTo(MainScreen())
                                    finalizer?.dispose()
                                    finalizer = null
                                },
                                { e -> Timber.d(e) }
                            )
                    }
                }
            }
        }
    }
}