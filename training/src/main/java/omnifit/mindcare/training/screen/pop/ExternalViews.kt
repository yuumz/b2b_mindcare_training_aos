package omnifit.mindcare.training.screen.pop

import android.view.ViewManager
import com.github.mikephil.charting.charts.LineChart
import org.jetbrains.anko.custom.ankoView

inline fun ViewManager.lineChart(theme: Int = 0, init: LineChart.() -> Unit): LineChart {
    return ankoView({ LineChart(it) }, theme, init)
}