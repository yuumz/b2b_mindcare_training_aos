package omnifit.mindcare.training.screen

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.constraintlayout.widget.ConstraintLayout
import omnifit.commons.core.doseNothing
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.DATE_TIME_PATTERN_DEFAULT
import omnifit.mindcare.training.common.SCREEN_ARGUMENTS_GAME_CONTENT_POSITION
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.headset.fx2.MEASUREMENT_DURATION_INFINITE
import omnifit.mindcare.training.headset.fx2.RESULT_ITEM_CONCENTRATION
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.helper.GameSource
import omnifit.mindcare.training.screen.pop.SensorsAttachmentPopScreen
import omnifit.mindcare.training.screenTo
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import timber.log.Timber

class GameScreen : UIScreen() {
    private val defaultGameContentUri = "file:///android_asset/game/4/index.html"

    private val gameContent: GameSource by lazy {
        arguments?.getInt(SCREEN_ARGUMENTS_GAME_CONTENT_POSITION, 0)?.let { position ->
            var gamePosition: Int = 0
            if (position > 4) ++gamePosition
            ContentHelper.gamelist()[gamePosition]
        } ?: ContentHelper.gamelist()[0]
    }

    private val gameContentUri: String by lazy {
        arguments?.run { "file:///android_asset/game/${gameContent.order}/index.html" } ?: defaultGameContentUri
    }

    var gameView: WebView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = UI {
        constraintLayout {
            backgroundResource = R.drawable.shape_bg_game

            gameView = webView {
                backgroundColor = Color.TRANSPARENT
            }.lparams(matchParent, matchParent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            // 홈으로 or 이전으로 버튼 위치
            imageButton(R.drawable.selector_return_back_wt) {
                id = R.id.screen_widget_id_01
                backgroundColor = Color.TRANSPARENT
                onClick {
                    performBackPressed()
                }
            }.lparams(dip(164.0f), dip(56.0f)) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                marginStart = dip(42.7f)
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                topMargin = dip(43.0f)
            }
        }

    }.view

    var concentrationCache: MutableList<Double> = mutableListOf()

    var isGameFinish: Boolean = false

    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        if (isEnter) {
            launchGame(this, gameContentUri)

            Headset.observeSensorsAttachmentStatusChange(this@GameScreen) { status ->
                Timber.d("---> 센서 부착 상태 : $status , $gameBeginTime, $isGameFinish")
                when (status) {
                    SensorsAttachmentStatus.DETACHED -> {
                        if (!gameBeginTime.isNullOrEmpty() && !isGameFinish) {
                            Timber.i("---> 게임 중지 팝업 오픈 ")
                            SensorsAttachmentPopScreen().run {
                                positiveAction = {
                                    isGameFinish = true
                                    releaseGame(CancelCause.Detached(SensorsAttachmentStatus.DETACHED))
                                    screenTo(GameListScreen())

                                }
                                show(this@GameScreen.fragmentManager)
                            }
                        }
                    }
                    else -> doseNothing()
                }
            }
        }
    }

    override fun onBackPressed(): Boolean {
        releaseGame(CancelCause.Forced)
        return screenTo(GameListScreen())
    }

    //    override fun onDestroy() {
    //        releaseGame(CancelCause.Forced)
    //        super.onDestroy()
    //    }

    var gameStage: String? = null
    var handler: Handler = Handler()

    fun launchGame(owner: GameScreen, gameContentHtmlData: String) {
        //        gameView?.setLayerType(View.LAYER_TYPE_SOFTWARE, null) // 애니메이션 없는 화면을 더 선명하게 보이기하기위함.

        gameView!!.settings.run {
            javaScriptEnabled = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = false
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        }

        gameView?.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                view.visibility = View.INVISIBLE
                view.loadUrl("javascript:onCreate()")
            }

            override fun onPageFinished(view: WebView, url: String?) {
                super.onPageFinished(view, url)
                view.loadUrl("javascript:onStart()")
                handler.postDelayed({ view.visibility = View.VISIBLE }, 100L)
            }
        }

        // 익명으로 브릿지 인터페이스 생성
        gameView?.addJavascriptInterface(object {
            /**
             * 웹게임 종료시 Web->App 호출.
             */
            @JavascriptInterface
            fun onDestroy(data: String) {

                Timber.i("게임종료 : $data")

                handler.post { owner.onBackPressed() }
            }

            /**
             * 그래픽 가속 사용여부 결정.
             */
            @JavascriptInterface
            fun gameStart(data: String) {

                //웹 게임 시작시 애니메이션 많은 화면 나올시 Setting 해주어야 애니메이션 부드러움.
                handler.post { gameView?.setLayerType(View.LAYER_TYPE_HARDWARE, null) }
            }

            /***
             * Javascript 에서 게임Play 시작시 호출
             * data : 스테이지 단계 Data --> Easy : 1 / Normal : 2 / Hard : 3
             ***/
            @JavascriptInterface
            fun onGameStart(stageFromWeb: String) {

                Timber.i("스테이지 : $stageFromWeb")

                gameStage = stageFromWeb

                isGameFinish = false

                Headset.startMeasurement(this@GameScreen, MEASUREMENT_DURATION_INFINITE, EyesStatus.OPENED) { status ->
                    when (status) {
                        is MeasurementStatus.Start -> {
                            gameBeginTime = DateTime.now().toString(DATE_TIME_PATTERN_DEFAULT)
                            concentrationCache = mutableListOf()
                        }
                        is MeasurementStatus.Measuring -> {
                            if (status.values.isNotEmpty()) {
                                status.values[RESULT_ITEM_CONCENTRATION].let { d ->
                                    if (d >= 0) postValueToWeb(d.toInt().toString().apply { Timber.d("WEB POSTING($this)") })
                                    concentrationCache.add(d)
                                }
                            }
                        }
                        is MeasurementStatus.Stop -> doseNothing()
                        is MeasurementStatus.Cancel -> doseNothing()
                    }
                }
            }

            /***
             * Web에서 게임종료시 app 함수 호출. ( 실 게임 time 종료 )
             * data : 게임총 점수 . ex: 590
             ***/
            @JavascriptInterface
            fun onGameEnd(successCountFromWeb: String) {
                isGameFinish = true

                Timber.i("획득 성공 횟수 : $successCountFromWeb")
                Headset.stopMeasurement()
            }
        }, "omnigame")

        gameView?.loadUrl(owner.gameContentUri)
    }

    private fun releaseGame(cancel: CancelCause) {
        handler.post {
            if (Headset.isMeasuring()) Headset.stopMeasurement(cancel)
            gameView?.removeAllViews()
            gameView?.destroy()
        }
    }

    private fun postValueToWeb(v: String) {
        gameView?.loadUrl("javascript:callJS('$v')")
    }

    var gameBeginTime: String? = null

    fun gameEndTime(elapsedTime: Int): String {
        return DateTime.parse(gameBeginTime, DateTimeFormat.forPattern(DATE_TIME_PATTERN_DEFAULT)).toLocalDateTime().plusSeconds(elapsedTime).toString(DATE_TIME_PATTERN_DEFAULT)
    }
}