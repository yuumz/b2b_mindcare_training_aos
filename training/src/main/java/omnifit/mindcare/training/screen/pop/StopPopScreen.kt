package omnifit.mindcare.training.screen.pop

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.UIPopScreen
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.FRAGMENT_ARGUMENTS_COMMENT
import omnifit.mindcare.training.common.FRAGMENT_ARGUMENTS_MESSAGE
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick

class StopPopScreen : UIPopScreen() {

    lateinit var screenUiComponent: StopPopComponent

    override fun <T : UIPopScreen> T.createScreenUiComponent(): AnkoComponent<T> =
        StopPopComponent().apply { screenUiComponent = this } as AnkoComponent<T>

    var arguments_: Bundle? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments_ = arguments
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window.setWindowAnimations(R.style.PopScreenAnimation)
    }
}

class StopPopComponent : AnkoLifecycleComponent<StopPopScreen> {

    var comment: NonSpacingTextView? = null
    var message: NonSpacingTextView? = null

    override fun createView(ui: AnkoContext<StopPopScreen>): View = with(ui) {
        relativeLayout {
            backgroundColorResource = R.color.x_000000_op15

            constraintLayout {
                backgroundResource = R.drawable.shape_round_7_bg_pop

                comment = nonSpacingTextView {
                    id = R.id.screen_widget_id_01
                    typeface = Font.kopubDotumMedium
                    textSize = 21.0f
                    textColorResource = R.color.x_898989
                    includeFontPadding = false
                    lines = 2
                    letterSpacing = -0.01f
                    setLineSpacing(11.0f, 1.0f)
                }.lparams(dip(0.0f), wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(50.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(53.3f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(50.7f)
                }

                message = nonSpacingTextView {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumBold
                    textSize = 28.0f
                    textColorResource = R.color.x_1c1c1c
                    lines = 1
                    letterSpacing = -0.01f
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_01
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }

                imageButton(R.drawable.selector_confirm) {
                    id = R.id.screen_widget_id_03
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        owner.buttonWhich = DialogInterface.BUTTON_POSITIVE
                        owner.dismiss()
                    }
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(42.7f)
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomMargin = dip(30.0f)
                }

                imageButton(R.drawable.selector_cancel) {
                    id = R.id.screen_widget_id_04
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        owner.buttonWhich = DialogInterface.BUTTON_NEGATIVE
                        owner.dismiss()
                    }
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_03
                    endToStart = R.id.screen_widget_id_03
                    bottomToBottom = R.id.screen_widget_id_03
                }

            }.lparams(dip(514.0f), dip(297.3f)) {
                gravity = Gravity.CENTER
            }
        }
    }

    override fun onViewCreated(owner: StopPopScreen) {
        owner.arguments_?.let {
            comment?.textResource = it.getInt(FRAGMENT_ARGUMENTS_COMMENT, -1)
            message?.textResource = it.getInt(FRAGMENT_ARGUMENTS_MESSAGE, -1)
        }
    }
}