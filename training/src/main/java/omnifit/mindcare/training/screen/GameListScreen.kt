package omnifit.mindcare.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextSwitcher
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearSnapHelper
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.commons.ui.anko.AnkoBindableComponentAdapter
import omnifit.commons.ui.anko.AnkoBindableComponentHolder
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.batteryLevelChangeObserver
import omnifit.mindcare.training.common.SCREEN_ARGUMENTS_GAME_CONTENT_POSITION
import omnifit.mindcare.training.component.GameContentItemComponent
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.helper.GameSource
import omnifit.mindcare.training.screen.pop.HeadsetPopScreen
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.NonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.withArguments
import travel.ithaka.android.horizontalpickerlib.PickerLayoutManager
import kotlin.properties.Delegates

class GameListScreen : UIScreen() {

    private val gameList: MutableList<GameSource> by lazy {
        ContentHelper.gamelist().toMutableList()
    }

    private var selectedPosition: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            title?.setText(gameList[n].sourceTitle)
            subTitle?.setText(gameList[n].explanation)
        }
    }

    private var title: TextSwitcher? = null
    private var subTitle: TextSwitcher? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundColorResource = R.color.x_ffffff

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_back_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        performBackPressed()
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 밧데리잔량
                constraintLayout {
                    id = R.id.screen_widget_id_02
                    backgroundResource = R.drawable.shape_round_28_op04_bg_headset

                    // 헤드셋 이미지
                    imageView(R.drawable.ic_headset_bk) {
                        id = R.id.screen_inner_widget_id_01
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(25.3f)
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }
                    // 헤드셋 밧데리 잔량
                    textView {
                        id = R.id.screen_inner_widget_id_02
                        typeface = Font.latoRegular
                        textSize = 21.3f
                        textColorResource = R.color.x_303030
                        lines = 1
                        includeFontPadding = false
                        batteryLevelChangeObserver(owner)
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_01
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        horizontalBias = 0.3f
                        verticalBias = 0.5f
                    }
                }.lparams(dip(137.3f), dip(56.0f)) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(106.7f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 설정
                imageButton(R.drawable.selector_setting_bk) {
                    id = R.id.screen_widget_id_03
                    scaleType = ImageView.ScaleType.CENTER
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        HeadsetPopScreen().run {
                            show(this@GameListScreen.fragmentManager)
                        }
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_02
                    marginStart = dip(28.0f)
                    topToTop = R.id.screen_widget_id_02
                    bottomToBottom = R.id.screen_widget_id_02
                }

                @Suppress("UNCHECKED_CAST")
                recyclerView {
                    id = R.id.screen_widget_id_04
                    clipToPadding = false
                    setPadding(dip(384.0f), 0, dip(381.3f), 0)
                    layoutManager = PickerLayoutManager(context, PickerLayoutManager.HORIZONTAL, false).apply {
                        isChangeAlpha = true
                        scaleDownBy = 0.1f
                        scaleDownDistance = 0.8f
                        setOnScrollStopListener { v ->
                            (v.tag as? GameSource)?.let { source ->
                                selectedPosition = gameList.indexOf(source)
                            }
                        }
                    }
                    LinearSnapHelper().attachToRecyclerView(this)
                    setHasFixedSize(true)      // 정적 리스트 데이터 성능용
                    setItemViewCacheSize(gameList.size) // 정적 리스트 캐시 사이즈
                    adapter = GameContentListAdapter(gameList, this@GameListScreen)
                    smoothScrollBy(1, 0)
                }.lparams(matchConstraint, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(44.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                title = textSwitcher {
                    id = R.id.screen_widget_id_05
                    setFactory {
                        NonSpacingTextView(context).apply {
                            typeface = Font.kopubDotumBold
//                            text = gameList[0].sourceTitle
                            textSize = 35.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            gravity = Gravity.CENTER
                            layoutParams = FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            )
                        }
                    }
                    measureAllChildren = false
                    inAnimation = fadeIn
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_04
                    topMargin = dip(40.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                subTitle = textSwitcher {
                    id = R.id.screen_widget_id_06
                    setFactory {
                        NonSpacingTextView(context).apply {
                            typeface = Font.kopubDotumMedium
//                            text = gameList[0].explanation
                            textSize = 24.0f
                            textColorResource = R.color.x_a8a8a8
                            includeFontPadding = false
                            gravity = Gravity.CENTER
                            layoutParams = FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            )
                        }
                    }
                    measureAllChildren = false
                    inAnimation = fadeIn
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_05
                    topMargin = dip(17.3f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }
            }.apply {
                selectedPosition = 0
            }
        }.view

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}

class GameContentListAdapter constructor(
    items: MutableList<GameSource>,
    owner: GameListScreen
) :
    AnkoBindableComponentAdapter<MutableList<GameSource>, GameListScreen, GameContentListAdapter.ComponentHolder>(
        items,
        owner
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(GameContentItemComponent(parent.context, owner)) { _, content ->
            when {
                !Headset.isConnected() -> HeadsetPopScreen().run { show(owner.fragmentManager) }
                !Headset.isSensorsAttached() -> owner.alert(
                    parent.context,
                    R.string.notifications_020,
                    R.color.x_000000_op60
                )
                else -> {
                    owner.screenTo(
                        GameScreen().withArguments(
                            Pair(
                                SCREEN_ARGUMENTS_GAME_CONTENT_POSITION,
                                content.order
                            )
                        )
                    )
                }
            }
        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<GameSource, GameListScreen>,
        action: (AnkoBindableComponent<GameSource, GameListScreen>, GameSource) -> Unit
    ) : AnkoBindableComponentHolder<GameSource, GameListScreen>(component) {
        init {
            (component as? GameContentItemComponent)?.run {
                content?.onClick { action(component, items[adapterPosition]) }
            }
        }
    }
}