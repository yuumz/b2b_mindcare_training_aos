package omnifit.mindcare.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import omnifit.commons.core.AnalysisAlgorithm
import omnifit.commons.core.DTP_MM_SS
import omnifit.commons.core.json.moshi.fromJson
import omnifit.commons.core.millisToFormatString
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.configuration.MeditationsConfiguration
import omnifit.mindcare.training.helper.ContentDataHelper
import omnifit.mindcare.training.helper.ContentResultHelper
import omnifit.mindcare.training.replaceNewLineAsSpace
import omnifit.mindcare.training.screen.pop.lineChart
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.DeepTrainingDurationView
import omnifit.mindcare.training.view.deepTrainingDurationGraph
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

class MeditationContentResultScreen : UIScreen() {

    private val resultType: Int by lazy {
        arguments?.run {
            getInt(SCREEN_ARGUMENTS_RESULT_TYPE, RESULT_MEDITATION)
        } ?: RESULT_MEDITATION
    }

    val result: MeditationsConfiguration? by lazy {
        MeditationsConfiguration.find(requireContext())
    }

    private val meditationProgramTitle: String by lazy {
        ContentDataHelper.findPlaybackContent(result?.micSignature ?: "RLX000", result?.sequence ?: 0).title
    }

    private val meditationTitle: String by lazy {
//        arguments?.run {
//            getString(SCREEN_ARGUMENTS_MEDITATION_CONTENT_TITLE)
//        } ?: ""

        ContentDataHelper.findAudioContent(result?.micSignature ?: "RLX000", result?.sequence ?: 0).title
    }

    private var deepTrainingDuration: DeepTrainingDurationView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.shape_bg_main

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_home_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        screenTo(MainScreen())
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                nonSpacingTextView(R.string.meditation_content_result_screen_010) {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumMedium
                    textSize = 32.0f
                    textColorResource = R.color.x_2e2e2e
                    letterSpacing = -0.03f
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 명상 종합 점수 레이어
                cardView {
                    id = R.id.screen_widget_id_03
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 명상 종합 점수 타이틀
                        nonSpacingTextView(R.string.meditation_content_result_screen_020) {
                            id = R.id.screen_inner_widget_id_01
                            typeface = Font.kopubDotumBold
                            textSize = 27.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(38.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        val score = ServiceAlgorithm.calcMeditationScore(
                            result?.deepMeditationTime ?: 0,
                            result?.totalMeditationTime ?: 0
                        )
                        val grade = MeditationIndicator.of(score)

                        // 명상 종합 점수 등급
                        nonSpacingTextView(grade.label) {
                            id = R.id.screen_inner_widget_id_02
                            typeface = Font.latoRegular
                            textSize = 76.0f
                            textColor = grade.color
                            letterSpacing = -0.03f
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_01
                            topMargin = dip(46.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 명상 종합 점수 라벨
                        textView(R.string.meditation_content_result_screen_030) {
                            id = R.id.screen_inner_widget_id_03
                            typeface = Font.latoRegular
                            textSize = 23.0f
                            textColorResource = R.color.x_abafb9
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_02
                            topMargin = dip(38.7f)
                            endToStart = R.id.screen_inner_widget_id_04
                            horizontalChainStyle = ConstraintLayout.LayoutParams.CHAIN_PACKED
                            horizontalBias = 0.5f
                        }

                        // 명상 종합 점수
                        nonSpacingTextView("$score") {
                            id = R.id.screen_inner_widget_id_04
                            typeface = Font.latoRegular
                            textSize = 40.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_03
                            marginStart = dip(16.0f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            bottomToBottom = R.id.screen_inner_widget_id_03
                        }

                        constraintLayout {
                            backgroundResource = R.drawable.shape_bg_result

                            //----------------------------------------------------------

                            // 등급 1 하이라이트
                            constraintLayout {
                                id = R.id.screen_inner_widget_id_05
                                backgroundColor =
                                    if (grade == MeditationIndicator.EXCELLENT) grade.color else Color.TRANSPARENT

                                imageView(R.drawable.ic_meditation_grade_focused) {
                                    isVisible = grade == MeditationIndicator.EXCELLENT
                                }.lparams(wrapContent, wrapContent) {
                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                    marginStart = dip(16.7f)
                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                }
                            }.lparams(matchConstraint, dip(37.3f)) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(0.2f)
                                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                topMargin = dip(30.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(0.2f)
                            }

                            // 등급 1 라벨
                            nonSpacingTextView(MeditationIndicator.EXCELLENT.label) {
                                id = R.id.screen_inner_widget_id_06
                                typeface =
                                    if (grade == MeditationIndicator.EXCELLENT) Font.latoBold else Font.latoRegular
                                textSize = 21.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.EXCELLENT) R.color.x_ffffff else R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_05
                                marginStart = dip(54.7f)
                                topToTop = R.id.screen_inner_widget_id_05
                                bottomToBottom = R.id.screen_inner_widget_id_05
                            }

                            // 등급 1 점수 범위
                            nonSpacingTextView("${MeditationIndicator.EXCELLENT.scoreRange.start} ~ ${MeditationIndicator.EXCELLENT.scoreRange.endInclusive}") {
                                typeface = Font.latoRegular
                                textSize = 22.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.EXCELLENT) R.color.x_ffffff else R.color.x_9196a5
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_06
                                marginStart = dip(160.0f)
                                topToTop = R.id.screen_inner_widget_id_06
                                bottomToBottom = R.id.screen_inner_widget_id_06
                            }

                            //----------------------------------------------------------

                            // 등급 2 하이라이트
                            constraintLayout {
                                id = R.id.screen_inner_widget_id_07
                                backgroundColor =
                                    if (grade == MeditationIndicator.VERY_GOOD) grade.color else Color.TRANSPARENT

                                imageView(R.drawable.ic_meditation_grade_focused) {
                                    isVisible = grade == MeditationIndicator.VERY_GOOD
                                }.lparams(wrapContent, wrapContent) {
                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                    marginStart = dip(16.7f)
                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                }
                            }.lparams(matchConstraint, dip(37.3f)) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(0.2f)
                                topToBottom = R.id.screen_inner_widget_id_05
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(0.2f)
                            }

                            // 등급 2 라벨
                            nonSpacingTextView(MeditationIndicator.VERY_GOOD.label) {
                                id = R.id.screen_inner_widget_id_08
                                typeface =
                                    if (grade == MeditationIndicator.VERY_GOOD) Font.latoBold else Font.latoRegular
                                textSize = 21.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.VERY_GOOD) R.color.x_ffffff else R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_07
                                marginStart = dip(54.7f)
                                topToTop = R.id.screen_inner_widget_id_07
                                bottomToBottom = R.id.screen_inner_widget_id_07
                            }

                            // 등급 2 점수 범위
                            nonSpacingTextView("${MeditationIndicator.VERY_GOOD.scoreRange.start} ~ ${MeditationIndicator.VERY_GOOD.scoreRange.endInclusive}") {
                                typeface = Font.latoRegular
                                textSize = 22.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.VERY_GOOD) R.color.x_ffffff else R.color.x_9196a5
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_08
                                marginStart = dip(160.0f)
                                topToTop = R.id.screen_inner_widget_id_08
                                bottomToBottom = R.id.screen_inner_widget_id_08
                            }

                            //----------------------------------------------------------

                            // 등급 3 하이라이트
                            constraintLayout {
                                id = R.id.screen_inner_widget_id_09
                                backgroundColor =
                                    if (grade == MeditationIndicator.GOOD) grade.color else Color.TRANSPARENT

                                imageView(R.drawable.ic_meditation_grade_focused) {
                                    isVisible = grade == MeditationIndicator.GOOD
                                }.lparams(wrapContent, wrapContent) {
                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                    marginStart = dip(16.7f)
                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                }
                            }.lparams(matchConstraint, dip(37.3f)) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(0.2f)
                                topToBottom = R.id.screen_inner_widget_id_07
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(0.2f)
                            }

                            // 등급 3 라벨
                            nonSpacingTextView(MeditationIndicator.GOOD.label) {
                                id = R.id.screen_inner_widget_id_10
                                typeface = if (grade == MeditationIndicator.GOOD) Font.latoBold else Font.latoRegular
                                textSize = 21.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.GOOD) R.color.x_ffffff else R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_09
                                marginStart = dip(54.7f)
                                topToTop = R.id.screen_inner_widget_id_09
                                bottomToBottom = R.id.screen_inner_widget_id_09
                            }

                            // 등급 3 점수 범위
                            nonSpacingTextView("${MeditationIndicator.GOOD.scoreRange.start} ~ ${MeditationIndicator.GOOD.scoreRange.endInclusive}") {
                                typeface = Font.latoRegular
                                textSize = 22.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.GOOD) R.color.x_ffffff else R.color.x_9196a5
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_10
                                marginStart = dip(160.0f)
                                topToTop = R.id.screen_inner_widget_id_10
                                bottomToBottom = R.id.screen_inner_widget_id_10
                            }

                            //----------------------------------------------------------

                            // 등급 4 하이라이트
                            constraintLayout {
                                id = R.id.screen_inner_widget_id_11
                                backgroundColor =
                                    if (grade == MeditationIndicator.NOT_GOOD) grade.color else Color.TRANSPARENT

                                imageView(R.drawable.ic_meditation_grade_focused) {
                                    isVisible = grade == MeditationIndicator.NOT_GOOD
                                }.lparams(wrapContent, wrapContent) {
                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                    marginStart = dip(16.7f)
                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                }
                            }.lparams(matchConstraint, dip(37.3f)) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(0.2f)
                                topToBottom = R.id.screen_inner_widget_id_09
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(0.2f)
                            }

                            // 등급 4 라벨
                            nonSpacingTextView(MeditationIndicator.NOT_GOOD.label) {
                                id = R.id.screen_inner_widget_id_12
                                typeface =
                                    if (grade == MeditationIndicator.NOT_GOOD) Font.latoBold else Font.latoRegular
                                textSize = 21.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.NOT_GOOD) R.color.x_ffffff else R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_11
                                marginStart = dip(54.7f)
                                topToTop = R.id.screen_inner_widget_id_11
                                bottomToBottom = R.id.screen_inner_widget_id_11
                            }

                            // 등급 4 점수 범위
                            nonSpacingTextView("${MeditationIndicator.NOT_GOOD.scoreRange.start} ~ ${MeditationIndicator.NOT_GOOD.scoreRange.endInclusive}") {
                                typeface = Font.latoRegular
                                textSize = 22.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.NOT_GOOD) R.color.x_ffffff else R.color.x_9196a5
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_12
                                marginStart = dip(160.0f)
                                topToTop = R.id.screen_inner_widget_id_12
                                bottomToBottom = R.id.screen_inner_widget_id_12
                            }

                            //----------------------------------------------------------

                            // 등급 5 하이라이트
                            constraintLayout {
                                id = R.id.screen_inner_widget_id_16
                                backgroundColor =
                                    if (grade == MeditationIndicator.BAD) grade.color else Color.TRANSPARENT

                                imageView(R.drawable.ic_meditation_grade_focused) {
                                    isVisible = grade == MeditationIndicator.BAD
                                }.lparams(wrapContent, wrapContent) {
                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                    marginStart = dip(16.7f)
                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                }
                            }.lparams(matchConstraint, dip(37.3f)) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(0.2f)
                                topToBottom = R.id.screen_inner_widget_id_11
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(0.2f)
                            }

                            // 등급 5 라벨
                            nonSpacingTextView(MeditationIndicator.BAD.label) {
                                id = R.id.screen_inner_widget_id_14
                                typeface = if (grade == MeditationIndicator.BAD) Font.latoBold else Font.latoRegular
                                textSize = 21.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.BAD) R.color.x_ffffff else R.color.x_2e2e2e
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_16
                                marginStart = dip(54.7f)
                                topToTop = R.id.screen_inner_widget_id_16
                                bottomToBottom = R.id.screen_inner_widget_id_16
                            }

                            // 등급 5 점수 범위
                            nonSpacingTextView("${MeditationIndicator.BAD.scoreRange.start} ~ ${MeditationIndicator.BAD.scoreRange.endInclusive}") {
                                typeface = Font.latoRegular
                                textSize = 22.0f
                                textColorResource =
                                    if (grade == MeditationIndicator.BAD) R.color.x_ffffff else R.color.x_9196a5
                                lines = 1
                                includeFontPadding = false
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_14
                                marginStart = dip(160.0f)
                                topToTop = R.id.screen_inner_widget_id_14
                                bottomToBottom = R.id.screen_inner_widget_id_14
                            }

                            //----------------------------------------------------------

                        }.lparams(dip(338.7f), dip(245.3f)) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_03
                            topMargin = dip(46.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                    }.lparams(matchParent, matchParent)

                }.lparams(dip(408.7f), dip(552.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(64.7f)
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(46.0f)
                }

                // 훈련 중 뇌파변화
                cardView {
                    id = R.id.screen_widget_id_04
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_02

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.meditation_content_result_screen_040) {
                            id = R.id.screen_inner_widget_id_15
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_ffffff
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(38.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(38.0f)
                        }

                        // 깊은 뇌파 구간
                        view {
                            backgroundColorResource = R.color.x_4ab6ff
                        }.lparams(dip(22.7f), dip(4.7f)) {
                            topToTop = R.id.screen_inner_widget_id_16
                            endToStart = R.id.screen_inner_widget_id_16
                            marginEnd = dip(10.7f)
                            bottomToBottom = R.id.screen_inner_widget_id_16
                        }

                        nonSpacingTextView(R.string.meditation_content_result_screen_050) {
                            id = R.id.screen_inner_widget_id_16
                            typeface = Font.kopubDotumMedium
                            letterSpacing = -0.1f
                            textSize = 16.0f
                            textColorResource = R.color.x_ffffff
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_15
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(41.3f)
                            bottomToBottom = R.id.screen_inner_widget_id_15
                        }

                        lineChart {
                            backgroundResource = R.drawable.img_bg_graph_01

                            setViewPortOffsets(0.0f, 0.0f, 0.0f, 0.0f)
                            setDrawGridBackground(false)
                            description.isEnabled = false
                            legend.isEnabled = false
                            setPinchZoom(false)
                            setScaleEnabled(false)

                            setTouchEnabled(true)
                            isDragXEnabled = true
                            isDragYEnabled = false

                            axisRight.isEnabled = false

                            xAxis.setDrawAxisLine(false)
                            xAxis.setDrawLabels(false)
                            xAxis.setDrawGridLines(false)

                            axisLeft.setDrawAxisLine(false)
                            axisLeft.setDrawLabels(false)
                            axisLeft.setDrawGridLines(false)
                            axisLeft.labelCount = 11
                            axisLeft.axisMaximum = 10.0f
                            axisLeft.axisMinimum = 0.0f

                            result?.micGraphValues
                                ?.let { vs ->
                                    doubleArrayOf().fromJson(vs).apply {
                                        setScaleMinima(size.toFloat().div(45.0f), 0.0f)
                                    }
                                }?.mapIndexed { idx, v ->
                                    if (result?.micSignature == SIGNATURE_MIC_BRAIN_ACTIVITY) {
                                        Entry(
                                            idx.toFloat(), when (AnalysisAlgorithm.transformSef90HzTo11pValue(v)) {
                                                0.0 -> 0.0f
                                                1.0 -> 1.0f
                                                2.0 -> 2.0f
                                                3.0 -> 3.0f
                                                4.0 -> 8.0f
                                                5.0 -> 9.0f
                                                6.0 -> 10.0f
                                                7.0 -> 4.0f
                                                8.0 -> 5.0f
                                                9.0 -> 6.0f
                                                else -> 7.0f
                                            }
                                        )
                                    } else Entry(idx.toFloat(), v.toFloat())
                                }?.let {
                                    LineDataSet(it, null).apply {
                                        mode = LineDataSet.Mode.HORIZONTAL_BEZIER
                                        cubicIntensity = 0.2f
                                        setDrawCircles(false)
                                        setDrawHighlightIndicators(false)
                                        setDrawValues(false)
                                    }
                                }?.let {
                                    LineData(it)
                                }?.let {
                                    data = it
                                }
                        }.lparams(dip(635.3f), dip(141.3f)) {
                            startToStart = R.id.screen_inner_widget_id_15
                            topToBottom = R.id.screen_inner_widget_id_15
                            topMargin = dip(38.0f)
                            endToEnd = R.id.screen_inner_widget_id_16
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(709.3f), dip(276.0f)) {
                    startToEnd = R.id.screen_widget_id_03
                    marginStart = dip(34.0f)
                    topToTop = R.id.screen_widget_id_03
                }

                // 전체 명상시간
                cardView {
                    id = R.id.screen_widget_id_05
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.meditation_content_result_screen_060) {
                            id = R.id.screen_inner_widget_id_15
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(32.0f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(36.7f)
                        }
                        // 명상시간
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_16
                            text = result?.totalMeditationTime?.millisToFormatString(DTP_MM_SS)
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_15
                            topMargin = dip(35.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 프로그램 타이틀
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_17
                            text = resources.getString(R.string.meditation_content_result_screen_090)
//                            text = meditationProgramTitle
                            typeface = Font.kopubDotumMedium
                            textSize = 17.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(36.0f)
                            topToBottom = R.id.screen_inner_widget_id_16
                            topMargin = dip(39.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(36.0f)
                            horizontalBias = 0.0f
                        }
                        // 콘텐츠 타이틀
                        nonSpacingTextView {
                            text = meditationTitle.replaceNewLineAsSpace()
                            typeface = Font.kopubDotumMedium
                            textSize = 17.0f
                            textColorResource = R.color.x_2e2e2e_op60
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = R.id.screen_inner_widget_id_17
                            topToBottom = R.id.screen_inner_widget_id_17
                            topMargin = dip(9.3f)
                            endToEnd = R.id.screen_inner_widget_id_17
                            horizontalBias = 0.0f
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(300.0f), dip(244.7f)) {
                    startToStart = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_03
                }

                // 깊은 명상시간
                cardView {
                    id = R.id.screen_widget_id_06
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.meditation_content_result_screen_070) {
                            id = R.id.screen_inner_widget_id_17
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(32.0f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(36.7f)
                        }
                        // 명상시간
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_18
                            text = result?.deepMeditationTime?.millisToFormatString(DTP_MM_SS)
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            lines = 1
                            includeFontPadding = false
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_17
                            topMargin = dip(35.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 집중시간
                        deepTrainingDuration =
                            deepTrainingDurationGraph {
                                totalTime = result?.totalMeditationTime ?: 0
                                deepTrainingTime = result?.deepMeditationTime ?: 0
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_18
                                topMargin = dip(15.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(376.0f), dip(244.7f)) {
                    endToEnd = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_03
                }

                // 주의 사항
                imageView(R.drawable.ic_information) {
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_07
                    endToStart = R.id.screen_widget_id_07
                    marginEnd = dip(10.7f)
                    bottomToBottom = R.id.screen_widget_id_07
                }

                nonSpacingTextView(R.string.meditation_content_result_screen_080) {
                    id = R.id.screen_widget_id_07
                    typeface = Font.kopubDotumMedium
                    textSize = 19.0f
                    textColorResource = R.color.x_9196a5
                    letterSpacing = -0.1f
                    includeFontPadding = false
                    lines = 1
                }.lparams(wrapContent, wrapContent) {
                    topToBottom = R.id.screen_widget_id_06
                    topMargin = dip(38.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(60.0f)
                }
            }
        }.view

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}