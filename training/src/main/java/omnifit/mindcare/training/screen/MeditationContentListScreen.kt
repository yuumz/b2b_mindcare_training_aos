package omnifit.mindcare.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.commons.ui.anko.AnkoBindableComponentAdapter
import omnifit.commons.ui.anko.AnkoBindableComponentHolder
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.batteryLevelChangeObserver
import omnifit.mindcare.training.common.SCREEN_ARGUMENTS_MEDITATION_CONTENT_POSITION
import omnifit.mindcare.training.common.SCREEN_ARGUMENTS_MEDITATION_CONTENT_TITLE
import omnifit.mindcare.training.component.MeditationContentItemComponent
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.helper.MeditationSource
import omnifit.mindcare.training.screen.pop.HeadsetPopScreen
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.withArguments

class MeditationContentListScreen : UIScreen() {

    private val meditationPlaylist: MutableList<MeditationSource> by lazy {
        ContentHelper.meditationPlaylist().toMutableList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundColorResource = R.color.x_ffffff

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_back_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        performBackPressed()
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                imageView(R.drawable.ic_service_logo_02) {
                    id = R.id.screen_widget_id_02
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 밧데리잔량
                constraintLayout {
                    id = R.id.screen_widget_id_03
                    backgroundResource = R.drawable.shape_round_28_op04_bg_headset

                    // 헤드셋 이미지
                    imageView(R.drawable.ic_headset_bk) {
                        id = R.id.screen_inner_widget_id_01
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(25.3f)
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }
                    // 헤드셋 밧데리 잔량
                    textView {
                        id = R.id.screen_inner_widget_id_02
                        typeface = Font.latoRegular
                        textSize = 21.3f
                        textColorResource = R.color.x_303030
                        lines = 1
                        includeFontPadding = false
                        batteryLevelChangeObserver(owner)
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_01
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        horizontalBias = 0.3f
                        verticalBias = 0.5f
                    }
                }.lparams(dip(137.3f), dip(56.0f)) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(106.7f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 설정
                imageButton(R.drawable.selector_setting_bk) {
                    id = R.id.screen_widget_id_04
                    scaleType = ImageView.ScaleType.CENTER
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        HeadsetPopScreen().run {
                            show(this@MeditationContentListScreen.fragmentManager)
                        }
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_03
                    marginStart = dip(28.0f)
                    topToTop = R.id.screen_widget_id_03
                    bottomToBottom = R.id.screen_widget_id_03
                }

                // 명상 타이틀
                nonSpacingTextView(R.string.meditation_contents_list_screen_010) {
                    id = R.id.screen_widget_id_05
                    typeface = Font.kopubDotumBold
                    textSize = 45.0f
                    textColorResource = R.color.x_2e2e2e
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(76.0f)
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(52.7f)
                }

                imageView(R.drawable.quotes_front_bk) {
                    id = R.id.screen_widget_id_06
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_05
                    topToBottom = R.id.screen_widget_id_05
                    topMargin = dip(18.0f)
                }

                // 설명
                textView(R.string.meditation_contents_list_screen_020) {
                    id = R.id.screen_widget_id_07
                    typeface = Font.kopubDotumMedium
                    textSize = 20.0f
                    textColorResource = R.color.x_9fa4af
                    lines = 2
                    includeFontPadding = false
                    setLineSpacing(10.7f, 1.0f)
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_06
                    marginStart = dip(10.7f)
                    topToBottom = R.id.screen_widget_id_05
                    topMargin = dip(25.3f)
                }

                recyclerView {
                    id = R.id.screen_widget_id_08
                    clipToPadding = false
                    setPadding(dip(40.0f), 0, dip(72.7f), 0)
                    layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    setHasFixedSize(true)
                    setItemViewCacheSize(meditationPlaylist.size)
                    adapter = MeditationContentListAdapter(meditationPlaylist, this@MeditationContentListScreen)
                }.lparams(matchParent, dip(438.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_07
                    topMargin = dip(43.3f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }
            }
        }.view

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}

class MeditationContentListAdapter constructor(
    items: MutableList<MeditationSource>,
    owner: MeditationContentListScreen
) :
    AnkoBindableComponentAdapter<MutableList<MeditationSource>, MeditationContentListScreen, MeditationContentListAdapter.ComponentHolder>(
        items,
        owner
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(MeditationContentItemComponent(parent.context, owner)) { _, position ->
            when {
                !Headset.isConnected() -> HeadsetPopScreen().run { show(owner.fragmentManager) }
                !Headset.isSensorsAttached() -> owner.alert(
                    parent.context,
                    R.string.notifications_020,
                    R.color.x_000000_op60
                )
                else -> owner.screenTo(
                    MeditationContentScreen().withArguments(
                        Pair(
                            SCREEN_ARGUMENTS_MEDITATION_CONTENT_POSITION,
                            position
                        )
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<MeditationSource, MeditationContentListScreen>,
//        action: (AnkoBindableComponent<MeditationSource, MeditationContentListScreen>, MeditationSource) -> Unit
        action: (AnkoBindableComponent<MeditationSource, MeditationContentListScreen>, Int) -> Unit
    ) : AnkoBindableComponentHolder<MeditationSource, MeditationContentListScreen>(component) {
        init {
            (component as? MeditationContentItemComponent)?.run {
                content?.onClick { contentPlay?.performClick() }
//                contentPlay?.onClick { action(component, items[adapterPosition]) }
                contentPlay?.onClick { action(component, adapterPosition) }
            }
        }
    }
}