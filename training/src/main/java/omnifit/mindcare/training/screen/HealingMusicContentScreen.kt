package omnifit.mindcare.training.screen

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearSnapHelper
import at.favre.lib.dali.Dali
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.core.*
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.commons.ui.anko.AnkoBindableComponentAdapter
import omnifit.commons.ui.anko.AnkoBindableComponentHolder
import omnifit.mindcare.training.*
import omnifit.mindcare.training.R
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.component.AnalyzingViewComponent
import omnifit.mindcare.training.component.HealingMusicContentItemComponent
import omnifit.mindcare.training.configuration.UserConfiguration
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.headset.fx2.*
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.helper.ContentResultHelper
import omnifit.mindcare.training.screen.pop.HeadsetPopScreen
import omnifit.mindcare.training.screen.pop.SensorsAttachmentPopScreen
import omnifit.mindcare.training.screen.pop.StopPopScreen
import omnifit.mindcare.training.view.CircularProgressBar
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.configuration
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber
import travel.ithaka.android.horizontalpickerlib.PickerLayoutManager
import kotlin.properties.Delegates
import kotlin.random.Random

class HealingMusicContentScreen : UIScreen() {

    val playlist: MutableList<AudioPlayer.AudioSource> by lazy {
        ContentHelper.healingMusicPlaylist().toMutableList()
    }

    private val recentlyPlayedPosition: Int by lazy {
        playlist.indexOfFirst { audio ->
            audio.sequence == UserConfiguration.get(requireContext()).recentlyHealingMusicContentSequence
        }.let { position ->
            Timber.d("--> 랜덤 힐링뮤직 : $position")
            if (position >= 0) position else 0
        }
    }

    private var lazyPlayAudio: (() -> Unit)? = null

    var selectedPosition: Int by Delegates.observable(-1) { _, o, n ->
        if (o != n) {
            title?.setText(playlist[n].sourceTitle)
            if (isAttached) playMusic(playlist[n])
            else lazyPlayAudio = { playMusic(playlist[n]) }
        }
    }

    private var isAttached: Boolean = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        isAttached = true
        lazyPlayAudio?.invoke() ?: Timber.d("--> Lazy play audio [$lazyPlayAudio]")
    }

    override fun onDestroy() {
        stopMusic()
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
        isAttached = false
    }

    private var title: TextSwitcher? = null
    private var playingTime: NonSpacingTextView? = null
    private var duration: NonSpacingTextView? = null
    private var analyzingView: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundColorResource = R.color.x_ffffff

                layoutTransition = LayoutTransition().apply {
                    addTransitionListener(object : LayoutTransition.TransitionListener {
                        override fun startTransition(
                            transition: LayoutTransition,
                            container: ViewGroup,
                            view: View,
                            transitionType: Int
                        ) {
                        }

                        override fun endTransition(
                            transition: LayoutTransition,
                            container: ViewGroup,
                            view: View,
                            transitionType: Int
                        ) {
                            // 결과 분석 프로그래스 애니메이션 시작 시점
                            if (view.id == R.id.screen_inner_widget_id_root_container) {
                                find<CircularProgressBar>(R.id.screen_inner_widget_id_03).startAnimation()
                            }
                        }
                    })
                }

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_back_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        performBackPressed()
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 밧데리잔량
                constraintLayout {
                    id = R.id.screen_widget_id_02
                    backgroundResource = R.drawable.shape_round_28_op04_bg_headset

                    // 헤드셋 이미지
                    imageView(R.drawable.ic_headset_bk) {
                        id = R.id.screen_inner_widget_id_01
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(25.3f)
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    }
                    // 헤드셋 밧데리 잔량
                    textView {
                        id = R.id.screen_inner_widget_id_02
                        typeface = Font.latoRegular
                        textSize = 21.3f
                        textColorResource = R.color.x_303030
                        lines = 1
                        includeFontPadding = false
                        batteryLevelChangeObserver(owner)
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_01
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                        horizontalBias = 0.3f
                        verticalBias = 0.5f
                    }
                }.lparams(dip(137.3f), dip(56.0f)) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(106.7f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 설정
                imageButton(R.drawable.selector_setting_bk) {
                    id = R.id.screen_widget_id_03
                    scaleType = ImageView.ScaleType.CENTER
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        HeadsetPopScreen().run {
                            show(this@HealingMusicContentScreen.fragmentManager)
                        }
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_02
                    marginStart = dip(28.0f)
                    topToTop = R.id.screen_widget_id_02
                    bottomToBottom = R.id.screen_widget_id_02
                }

                @Suppress("UNCHECKED_CAST")
                recyclerView {
                    id = R.id.screen_widget_id_04
                    clipToPadding = false
                    setPadding(dip(384.0f), 0, dip(381.3f), 0)
                    layoutManager = PickerLayoutManager(context, PickerLayoutManager.HORIZONTAL, false).apply {
                        isChangeAlpha = true
                        scaleDownBy = 0.1f
                        scaleDownDistance = 0.8f
                        setOnScrollStopListener { v ->
                            (v.tag as? AudioPlayer.AudioSource)?.let { source ->
                                selectedPosition = playlist.indexOf(source)
                            }
                        }
                    }
                    LinearSnapHelper().attachToRecyclerView(this)
                    setHasFixedSize(true)      // 정적 리스트 데이터 성능용
                    setItemViewCacheSize(playlist.size) // 정적 리스트 캐시 사이즈
                    adapter = HealingMusicContentListAdapter(playlist, this@HealingMusicContentScreen)
                    smoothScrollBy(1, 0)
                    smoothScrollToPosition(recentlyPlayedPosition)
                }.lparams(matchConstraint, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(44.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                title = textSwitcher {
                    id = R.id.screen_widget_id_05
                    setFactory {
                        NonSpacingTextView(context).apply {
                            typeface = Font.kopubDotumBold
                            textSize = 33.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            gravity = Gravity.CENTER
                            layoutParams = FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            )
                        }
                    }
                    measureAllChildren = false
                    inAnimation = fadeIn
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_04
                    topMargin = dip(40.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                playingTime = nonSpacingTextView("00:00") {
                    id = R.id.screen_widget_id_06
                    typeface = Font.latoRegular
                    textSize = 24.0f
                    letterSpacing = -0.01f
                    lines = 1
                    includeFontPadding = false
                    paint.shader = LinearGradient(
                        0.0f,
                        0.0f,
                        paint.measureText(text.toString()),
                        textSize,
                        Color.parseColor("#ff6c80ff"),
                        Color.parseColor("#ff2ba4f5"),
                        Shader.TileMode.CLAMP
                    )
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_07
                    endToStart = R.id.screen_widget_id_07
                    marginEnd = dip(10.7f)
                    bottomToBottom = R.id.screen_widget_id_07
                }

                view {
                    id = R.id.screen_widget_id_07
                    backgroundResource = R.drawable.shape_time_divider_dot
                }.lparams(dip(5.3f), dip(5.3f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_05
                    topMargin = dip(26.0f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                duration = nonSpacingTextView("00:00") {
                    id = R.id.screen_widget_id_08
                    typeface = Font.latoRegular
                    textSize = 24.0f
                    textColorResource = R.color.x_a8a8a8
                    letterSpacing = -0.01f
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_07
                    marginStart = dip(10.7f)
                    topToTop = R.id.screen_widget_id_07
                    bottomToBottom = R.id.screen_widget_id_07
                }

                analyzingView = AnalyzingViewComponent<HealingMusicContentScreen>(ServiceCategory.HEALING_MUSIC)
                    .createView(AnkoContext.create(requireContext(), this@HealingMusicContentScreen))
                    .also { v ->
                        addView(v)
                        v.isVisible = false
                    }
            }
        }.view

    private var isNeuroFeedbackStarted: Boolean = false

    private var durationTime: Int = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AudioPlayer.observePlaybackStatus(this@HealingMusicContentScreen) { status ->
            when (status) {
                is AudioPlayer.PlaybackStatus.Prepare -> doseNothing()
                is AudioPlayer.PlaybackStatus.Start -> {
//                    updatePlayingTime(status.currentPosition)
                    updatePlayingTime()
                    durationTime = status.duration
                    updateDurationTime(durationTime)
                    UserConfiguration.get(requireContext()).apply {
                        recentlyHealingMusicContentSequence = status.audioSource.sequence
                    }.set(requireContext())

                    if (!isNeuroFeedbackStarted) {
                        startNeuroFeedback()
                        isNeuroFeedbackStarted = true
                    }
                }
                is AudioPlayer.PlaybackStatus.Playing -> {
                    updatePlayingTime(status.currentPosition)
                }
                is AudioPlayer.PlaybackStatus.Pause -> doseNothing()
                is AudioPlayer.PlaybackStatus.Resume -> doseNothing()
                is AudioPlayer.PlaybackStatus.Stop -> doseNothing()
                is AudioPlayer.PlaybackStatus.Complete -> {
                    // 랜덤 재생
//                    find<RecyclerView>(R.id.screen_widget_id_04).smoothScrollToPosition(playNextPosition(selectedPosition))
//                    completedTimeSeconds += neuroFeedbackProgressTimeSeconds
                    updatePlayingTime(durationTime)
                    stopNeuroFeedback()

                }
                is AudioPlayer.PlaybackStatus.Error -> doseNothing()
                else -> doseNothing()
            }
        }

        // 측정에서 전달되는 값으로 처리 하기엔 페이지 이동이 불안정함
        Headset.observeSensorsAttachmentStatusChange(this@HealingMusicContentScreen) { status ->
            Timber.d("---> 센서 부착 상태 : $status , $neuroFeedbackProgressTimeSeconds, ${isResultsCanBeViewed()}")
            when(status) {
                SensorsAttachmentStatus.DETACHED -> {
                    if(neuroFeedbackProgressTimeSeconds > 0 && !isResultsCanBeViewed()) {
                        SensorsAttachmentPopScreen().run {
                            positiveAction = {
                                Headset.stopMeasurement(CancelCause.Detached(SensorsAttachmentStatus.DETACHED))
                            }
                            show(this@HealingMusicContentScreen.fragmentManager)
                        }
                    }
                }
                else -> doseNothing()
            }
        }
    }

    override fun onBackPressed(): Boolean {
        if (analyzingView?.isVisible == true) {
            alert(requireContext(), R.string.healing_music_content_screen_050, gravity = Gravity.CENTER)
            return true
        }

        // 추후 무한 재생의 경우 back key 정의 필요
        if (Headset.isMeasuring()) {
            StopPopScreen()
                .withArguments(
                    Pair(FRAGMENT_ARGUMENTS_COMMENT, R.string.healing_music_content_screen_030),
                    Pair(FRAGMENT_ARGUMENTS_MESSAGE, R.string.healing_music_content_screen_040)
                )
                .run {
                    positiveAction = {
                        Headset.stopMeasurement(CancelCause.Forced)
                    }
                    show(this@HealingMusicContentScreen.fragmentManager)
                }
            return true
        }

        return screenTo(MainScreen())
    }

    private fun updatePlayingTime(millis: Int = 0) {
        neuroFeedbackProgressTimeSeconds = millis.millisToSeconds()
        playingTime?.text = millis.millisToFormatString(DTP_MM_SS)
    }

    private fun updateDurationTime(millis: Int) {
        duration?.text = millis.millisToFormatString(DTP_MM_SS)
    }

    private fun playMusic(source: AudioPlayer.AudioSource) {
        AudioPlayer.play(
            requireContext(),
            this@HealingMusicContentScreen,
            listOf(source)
        )
    }

    private fun stopMusic() {
        AudioPlayer.stop()
    }

    private tailrec fun playNextPosition(o: Int): Int {
        val n: Int = Random.nextInt(0, playlist.size)
        Timber.d("--> 랜덤 힐링뮤직 포지션 O[$o], N[$n]")
        return if (o != n) n else playNextPosition(o)
    }

    private var neuroFeedbackProgressTimeSeconds: Int = 0
    private var availableDataCount: Int = 0
    private var deepHealingCount: Int = 0

    private fun isResultsCanBeViewed(): Boolean {
        return neuroFeedbackProgressTimeSeconds >= 3.millisToSeconds() && availableDataCount >= 20
    }

    private fun startNeuroFeedback() {
        Headset.startMeasurement(
            this@HealingMusicContentScreen,
            MEASUREMENT_DURATION_INFINITE
        ) { status ->
            when (status) {
                is MeasurementStatus.Start -> doseNothing()
                is MeasurementStatus.Measuring -> {
                    if (status.values.isNotEmpty()) {
                        var micSignature = SIGNATURE_MIC_BRAIN_STRESS
                        checkDeepHealing(
                            micSignature,
                            when (micSignature) {
                                SIGNATURE_MIC_BRAIN_STRESS -> status.values[RESULT_ITEM_L_HBETA]
                                SIGNATURE_MIC_RELAXATION -> status.values[RESULT_ITEM_RELAXATION]
                                SIGNATURE_MIC_BRAIN_ACTIVITY -> {
                                    // 원시데이터에서 지표 5단계로 변환
                                    status.values
                                        .splitPowerSpectrum(15, 143)
                                        .computeSef90Hz()
                                        .let { hz ->
                                            AnalysisAlgorithm.transformSef90HzTo5pValue(hz)
                                        }
                                }
                                else -> -1.0
                            }
                        )
                    }
                }
                is MeasurementStatus.Stop -> {
                    Timber.d("---> 힐링뮤직 측정 종료 : [$availableDataCount], [$neuroFeedbackProgressTimeSeconds]")
                    stopMusic()
                    if (isResultsCanBeViewed()) {
                        ContentResultHelper.createHealingMusicResult(
                            requireContext(),
                            neuroFeedbackProgressTimeSeconds,
                            playlist[selectedPosition].sequence,
                            SIGNATURE_MIC_BRAIN_STRESS,
                            deepHealingCount,
                            playlist[selectedPosition].albumTitle,
                            status.result
                        ) { r ->
                        }
                        // Blur 백그라운드 생성
                        this@HealingMusicContentScreen.view?.run {
                            Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888).also { bmp ->
                                draw(Canvas(bmp))
                            }
                        }?.let { bmp ->
                            Dali.create(context)
                                .load(bmp)
                                .blurRadius(25)
                                .concurrent()
                                .skipCache()
                                .get()
                        }.let { screenshot ->
                            analyzingView?.backgroundDrawable = screenshot
                            analyzingView?.isVisible = true
                        }
                    }
                }

                is MeasurementStatus.Cancel -> {
                    Timber.d("--> 힐링뮤직 측정 취소 : [${status.cause}]")
                    performBackPressed()
                }
            }
        }
    }

    private fun stopNeuroFeedback() {
        UserConfiguration.get(requireContext()).apply {
            recentlyHealingMusicContentSequence = playlist[playNextPosition(selectedPosition)].sequence
        }.set(requireContext())
        Headset.stopMeasurement()
    }

    private fun checkDeepHealing(
        micSignature: String,
        indicatorValue: Double
    ) {

        if (indicatorValue.isAvailable()) {
            availableDataCount++
        }

        ServiceAlgorithm.checkDeepHealingMusicDuringHealing(micSignature, indicatorValue)
            .let { able ->
                if (able) {
                    deepHealingCount++
                    Timber.i("--> deep : $deepHealingCount")
                }
            }
    }
}

class HealingMusicContentListAdapter constructor(
    items: MutableList<AudioPlayer.AudioSource>,
    owner: HealingMusicContentScreen
) :
    AnkoBindableComponentAdapter<MutableList<AudioPlayer.AudioSource>, HealingMusicContentScreen, HealingMusicContentListAdapter.ComponentHolder>(
        items,
        owner
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(HealingMusicContentItemComponent(parent.context, owner)) { _, content ->

        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<AudioPlayer.AudioSource, HealingMusicContentScreen>,
        action: (AnkoBindableComponent<AudioPlayer.AudioSource, HealingMusicContentScreen>, AudioPlayer.AudioSource) -> Unit
    ) : AnkoBindableComponentHolder<AudioPlayer.AudioSource, HealingMusicContentScreen>(component) {
        init {
        }
    }
}