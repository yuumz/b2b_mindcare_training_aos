package omnifit.mindcare.training.screen

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.component.ResultGraphComponent
import omnifit.mindcare.training.configuration.MeasurementConfiguration
import omnifit.mindcare.training.data.Code
import omnifit.mindcare.training.helper.ContentDataHelper
import omnifit.mindcare.training.screen.pop.MeasurementResultPopScreen
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.withArguments
import kotlin.math.absoluteValue

class MeasurementResultScreen : UIScreen() {

    val result: MeasurementConfiguration? by lazy {
        MeasurementConfiguration.find(requireContext())
    }

    private val measurementIndicator: IntArray by lazy {
        intArrayOf(
            result?.brainStressIndicator?.ordinal ?: 0,
            result?.concentrationIndicator?.ordinal ?: 0,
            result?.brainActivityIndicator?.ordinal ?: 0,
            result?.balanceIndicator?.ordinal ?: 0
        )
    }

    private val measurementSimpleLevel: Array<MeasurementSimpleLevel> by lazy {
        arrayOf(
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BRAIN_STRESS,
                    measurementIndicator[MEASUREMENT_ITEM_BRAIN_STRESS]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_CONCENTRATION,
                    measurementIndicator[MEASUREMENT_ITEM_CONCENTRATION]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BRAIN_ACTIVITY,
                    measurementIndicator[MEASUREMENT_ITEM_BRAIN_ACTIVITY]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BALANCE,
                    measurementIndicator[MEASUREMENT_ITEM_BALANCE]
                )
            )
        )
    }

    var brainConditionLevel: NonSpacingTextView? = null
    var brainConditionScore: NonSpacingTextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.shape_bg_main

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_home_bk) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        screenTo(MainScreen())
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                nonSpacingTextView(R.string.measurement_result_screen_010) {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumMedium
                    textSize = 32.0f
                    textColorResource = R.color.x_2e2e2e
                    letterSpacing = -0.03f
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 두뇌컨디션
                cardView {
                    id = R.id.screen_widget_id_03
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01
                    foreground = context.getDrawable(R.drawable.selector_card_fg)
                    // 타이틀
                    constraintLayout {
                        nonSpacingTextView(R.string.measurement_result_screen_020) {
                            id = R.id.screen_inner_widget_id_01
                            typeface = Font.kopubDotumBold
                            textSize = 27.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(38.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 로고
                        imageView(R.drawable.ic_brain_01) {
                            id = R.id.screen_inner_widget_id_02
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_01
                            topMargin = dip(18.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 레벨
                        brainConditionLevel = nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_03
                            text = updateBrainConditionLevel()
                            typeface = Font.kopubDotumBold
                            textSize = 53.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_02
                            topMargin = dip(23.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                        // 점수
                        brainConditionScore = nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_04
                            text = "${result?.brainHealthScore}"
                            typeface = Font.latoLight
                            textSize = 53.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_03
                            topMargin = dip(18.0f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        nonSpacingTextView(R.string.main_screen_080) {
                            typeface = Font.kopubDotumMedium
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToEnd = R.id.screen_inner_widget_id_04
                            marginStart = dip(7.3f)
                            bottomToBottom = R.id.screen_inner_widget_id_04
                        }

                        // 설명
                        constraintLayout {
                            backgroundColorResource = R.color.x_f8fafb

                            // 설명로고
                            imageView(R.drawable.ic_brain_02) {
                                id = R.id.screen_inner_widget_id_05
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                marginStart = dip(22.7f)
                                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                topMargin = dip(20.7f)
                            }
                            // 설명타이틀
                            nonSpacingTextView(R.string.measurement_result_screen_030) {
                                typeface = Font.kopubDotumMedium
                                textSize = 24.0f
                                textColorResource = R.color.x_2e2e2e
                                letterSpacing = -0.1f
                                includeFontPadding = false
                                lines = 1
                            }.lparams(dip(0.0f), wrapContent) {
                                startToEnd = R.id.screen_inner_widget_id_05
                                marginEnd = dip(8.7f)
                                bottomToBottom = R.id.screen_inner_widget_id_05
                            }
                            // 설명내용
                            textView(R.string.measurement_result_screen_040) {
                                typeface = Font.kopubDotumMedium
                                textSize = 17.0f
                                textColorResource = R.color.x_9196a5
                                includeFontPadding = false
                                letterSpacing = -0.01f
                                setLineSpacing(8.3f, 1.0f)
                            }.lparams(dip(0.0f), wrapContent) {
                                startToStart = R.id.screen_inner_widget_id_05
                                topToBottom = R.id.screen_inner_widget_id_05
                                topMargin = dip(18.7f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                marginEnd = dip(44.0f)
                                verticalBias = 0.0f
                            }
                        }.lparams(dip(262.0f), dip(149.3f)) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_04
                            topMargin = dip(35.3f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(334.0f), dip(552.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(64.7f)
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(46.0f)
                }

                // 두뇌스트레스
                cardView {
                    id = R.id.screen_widget_id_04
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01
                    foreground = context.getDrawable(R.drawable.selector_card_fg)
                    isSoundEffectsEnabled = true
                    isClickable = true
                    onClick {
                        screenToDetailedResultScreen(MEASUREMENT_ITEM_BRAIN_STRESS)
                    }

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.measurement_result_screen_050) {
                            id = R.id.screen_inner_widget_id_06
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(40.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(37.3f)
                        }
                        // 자세히보기
                        imageButton(R.drawable.selector_result_detail) {
                            backgroundColor = Color.TRANSPARENT
                            onClick {
                                screenToDetailedResultScreen(MEASUREMENT_ITEM_BRAIN_STRESS)
                            }
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_06
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(20.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_06
                        }

                        // 스트레스 레벨
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_07
                            text = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_STRESS].labelOfEn
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_06
                            topMargin = dip(34.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 스트레스 그래프
                        ResultGraphComponent<MeasurementResultScreen>(
                            context = requireContext(),
                            _item = MEASUREMENT_ITEM_BRAIN_STRESS,
                            _result = measurementIndicator[MEASUREMENT_ITEM_BRAIN_STRESS],
                            _simpleLevel = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_STRESS]
                        )
                            .createView(AnkoContext.create(requireContext(), this@MeasurementResultScreen))
                            .lparams(matchParent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_07
                                topMargin = dip(20.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomMargin = dip(18.0f)
                            }
                            .also { v ->
                                addView(v)
                            }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(376.0f), dip(258.7f)) {
                    startToEnd = R.id.screen_widget_id_03
                    marginStart = dip(32.7f)
                    topToTop = R.id.screen_widget_id_03
                }

                // 집중도
                cardView {
                    id = R.id.screen_widget_id_05
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01
                    foreground = context.getDrawable(R.drawable.selector_card_fg)
                    isSoundEffectsEnabled = true
                    isClickable = true
                    onClick {
                        screenToDetailedResultScreen(MEASUREMENT_ITEM_CONCENTRATION)
                    }

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.measurement_result_screen_060) {
                            id = R.id.screen_inner_widget_id_08
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(40.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(37.3f)
                        }
                        // 자세히보기
                        imageButton(R.drawable.selector_result_detail) {
                            backgroundColor = Color.TRANSPARENT
                            onClick {
                                screenToDetailedResultScreen(MEASUREMENT_ITEM_CONCENTRATION)
                            }
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_08
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(20.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_08
                        }

                        constraintLayout {
                            id = R.id.screen_inner_widget_id_09
                            // 집중도 레벨
                            nonSpacingTextView {
                                id = R.id.screen_inner_widget_id_10
                                text = measurementSimpleLevel[MEASUREMENT_ITEM_CONCENTRATION].labelOfEn
                                typeface = Font.latoBold
                                textSize = 48.0f
                                textColorResource = R.color.x_2e2e2e
                                includeFontPadding = false
                                lines = 1
                            }.lparams(wrapContent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            }

                            // 집중도 타입
                            nonSpacingTextView {
                                text = "(${updateConcentrationType().label})"
                                typeface = Font.kopubDotumMedium
                                textSize = 23.0f
                                textColorResource = R.color.x_2e2e2e
                                includeFontPadding = false
                                lines = 1
                            }.lparams(wrapContent, wrapContent) {
                                startToEnd = R.id.screen_inner_widget_id_10
                                marginStart = dip(12.0f)
                                bottomToBottom = R.id.screen_inner_widget_id_10
                            }
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_08
                            topMargin = dip(34.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 집중도 그래프
                        ResultGraphComponent<MeasurementResultScreen>(
                            context = requireContext(),
                            _item = MEASUREMENT_ITEM_CONCENTRATION,
                            _result = measurementIndicator[MEASUREMENT_ITEM_CONCENTRATION],
                            _simpleLevel = measurementSimpleLevel[MEASUREMENT_ITEM_CONCENTRATION]
                        )
                            .createView(AnkoContext.create(requireContext(), this@MeasurementResultScreen))
                            .lparams(matchParent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_09
                                topMargin = dip(20.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomMargin = dip(18.0f)
                            }
                            .also { v ->
                                addView(v)
                            }
                    }.lparams(matchParent, matchParent)

                }.lparams(dip(376.0f), dip(258.7f)) {
                    startToEnd = R.id.screen_widget_id_04
                    marginStart = dip(34.0f)
                    topToTop = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_04
                }

                // 두뇌활동정도
                cardView {
                    id = R.id.screen_widget_id_06
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01
                    foreground = context.getDrawable(R.drawable.selector_card_fg)
                    isSoundEffectsEnabled = true
                    isClickable = true
                    onClick {
                        screenToDetailedResultScreen(MEASUREMENT_ITEM_BRAIN_ACTIVITY)
                    }

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.measurement_result_screen_070) {
                            id = R.id.screen_inner_widget_id_11
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(40.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(37.3f)
                        }
                        // 자세히보기
                        imageButton(R.drawable.selector_result_detail) {
                            backgroundColor = Color.TRANSPARENT
                            onClick {
                                screenToDetailedResultScreen(MEASUREMENT_ITEM_BRAIN_ACTIVITY)
                            }
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_11
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(20.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_11
                        }

                        // 두뇌활동정도 레벨
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_12
                            text = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_ACTIVITY].labelOfEn
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_11
                            topMargin = dip(34.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 두뇌활동정도 그래프
                        ResultGraphComponent<MeasurementResultScreen>(
                            context = requireContext(),
                            _item = MEASUREMENT_ITEM_BRAIN_ACTIVITY,
                            _result = measurementIndicator[MEASUREMENT_ITEM_BRAIN_ACTIVITY],
                            _simpleLevel = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_ACTIVITY]
                        )
                            .createView(AnkoContext.create(requireContext(), this@MeasurementResultScreen))
                            .lparams(matchParent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_12
                                topMargin = dip(20.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomMargin = dip(18.0f)
                            }
                            .also { v ->
                                addView(v)
                            }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(376.0f), dip(258.7f)) {
                    startToStart = R.id.screen_widget_id_04
                    endToEnd = R.id.screen_widget_id_04
                    bottomToBottom = R.id.screen_widget_id_03
                }

                // 좌우뇌불균형
                cardView {
                    id = R.id.screen_widget_id_07
                    cardElevation = dip(5.0f).toFloat()
                    backgroundResource = R.drawable.shape_round_26_7_bg_result_01
                    foreground = context.getDrawable(R.drawable.selector_card_fg)
                    isSoundEffectsEnabled = true
                    isClickable = true
                    onClick {
                        screenToDetailedResultScreen(MEASUREMENT_ITEM_BALANCE)
                    }

                    constraintLayout {
                        // 타이틀
                        nonSpacingTextView(R.string.measurement_result_screen_080) {
                            id = R.id.screen_inner_widget_id_16
                            typeface = Font.kopubDotumBold
                            letterSpacing = -0.1f
                            textSize = 23.0f
                            textColorResource = R.color.x_afb3c0
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            marginStart = dip(40.7f)
                            topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                            topMargin = dip(37.3f)
                        }
                        // 자세히보기
                        imageButton(R.drawable.selector_result_detail) {
                            backgroundColor = Color.TRANSPARENT
                            onClick {
                                screenToDetailedResultScreen(MEASUREMENT_ITEM_BALANCE)
                            }
                        }.lparams(wrapContent, wrapContent) {
                            topToTop = R.id.screen_inner_widget_id_16
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                            marginEnd = dip(20.0f)
                            bottomToBottom = R.id.screen_inner_widget_id_16
                        }

                        // 좌우뇌불균형 레벨
                        nonSpacingTextView {
                            id = R.id.screen_inner_widget_id_14
                            text = measurementSimpleLevel[MEASUREMENT_ITEM_BALANCE].labelOfEn
                            typeface = Font.latoBold
                            textSize = 48.0f
                            textColorResource = R.color.x_2e2e2e
                            includeFontPadding = false
                            lines = 1
                        }.lparams(wrapContent, wrapContent) {
                            startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                            topToBottom = R.id.screen_inner_widget_id_16
                            topMargin = dip(34.7f)
                            endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        }

                        // 좌우뇌불균형 그래프
                        ResultGraphComponent<MeasurementResultScreen>(
                            context = requireContext(),
                            _item = MEASUREMENT_ITEM_BALANCE,
                            _result = (measurementIndicator[MEASUREMENT_ITEM_BALANCE] - 4).absoluteValue,
                            _simpleLevel = measurementSimpleLevel[MEASUREMENT_ITEM_BALANCE]
                        )
                            .createView(AnkoContext.create(requireContext(), this@MeasurementResultScreen))
                            .lparams(matchParent, wrapContent) {
                                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                topToBottom = R.id.screen_inner_widget_id_14
                                topMargin = dip(20.0f)
                                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                                bottomMargin = dip(18.0f)
                            }
                            .also { v ->
                                addView(v)
                            }
                    }.lparams(matchParent, matchParent)
                }.lparams(dip(376.0f), dip(258.7f)) {
                    startToStart = R.id.screen_widget_id_05
                    endToEnd = R.id.screen_widget_id_05
                    bottomToBottom = R.id.screen_widget_id_06
                }

                // 주의 사항
                imageView(R.drawable.ic_information) {
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_08
                    endToStart = R.id.screen_widget_id_08
                    marginEnd = dip(10.7f)
                    bottomToBottom = R.id.screen_widget_id_08
                }

                nonSpacingTextView(R.string.measurement_result_screen_090) {
                    id = R.id.screen_widget_id_08
                    typeface = Font.kopubDotumMedium
                    textSize = 19.0f
                    textColorResource = R.color.x_9196a5
                    letterSpacing = -0.1f
                    includeFontPadding = false
                    lines = 1
                }.lparams(wrapContent, wrapContent) {
                    topToBottom = R.id.screen_widget_id_07
                    topMargin = dip(38.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(60.0f)
                }
            }
        }.view

    private fun updateBrainConditionLevel(): String {
        var stress = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_STRESS].verdictValue
        var concentration = measurementSimpleLevel[MEASUREMENT_ITEM_CONCENTRATION].verdictValue
        var activity = measurementSimpleLevel[MEASUREMENT_ITEM_BRAIN_ACTIVITY].verdictValue
        var balance = measurementSimpleLevel[MEASUREMENT_ITEM_BALANCE].verdictValue

        return MeasurementSimpleLevel.of((stress + concentration + activity + balance) / 4.toLong()).labelOfKr
    }

    private fun updateConcentrationType(): Code {
        return ContentDataHelper.findConcentrationType(result?.concentrationTypeIndicator?.codeSignature ?: "CET007")
    }

    private fun screenToDetailedResultScreen(item: Int) {
        MeasurementResultPopScreen()
            .withArguments(Pair(SCREEN_ARGUMENTS_SHORTCUT_RESULT_ITEM, item))
            .run {
                show(this@MeasurementResultScreen.fragmentManager)
            }
    }

    override fun onBackPressed(): Boolean {
        return screenTo(MainScreen())
    }
}

