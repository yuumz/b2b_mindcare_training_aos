package omnifit.mindcare.training.screen.pop

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.UIPopScreen
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.common.collect.Iterables
import omnifit.commons.ui.anko.AnkoBindableComponent
import omnifit.commons.ui.anko.AnkoBindableComponentAdapter
import omnifit.commons.ui.anko.AnkoBindableComponentHolder
import omnifit.mindcare.training.deviceConnectChangeObserver
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.configuration.DeviceConfiguration
import omnifit.mindcare.training.headset.ConnectionStatus
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.configuration.HeadsetConfiguration
import omnifit.mindcare.training.headset.ScanStatus
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import omnifit.mindcare.training.whenTrue
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import java.util.concurrent.CopyOnWriteArrayList

class HeadsetPopScreen : UIPopScreen() {

    lateinit var screenUiComponent: HeadsetSettingComponent

    override fun <T : UIPopScreen> T.createScreenUiComponent(): AnkoComponent<T> =
        HeadsetSettingComponent().apply { screenUiComponent = this } as AnkoComponent<T>

    override fun onStart() {
        super.onStart()
        dialog!!.window.setWindowAnimations(R.style.PopScreenAnimation)
    }

    private var isDiscovery: Boolean = false

    fun toggleDiscovery(context: Context) {
        if (isDiscovery) {
            stopDiscovery()
        } else {
            startDiscovery(context)
        }
    }

    private fun startDiscovery(context: Context) {
        stopDiscovery()
        Headset.startDiscovery(context) { status ->
            when (status) {
                ScanStatus.Start -> {
                    displayProgress(true)
                    (screenUiComponent.headsetList?.adapter as DiscoveredItemListAdapter).clearDiscoveredItem()
                }
                is ScanStatus.Found -> {
                    with(status.result) {
                        (screenUiComponent.headsetList?.adapter as DiscoveredItemListAdapter).addDiscoveredItem(
                            DiscoveredItem(this)
                        )
                    }
                }
                ScanStatus.Finished -> displayProgress(false)
            }
        }
    }

    fun stopDiscovery() {
        Headset.stopDiscovery()
    }

    fun toggleConnectToDevice(context: Context) {
        if (!Headset.isConnected()) {
            if (HeadsetConfiguration.isRegistered(context)) {
                HeadsetConfiguration.get(context).also { configuration ->
                    Headset.connectTo(configuration.headsetDevice.address)
                }
            }
        } else {
            Headset.disconnectTo()
        }
    }

    fun connectToDevice(device: BluetoothDevice) {
        Headset.connectTo(device)
    }

    private fun displayProgress(isVisible: Boolean) {
        isDiscovery = isVisible
        screenUiComponent.searchButton?.text =
            if (isDiscovery) context?.getString(R.string.headset_setting_screen_050) else context?.getString(R.string.headset_setting_screen_040)
        screenUiComponent.searchProgress?.isVisible = isVisible
    }

    fun updateHeadsetDevice(context: Context) {
        Headset.observeConnectStatusChange(this) { status ->
            updateHeadsetDeviceProgress(status)
            updateHeadsetDeviceNameColor(status)
            if (HeadsetConfiguration.isRegistered(context)) {
                screenUiComponent.nonHeadset?.isInvisible = true
                screenUiComponent.headset?.isInvisible = false
                HeadsetConfiguration.get(context).also { configuration ->
                    updateHeadsetDeviceName(configuration)
                }
            } else {
                screenUiComponent.nonHeadset?.isInvisible = false
                screenUiComponent.headset?.isInvisible = true
            }
        }
    }

    private fun updateHeadsetDeviceProgress(status: ConnectionStatus) {
        screenUiComponent.headsetBTIcon?.backgroundResource = if (status == ConnectionStatus.CONNECTED) R.drawable.ic_bt_on else R.drawable.ic_bt_off
    }

    private fun updateHeadsetDeviceNameColor(status: ConnectionStatus) {
        screenUiComponent.headsetName.let { v ->
            v?.textColorResource = if (status == ConnectionStatus.CONNECTED) R.color.x_32a0f6 else R.color.x_a7a7a7
        }
        screenUiComponent.headsetAddress.let { v ->
            v?.textColorResource = if (status == ConnectionStatus.CONNECTED) R.color.x_1c1c1c else R.color.x_a7a7a7
        }
    }

    fun updateHeadsetDeviceName(configuration: HeadsetConfiguration?) {
        var headset = configuration?.productName!!.split("|")
        screenUiComponent.headsetName?.text = headset[0]
        screenUiComponent.headsetAddress?.text = headset[1]
    }
}

class HeadsetSettingComponent : AnkoLifecycleComponent<HeadsetPopScreen> {

    var headset: ConstraintLayout? = null
    var headsetName: TextView? = null
    var headsetAddress: TextView? = null
    var headsetBTIcon: ImageView? = null
    var headsetConnectProcess: ProgressBar? = null
    var nonHeadset: NonSpacingTextView? = null
    var searchProgress: ProgressBar? = null
    var headsetList: RecyclerView? = null
    var searchButton: Button? = null

    override fun createView(ui: AnkoContext<HeadsetPopScreen>): View = with(ui) {
        relativeLayout {
            backgroundColorResource = R.color.x_000000_op15

            constraintLayout {
                backgroundResource = R.drawable.shape_round_7_bg_pop

                // 타이틀
                nonSpacingTextView(R.string.headset_setting_screen_010) {
                    id = R.id.screen_widget_id_01
                    typeface = Font.kopubDotumBold
                    textSize = 30.0f
                    textColorResource = R.color.x_1c1c1c
                    lines = 1
                    letterSpacing = -0.01f
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(50.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(35.3f)
                }

                // 닫기
                imageButton(R.drawable.selector_close) {
                    id = R.id.screen_widget_id_02
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        owner.dismiss()
                    }
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(50.0f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                headset = constraintLayout {
                    id = R.id.screen_widget_id_03
                    // 헤드셋이름
                    headsetName = textView {
                        id = R.id.screen_inner_widget_id_01
                        typeface = Font.kopubDotumMedium
                        textSize = 25.7f
                        textColorResource = R.color.x_2da3f5
                        lines = 1
                        includeFontPadding = false
                        onClick {
                            owner.toggleConnectToDevice(owner.requireContext())
                        }
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    }

                    // 헤드셋 주소
                    headsetAddress = textView {
                        id = R.id.screen_inner_widget_id_02
                        typeface = Font.kopubDotumMedium
                        textSize = 25.7f
                        textColorResource = R.color.x_1c1c1c
                        lines = 1
                        includeFontPadding = false
                        onClick {
                            owner.toggleConnectToDevice(owner.requireContext())
                        }
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = R.id.screen_inner_widget_id_01
                        topToBottom = R.id.screen_inner_widget_id_01
                        topMargin = dip(5.0f)
                    }

                    headsetBTIcon = imageView {
                        id = R.id.screen_inner_widget_id_03
                    }.lparams(wrapContent, wrapContent) {
                        topToTop = R.id.screen_inner_widget_id_01
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        marginEnd = dip(3.0f)
                        bottomToBottom = R.id.screen_inner_widget_id_02
                    }

                    headsetConnectProcess = progressBar {
                        isVisible = false
                        indeterminateTintList = AppCompatResources.getColorStateList(context, R.color.x_2da3f5)
                        indeterminateTintMode = PorterDuff.Mode.SRC_ATOP
                        if (HeadsetConfiguration.isRegistered(context)) {
                            deviceConnectChangeObserver(owner)
                        }
                    }.lparams(dip(60.5f), dip(58.7f)) {
                        topToTop = R.id.screen_inner_widget_id_01
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                        bottomToBottom = R.id.screen_inner_widget_id_02
                    }

                }.lparams(dip(293.9f), wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(146.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }.applyRecursively {
                    owner.updateHeadsetDevice(owner.requireContext())
                }

                nonHeadset = nonSpacingTextView(R.string.headset_setting_screen_020) {
                    id = R.id.screen_widget_id_04
                    typeface = Font.kopubDotumMedium
                    textSize = 23.0f
                    textColorResource = R.color.x_b0b0b0
                    lines = 1
                    letterSpacing = -0.01f
                    includeFontPadding = false
                    onClick {
                        owner.toggleConnectToDevice(owner.requireContext())
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(146.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                // 디바이스 리스트
                headsetList = recyclerView {
                    id = R.id.screen_widget_id_05
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    adapter = DiscoveredItemListAdapter(CopyOnWriteArrayList(), owner)
                }.lparams(dip(0.0f), dip(0.0f)) {
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(78.0f)
                    startToStart = R.id.screen_widget_id_07
                    endToEnd = R.id.screen_widget_id_07
                    bottomToTop = R.id.screen_widget_id_06
                    bottomMargin = dip(10.0f)
                }

                // 검색 프로그레스
                searchProgress = horizontalProgressBar {
                    id = R.id.screen_widget_id_06
                    isIndeterminate = true
                    indeterminateTintList = AppCompatResources.getColorStateList(context, R.color.x_2da3f5)
                    indeterminateTintMode = PorterDuff.Mode.SRC_ATOP
                    isVisible = false
                }.lparams(dip(0.0f), wrapContent) {
                    startToStart = R.id.screen_widget_id_07
                    endToEnd = R.id.screen_widget_id_07
                    bottomToTop = R.id.screen_widget_id_07
                    bottomMargin = dip(5.0f)
                }

                // 검색하기
                searchButton = button(R.string.headset_setting_screen_040) {
                    id = R.id.screen_widget_id_07
                    backgroundResource = R.drawable.selector_round_10_bottom_button_bg
                    textSize = 24.0f
                    typeface = Font.kopubDotumBold
                    animation = null
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_bottom_button_text))
                    onClick {
                        owner.toggleDiscovery(context)
                    }
                }.lparams(dip(553.3f), dip(74.7f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomMargin = dip(30.0f)
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }

            }.lparams(dip(633.3f), dip(630.7f)) {
                gravity = Gravity.CENTER
            }
        }
    }

    override fun onViewCreated(owner: HeadsetPopScreen) {
    }
}

class DiscoveredItemListAdapter constructor(device: CopyOnWriteArrayList<DiscoveredItem>, owner: HeadsetPopScreen) :
    AnkoBindableComponentAdapter<CopyOnWriteArrayList<DiscoveredItem>, HeadsetPopScreen, DiscoveredItemListAdapter.ComponentHolder>(
        device,
        owner
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComponentHolder {
        return ComponentHolder(
            HeadsetSettingPopItemComponent(
                parent.context,
                owner
            )
        ) { _, content ->
            HeadsetConfiguration(
                headsetDevice = DeviceConfiguration(
                    content.name(),
                    content.address()
                )
            )
                .let { headsetConfiguration ->
                    headsetConfiguration.set(owner.requireContext())
                    owner.updateHeadsetDeviceName(headsetConfiguration)
                }
            owner.connectToDevice(content.device())
            owner.stopDiscovery()
        }
    }

    override fun onBindViewHolder(holder: ComponentHolder, position: Int) {
        holder.component.bind(items[position], position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addDiscoveredItem(result: DiscoveredItem) {
        if (isAvailableItem(result))
            items.add(result).whenTrue { notifyDataSetChanged() }
    }

    fun clearDiscoveredItem() {
        items.clear()
        notifyDataSetChanged()
    }

    private fun isAvailableItem(discoveredItem: DiscoveredItem): Boolean {
        return try {
            Iterables.find(items) { input ->
                input?.address().equals(discoveredItem.address())
            } == null
        } catch (e: java.util.NoSuchElementException) {
            true
        }

    }

    inner class ComponentHolder(
        component: AnkoBindableComponent<DiscoveredItem, HeadsetPopScreen>,
        action: (AnkoBindableComponent<DiscoveredItem, HeadsetPopScreen>, DiscoveredItem) -> Unit
    ) : AnkoBindableComponentHolder<DiscoveredItem, HeadsetPopScreen>(component) {
        init {
            (component as? HeadsetSettingPopItemComponent)?.run {
                content?.onClick { action(component, items[adapterPosition]) }
            }
        }
    }
}

class DiscoveredItem constructor(bluetoothDevice: BluetoothDevice) {
    var device: BluetoothDevice = bluetoothDevice

    fun device(): BluetoothDevice {
        return device
    }

    fun name(): String {
        return device.name
    }

    fun address(): String {
        return device.address
    }
}

class HeadsetSettingPopItemComponent<P : HeadsetPopScreen>(
    context: Context,
    owner: P
) : AnkoBindableComponent<DiscoveredItem, P>(context, owner) {

    var content: ConstraintLayout? = null

    private var deviceName: TextView? = null
    private var deviceAddress: TextView? = null

    override fun createView(ui: AnkoContext<P>): View = with(ui) {
        verticalLayout {

            content = constraintLayout {
                deviceName = textView {
                    typeface = Font.latoRegular
                    textSize = 23.0f
                    textColorResource = R.color.x_b0b0b0
                    lines = 1
                    includeFontPadding = false
                    gravity = Gravity.CENTER_VERTICAL
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(15.0f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }

                deviceAddress = textView {
                    typeface = Font.latoRegular
                    textSize = 23.0f
                    textColorResource = R.color.x_1c1c1c
                    lines = 1
                    includeFontPadding = false
                    gravity = Gravity.CENTER_VERTICAL
                }.lparams(wrapContent, wrapContent) {
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(25.0f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }

                view {
                    backgroundColorResource = R.color.x_ececec
                }.lparams(matchParent, dip(1.3f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }
            }.lparams(matchParent, dip(62.7f))
        }
    }

    override fun bind(data: DiscoveredItem, position: Int) {
        super.bind(data, position)
        deviceName?.text = data.name()
        deviceAddress?.text = data.address()
    }
}