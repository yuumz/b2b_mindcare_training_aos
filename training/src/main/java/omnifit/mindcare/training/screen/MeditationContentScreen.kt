package omnifit.mindcare.training.screen

import android.animation.LayoutTransition
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.media.AudioAttributes
import android.media.SoundPool
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import at.favre.lib.dali.Dali
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import omnifit.commons.audioplayer.AudioPlayer
import omnifit.commons.core.*
import omnifit.mindcare.training.*
import omnifit.mindcare.training.R
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.component.AnalyzingViewComponent
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.headset.fx2.*
import omnifit.mindcare.training.helper.ContentHelper
import omnifit.mindcare.training.helper.ContentResultHelper
import omnifit.mindcare.training.helper.MeditationSource
import omnifit.mindcare.training.screen.pop.SensorsAttachmentPopScreen
import omnifit.mindcare.training.screen.pop.StopPopScreen
import omnifit.mindcare.training.view.CircularProgressBar
import omnifit.mindcare.training.view.HeadsetStatusVisualizer
import omnifit.mindcare.training.view.headsetStatusVisualizer
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.sdk27.coroutines.onTouch
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.runOnUiThread
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MeditationContentScreen : UIScreen() {

    val meditationContent: MeditationSource by lazy {
        arguments?.getInt(SCREEN_ARGUMENTS_MEDITATION_CONTENT_POSITION, 0)?.let { position ->
            ContentHelper.meditationPlaylist()[position]
        } ?: ContentHelper.meditationPlaylist()[0]
    }

    override fun onAttach(context: Context) {
        (meditationContent.source as? Uri)?.run {
            videoBackgroundSource = meditationContent
        }
        super.onAttach(context)
    }

    private var playingTime: TextView? = null
    private var analyzingView: View? = null
    private var visualizer: HeadsetStatusVisualizer? = null
    lateinit var transitionLayer: ConstraintLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            frameLayout {
                id = R.id.screen_widget_id_root_container

                if (meditationContent.utsSignature != SIGNATURE_UTS_MEDITATION) {
                    imageView {
                        id = R.id.screen_widget_id_05
                        setImageDrawable(BitmapDrawable(resources, meditationContent.thumbnail))
                        scaleType = ImageView.ScaleType.CENTER_CROP
                    }.lparams(matchParent, matchParent)
                }

                layoutTransition = LayoutTransition().apply {
                    addTransitionListener(object : LayoutTransition.TransitionListener {
                        override fun startTransition(
                            transition: LayoutTransition,
                            container: ViewGroup,
                            view: View,
                            transitionType: Int
                        ) {
                        }

                        override fun endTransition(
                            transition: LayoutTransition,
                            container: ViewGroup,
                            view: View,
                            transitionType: Int
                        ) {
                            when (view.id) {
                                R.id.screen_widget_transition_container -> {
                                    if (transitionType == LayoutTransition.DISAPPEARING) {
                                        view.isVisible = false
                                    }
                                }
                                R.id.screen_inner_widget_id_root_container -> {
                                    find<CircularProgressBar>(R.id.screen_inner_widget_id_03).startAnimation()
                                }
                            }
                            // 결과 분석 프로그래스 애니메이션 시작 시점
                            if (view.id == R.id.screen_inner_widget_id_root_container) {
                                find<CircularProgressBar>(R.id.screen_inner_widget_id_03).startAnimation()
                            }
                        }
                    })
                }

                onTouch(returnValue = true) { _, e ->
                    if (e.action == MotionEvent.ACTION_DOWN
                        && meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION
                    ) {
                        toggleTransitionAll()
                    }
                }

                // 헤드셋 비주얼라이저
                visualizer = headsetStatusVisualizer(
                    icon = R.drawable.ic_wearing,
                    frame = *intArrayOf(
                        R.drawable.ic_visualizer_effect_01,
                        R.drawable.ic_visualizer_effect_02,
                        R.drawable.ic_visualizer_effect_03,
                        R.drawable.ic_visualizer_effect_04,
                        R.drawable.ic_visualizer_effect_05,
                        R.drawable.ic_visualizer_effect_06,
                        R.drawable.ic_visualizer_effect_07,
                        R.drawable.ic_visualizer_effect_08
                    )
                ) {
                    id = R.id.screen_widget_id_02
                    bindSensorsAttachmentStatusChangeObserver(owner)
                }.lparams(matchParent, wrapContent) {
                    marginStart = dip(36.7f)
                    topMargin = dip(134.7f)
                }

                transitionLayer = constraintLayout {
                    id = R.id.screen_widget_transition_container

                    // 홈으로 or 이전으로 버튼 위치
                    imageButton(R.drawable.selector_return_back_wt) {
                        id = R.id.screen_widget_id_01
                        backgroundColor = Color.TRANSPARENT
                        onClick {
                            performBackPressed()
                        }
                    }.lparams(dip(164.0f), dip(56.0f)) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        marginStart = dip(42.7f)
                        topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                        topMargin = dip(43.0f)
                    }

                    // 진행시간
                    playingTime = textView("00:00") {
                        id = R.id.screen_widget_id_03
                        background = null
                        typeface = Font.latoLight
                        textSize = 93.3f
                        textColorResource = R.color.x_ffffff
                        letterSpacing = -0.03f
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        topToBottom = R.id.screen_widget_id_01
                        topMargin = dip(192.7f)
                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    }

                    textView(meditationContent.playTime.millisToFormatString(DTP_MM_SS)) {
                        id = R.id.screen_widget_id_04
                        background = null
                        typeface = Font.latoRegular
                        textSize = 29.3f
                        textColorResource = R.color.x_a8a8a8
                        letterSpacing = -0.01f
                        lines = 1
                        includeFontPadding = false
                        isVisible = meditationContent.utsSignature != SIGNATURE_UTS_MEDITATION_TRAINING
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = R.id.screen_widget_id_03
                        topToBottom = R.id.screen_widget_id_03
                        topMargin = dip(24.0f)
                        endToEnd = R.id.screen_widget_id_03
                    }

                }.lparams(matchParent, matchParent)

                analyzingView = AnalyzingViewComponent<MeditationContentScreen>(ServiceCategory.MEDITATIONS)
                    .createView(AnkoContext.create(requireContext(), this@MeditationContentScreen))
                    .also { v ->
                        addView(v)
                        v.isVisible = false
                    }
            }
        }.view

    override fun onDestroy() {
        releaseEffectSound()
        super.onDestroy()
    }

    @Suppress("CheckResult")
    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
        if (isEnter && meditationContent.utsSignature != SIGNATURE_UTS_MEDITATION) {
            AudioPlayer.observePlaybackStatus(this@MeditationContentScreen) { status ->
                when (status) {
                    is AudioPlayer.PlaybackStatus.Prepare -> {
                        updatePlayingTime()
                        if (meditationContent.useEffectSound) {
                            initEffectSounds {
                                initEffectSounds {
                                    startNeuroFeedback()
                                }
                            }
                        } else startNeuroFeedback()
                    }
                    is AudioPlayer.PlaybackStatus.Start -> doseNothing()
                    is AudioPlayer.PlaybackStatus.Playing -> {
                        updatePlayingTime(status.currentPosition)
                    }
                    is AudioPlayer.PlaybackStatus.Pause -> doseNothing()
                    is AudioPlayer.PlaybackStatus.Resume -> doseNothing()
                    is AudioPlayer.PlaybackStatus.Stop -> doseNothing()
                    is AudioPlayer.PlaybackStatus.Complete -> completedByOneTurn()
                    is AudioPlayer.PlaybackStatus.Error -> doseNothing()
                    else -> doseNothing()
                }
            }
            AudioPlayer.play(
                requireContext(),
                this@MeditationContentScreen,
                listOf((meditationContent.source as AudioPlayer.AudioSource))
            )
        }

        Single.just(true)
            .delay(3000L, TimeUnit.MILLISECONDS)
            .bindUntilEvent(screen, Lifecycle.Event.ON_DESTROY)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    if (meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION
                        && !isTransitionLayerHidden()
                    ) {
                        toggleTransitionAll()
                    }
                },
                { e -> Timber.e(e) }
            )

        // 측정에서 전달되는 값으로 처리 하기엔 페이지 이동이 불안정함
        Headset.observeSensorsAttachmentStatusChange(this@MeditationContentScreen) { status ->
            Timber.d("---> 센서 부착 상태 : $status , $neuroFeedbackProgressTimeSeconds, ${isNotResultsCanBeViewed()}")
            when(status) {
                SensorsAttachmentStatus.DETACHED -> {
                    if(neuroFeedbackProgressTimeSeconds > 0 && isNotResultsCanBeViewed()) {
                        SensorsAttachmentPopScreen().run {
                            positiveAction = {
                                Headset.stopMeasurement(CancelCause.Detached(SensorsAttachmentStatus.DETACHED))
                            }
                            show(this@MeditationContentScreen.fragmentManager)
                        }
                    }
                }
                else -> doseNothing()
            }
        }
    }

    private var forced: Boolean = false

    override fun onBackPressed(): Boolean {
        if (analyzingView?.isVisible == true) {
            alert(requireContext(), R.string.meditation_content_screen_060, gravity = Gravity.CENTER)
            return true
        }
        return stopNeuroFeedback(forced)
    }

    override fun onUserLeaveHint() {
        super.onUserLeaveHint()
        if (meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION) {
            cancelMeditation()
        }
    }

    private fun isTransitionLayerHidden(): Boolean {
//        return !find<ConstraintLayout>(R.id.screen_widget_transition_container).isVisible

        return !transitionLayer.isVisible
    }

    private fun toggleTransitionAll() {
//        find<ConstraintLayout>(R.id.screen_widget_transition_container).apply { isInvisible = isVisible }
        transitionLayer.apply {
            isInvisible = isVisible
        }
    }

    // 라이브 영상 파일 관련
    override fun onPlaybackStatusChanged(status: Int, curr: Int, duration: Int) {
        updatePlayingTime(curr)
        when (status) {
            UIFrame.PLAYBACK_STATUS_START -> startNeuroFeedback()
            UIFrame.PLAYBACK_STATUS_COMPLETE -> completedByOneTurn()
        }
    }

    private fun completedByOneTurn() {
        if (meditationContent.utsSignature != SIGNATURE_UTS_MEDITATION_TRAINING) {
            updatePlayingTime(meditationContent.playTime.toInt())
            stopNeuroFeedback()
        }
    }

    private fun updatePlayingTime(millis: Int = 0) {
        playingTime?.text = when (meditationContent.utsSignature) {
            // 자율훈련은 측정 진행 시간을 기준으로 디스플레이 함.
            SIGNATURE_UTS_MEDITATION_TRAINING -> {
                neuroFeedbackProgressTimeSeconds.secondsToFormatString(DTP_MM_SS)
            }
            else -> {
                neuroFeedbackProgressTimeSeconds = millis.millisToSeconds()
                millis.millisToSeconds().secondsToFormatString(DTP_MM_SS)

            }
        }
    }

    private fun cancelMeditation() {
        stopMeditationContent()
        if (!Headset.stopMeasurement(CancelCause.Forced)) {
            this@MeditationContentScreen.forced = true
            performBackPressed()
        }
        alert(requireContext(), R.string.meditation_content_screen_070)
    }

    private fun stopMeditationContent() {
        when (meditationContent.utsSignature) {
            SIGNATURE_UTS_MEDITATION -> stopLiveBackground()
            else -> {
                stopEffectSound()
                AudioPlayer.stop()
            }
        }
    }

    private var neuroFeedbackProgressTimeSeconds: Int = 0
    private var deepMeditationCount: Int = 0
    private var rewardPoint: Int = 0

    private fun isNotResultsCanBeViewed(): Boolean {
        return when (meditationContent.utsSignature) {
            // @formatter:off
            SIGNATURE_UTS_MEDITATION_MBSR -> neuroFeedbackProgressTimeSeconds < meditationContent.playTime.millisToSeconds()
            else                          -> neuroFeedbackProgressTimeSeconds < 60 * 3
            // @formatter:on
        }
    }

    private fun startNeuroFeedback() {
        Headset.startMeasurement(
            this@MeditationContentScreen,
            MEASUREMENT_DURATION_INFINITE,
            if (meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION) EyesStatus.OPENED else EyesStatus.CLOSED
        ) { status ->
            // yuumz: 측정 시간은 따로-음원타임과 측정타임의 간격차가 커서 따로 처리
            if (meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION_TRAINING)
                neuroFeedbackProgressTimeSeconds = status.elapsedTime

            when (status) {
                is MeasurementStatus.Start -> doseNothing()
                is MeasurementStatus.Measuring -> {
                    if (status.values.isNotEmpty()) {
                        checkDeepMeditation(
                            meditationContent.micSignature,
                            when (meditationContent.micSignature) {
                                SIGNATURE_MIC_BRAIN_STRESS -> status.values[RESULT_ITEM_L_HBETA]
                                SIGNATURE_MIC_RELAXATION -> status.values[RESULT_ITEM_RELAXATION]
                                SIGNATURE_MIC_BRAIN_ACTIVITY -> {
                                    // 원시데이터에서 지표 5단계로 변환
                                    status.values
                                        .splitPowerSpectrum(15, 143)
                                        .computeSef90Hz()
                                        .let { hz ->
                                            AnalysisAlgorithm.transformSef90HzTo5pValue(hz)
                                        }
                                }
                                else -> -1.0
                            },
                            meditationContent.utsSignature == SIGNATURE_UTS_MEDITATION,
                            meditationContent.useEffectSound
                        )
                    }
                }
                is MeasurementStatus.Stop -> {
                    Timber.d("---> 명상테라피 측정 종료 : $neuroFeedbackProgressTimeSeconds")
                    stopMeditationContent()
                    if (!isNotResultsCanBeViewed()) {
                        ContentResultHelper.createMeditationResult(
                            requireContext(),
                            neuroFeedbackProgressTimeSeconds,
                            meditationContent.sequence,
                            meditationContent.micSignature,
                            deepMeditationCount,
                            rewardPoint,
                            meditationContent.albumTitle,
                            status.result
                        ) { r ->
                            r?.let { m ->
                                if (onUserLeave) {
                                    runOnUiThread {
                                        screenTo(
//                                            MeditationContentResultScreen().withArguments(
//                                                Pair(SCREEN_ARGUMENTS_RESULT_TYPE, RESULT_MEDITATION),
//                                                Pair(
//                                                    SCREEN_ARGUMENTS_MEDITATION_CONTENT_TITLE,
//                                                    meditationContent.sourceTitle
//                                                )
//                                            )
                                            MeditationContentResultScreen().withArguments(
                                                Pair(SCREEN_ARGUMENTS_RESULT_TYPE, RESULT_MEDITATION)
                                            )
                                        )
                                        alert(
                                            requireContext(),
                                            getString(R.string.meditation_content_screen_070).replaceNewLineAsSpace()
                                        )
                                    }
                                }
                            }
                        }
                        // 3분이 지나고 나서 결과 화면으로 이동이 가능하다
                        if (!onUserLeave) {
                            this@MeditationContentScreen.view?.run {
                                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888).also { bmp ->
                                    draw(Canvas(bmp))
                                }
                            }?.let { bmp ->
                                Dali.create(context)
                                    .load(bmp)
                                    .blurRadius(25)
                                    .concurrent()
                                    .skipCache()
                                    .get()
                            }.let { screenshot ->
                                analyzingView?.backgroundDrawable = screenshot
                                analyzingView?.isVisible = true
                            }
                        }
                    }
                }

                is MeasurementStatus.Cancel -> {
                    Timber.d("--> 명상테라피 측정 취소 : [${status.cause}]")
                    forced = true
                    performBackPressed()
                }
            }
        }
    }

    private fun stopNeuroFeedback(forced: Boolean = false): Boolean {
        if (forced) {
            return screenTo(MeditationContentListScreen())
        }
        when (meditationContent.utsSignature) {
            // @formatter:off
            SIGNATURE_UTS_MEDITATION_MBSR -> Pair(isNotResultsCanBeViewed(), 0)
            else                          -> Pair(isNotResultsCanBeViewed(), 1)
            // @formatter:on
        }.also { (alert, type) ->
            // type : 0 - MBSR, 1 - 명상/자율훈련
            if (alert) {
                StopPopScreen()
                    .withArguments(
                        Pair(
                            FRAGMENT_ARGUMENTS_COMMENT,
                            if (type == 0) R.string.meditation_content_screen_030 else R.string.meditation_content_screen_040
                        ),
                        Pair(FRAGMENT_ARGUMENTS_MESSAGE, R.string.meditation_content_screen_050)
                    )
                    .run {
                        positiveAction = {
                            stopMeditationContent()
                            if (!Headset.stopMeasurement(CancelCause.Forced)) {
                                this@MeditationContentScreen.forced = true
                                performBackPressed()
                            }
                        }
                        show(this@MeditationContentScreen.fragmentManager)
                    }
            } else {
                stopMeditationContent()
                Headset.stopMeasurement()
            }
        }
        return true
    }

    private fun checkDeepMeditation(
        micSignature: String,
        indicatorValue: Double,
        useEffectAnim: Boolean = false,
        useEffectSound: Boolean = false
    ) {
        if (useEffectSound) {
            when (indicatorValue) {
                in 0.0..2.9,
                in 3.0..6.9 -> stopEffectSound()
                in 7.0..10.0 -> effectCalmLevelSound()
                else -> doseNothing()
            }
        }

        ServiceAlgorithm.checkDeepMeditationDuringMeditation(micSignature, indicatorValue)
            .let { able ->
                if (able) {
                    if (useEffectAnim) {
                        visualizer?.effectOn()
                    }
                    deepMeditationCount++
                    Timber.i("--> deep : $deepMeditationCount")

                    rewardPoint += ServiceAlgorithm.calcMeditationRewardPoint(micSignature, indicatorValue)
                } else {
                    if (useEffectAnim) {
                        visualizer?.effectOff()
                    }
                }
            }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ SOUND POOL
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private var isEffectSoundLoadCompleted: Boolean = false

    private fun initEffectSounds(onComplete: () -> Unit) {
        if (isEffectSoundLoadCompleted) {
            onComplete()
            return
        }
        effectSound.setOnLoadCompleteListener { _, _, status ->
            isEffectSoundLoadCompleted = status == 0
            if (isEffectSoundLoadCompleted) {
                onComplete()
            }
        }
        effectSoundId
    }

    private val effectSound: SoundPool by lazy {
        SoundPool.Builder()
            .setMaxStreams(1)
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            .build()
    }

    private val effectSoundId: Int by lazy {
        effectSound.load(context!!, R.raw.effect_calm, 1)
    }

    private var playingEffectSoundId: Int = 0
    private var playingEffectSoundStreamId: Int = 0

    private fun effectCalmLevelSound() {
        if (playingEffectSoundId != effectSoundId) {
            playingEffectSoundId = effectSoundId
            playingEffectSoundStreamId = effectSound.play(playingEffectSoundId, 1f, 1f, 0, -1, 1f)
        }
    }

    private fun stopEffectSound() {
        if (playingEffectSoundId != 0) {
            playingEffectSoundId = 0
            effectSound.stop(playingEffectSoundStreamId)
        }
    }

    private fun releaseEffectSound() {
        effectSound.release()
    }
}