package omnifit.mindcare.training.screen.pop

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import androidx.fragment.app.UIPopScreen
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.algorithm.ServiceAlgorithm
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.component.ResultGraphPopComponent
import omnifit.mindcare.training.configuration.MeasurementConfiguration
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.onPageChangeListener
import org.jetbrains.anko.support.v4.viewPager
import kotlin.math.absoluteValue

class MeasurementResultPopScreen : UIPopScreen() {

    lateinit var screenUiComponent: MeasurementResultComponent

    override fun <T : UIPopScreen> T.createScreenUiComponent(): AnkoComponent<T> =
        MeasurementResultComponent().apply { screenUiComponent = this } as AnkoComponent<T>

    private var arguments_: Bundle? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments_ = arguments
    }

    override fun onStart() {
        super.onStart()
        dialog!!.window.setWindowAnimations(R.style.PopScreenAnimation)
    }

    val result: MeasurementConfiguration? by lazy {
        MeasurementConfiguration.find(requireContext())
    }

    val shortcutResultItem: Int by lazy {
        arguments_?.getInt(SCREEN_ARGUMENTS_SHORTCUT_RESULT_ITEM, 0) ?: 0
    }

    val measurementIndicator: IntArray by lazy {
        intArrayOf(
            result?.brainStressIndicator?.ordinal ?: 0,
            result?.concentrationIndicator?.ordinal ?: 0,
            result?.brainActivityIndicator?.ordinal ?: 0,
            result?.balanceIndicator?.ordinal ?: 0
        )
    }

    val measurementSimpleLevel: Array<MeasurementSimpleLevel> by lazy {
        arrayOf(
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BRAIN_STRESS,
                    measurementIndicator[MEASUREMENT_ITEM_BRAIN_STRESS]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_CONCENTRATION,
                    measurementIndicator[MEASUREMENT_ITEM_CONCENTRATION]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BRAIN_ACTIVITY,
                    measurementIndicator[MEASUREMENT_ITEM_BRAIN_ACTIVITY]
                )
            ),
            MeasurementSimpleLevel.of(
                ServiceAlgorithm.measurementStep(
                    MEASUREMENT_ITEM_BALANCE,
                    measurementIndicator[MEASUREMENT_ITEM_BALANCE]
                )
            )
        )
    }

    val brainStressAnalysisTitle: Array<String> by lazy { resources.getStringArray(R.array.brain_stress_analysis_title) }
    val concentrationAnalysisTitle: Array<String> by lazy { resources.getStringArray(R.array.concentration_analysis_title) }
    val brainActivityAnalysisTitle: Array<String> by lazy { resources.getStringArray(R.array.brain_activity_analysis_title) }
    val balanceAnalysisTitle: Array<String> by lazy { resources.getStringArray(R.array.balance_analysis_title) }

    val resultItemExpressions: Array<String> by lazy { resources.getStringArray(R.array.measurement_item_expressions) }
    val resultItemAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.measurement_item_analysis_explanations) }
    val brainStressAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.brain_stress_analysis_explanations) }
    val concentrationAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.concentration_analysis_explanations) }
    val concentrationTypeAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.concentration_type_analysis_explanations) }
    val brainActivityAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.brain_activity_analysis_explanations) }
    val balanceAnalysisExplanations: Array<String> by lazy { resources.getStringArray(R.array.balance_analysis_explanations) }

}

class MeasurementResultComponent :
    AnkoLifecycleComponent<MeasurementResultPopScreen> {

    private var resultExpressions: NonSpacingTextView? = null
    private var items: ViewPager? = null
    private var prevItem: ImageView? = null
    private var nextItem: ImageView? = null
    private var navigator: RadioGroup? = null
    private var navigator01: RadioButton? = null
    private var navigator02: RadioButton? = null
    private var navigator03: RadioButton? = null
    private var navigator04: RadioButton? = null

    override fun createView(ui: AnkoContext<MeasurementResultPopScreen>): View = with(ui) {
        constraintLayout {
            backgroundColorResource = R.color.x_000000_op15

            prevItem = imageView(R.drawable.ic_left_arrow) {
                onClick {
                    showPrevResult()
                }
            }.lparams(wrapContent, wrapContent) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                marginStart = dip(46.7f)
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            constraintLayout {
                backgroundResource = R.drawable.shape_round_7_bg_pop

                // 타이틀
                resultExpressions = nonSpacingTextView {
                    id = R.id.screen_widget_id_01
                    typeface = Font.kopubDotumBold
                    textSize = 30.0f
                    textColorResource = R.color.x_1c1c1c
                    lines = 1
                    letterSpacing = -0.01f
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(50.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(35.3f)
                }

                // 닫기
                imageButton(R.drawable.selector_close) {
                    id = R.id.screen_widget_id_02
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        owner.dismiss()
                    }
                }.lparams(wrapContent, wrapContent) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(50.0f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                items = viewPager {
                    offscreenPageLimit = 0
                    adapter = object : PagerAdapter() {
                        override fun instantiateItem(container: ViewGroup, position: Int): Any {
                            // yuumz: 추후 디비 사용시 디비에서 코드값을 가져오는 방식으로 처리해야함
                            return object : AnkoComponent<MeasurementResultPopScreen> {
                                override fun createView(ui: AnkoContext<MeasurementResultPopScreen>): View =
                                    with(ui) {
                                        scrollView {
                                            constraintLayout {

                                                imageView(R.drawable.quotes_front_bk) {
                                                }.lparams(wrapContent, wrapContent) {
                                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                                    topMargin = dip(79.0f)
                                                    endToStart = R.id.screen_inner_widget_id_01
                                                    marginEnd = dip(10.7f)
                                                }
                                                textView {
                                                    id = R.id.screen_inner_widget_id_01
                                                    text = updateResultAnalysisTitle(owner, position)
//                                                    textResource = R.string.test
                                                    typeface = Font.kopubDotumMedium
                                                    textSize = 40.0f
                                                    textColorResource = R.color.x_1c1c1c
                                                    lines = 1
                                                    includeFontPadding = false
                                                    letterSpacing = -0.03f
                                                }.lparams(wrapContent, wrapContent) {
                                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                                    topMargin = dip(82.7f)
                                                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                                }

                                                imageView(R.drawable.quotes_back_bk) {
                                                }.lparams(wrapContent, wrapContent) {
                                                    startToEnd = R.id.screen_inner_widget_id_01
                                                    marginStart = dip(10.7f)
                                                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                                                    topMargin = dip(79.0f)
                                                }

                                                //
                                                ResultGraphPopComponent<MeasurementResultPopScreen>(
                                                    context = owner.requireContext(),
                                                    _item = position,
                                                    _result = if (position == MEASUREMENT_ITEM_BALANCE) (owner.measurementIndicator[position] - 4).absoluteValue
                                                    else owner.measurementIndicator[position],
                                                    _simpleLevel = owner.measurementSimpleLevel[position]
                                                )
                                                    .createView(AnkoContext.create(owner.requireContext(), owner))
                                                    .lparams(matchParent, wrapContent) {
                                                        startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                                        topToBottom = R.id.screen_inner_widget_id_01
                                                        topMargin = dip(29.3f)
                                                        endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                                    }
                                                    .also { v ->
                                                        addView(v)
                                                    }


                                                // 분석 결과 설명 1
                                                linearLayout {
                                                    id = R.id.screen_inner_widget_id_02

                                                    imageView(R.drawable.shape_result_explanation_dot) {
                                                    }.lparams(dip(4.0f), dip(4.0f)) {
                                                        topMargin = dip(6.3f)
                                                    }

                                                    nonSpacingTextView(
                                                        when (position) {
                                                            MEASUREMENT_ITEM_CONCENTRATION -> String.format(
                                                                owner.resultItemAnalysisExplanations[position],
                                                                owner.concentrationTypeAnalysisExplanations[owner.result?.concentrationTypeValue
                                                                    ?: 6]
                                                            )
                                                            else -> owner.resultItemAnalysisExplanations[position]
                                                        }
                                                    ) {
                                                        typeface = Font.kopubDotumMedium
                                                        textSize = 21.0f
                                                        textColorResource = R.color.x_898989
                                                        letterSpacing = -0.01f
                                                        includeFontPadding = false
                                                    }.lparams(matchParent, wrapContent) {
                                                        marginStart = dip(11.3f)
                                                    }
                                                }.lparams(matchConstraint, wrapContent) {
                                                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                                                    marginStart = dip(51.3f)
                                                    topToBottom = R.id.screen_inner_widget_id_01
                                                    topMargin = dip(194.0f)
                                                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                                                    marginEnd = dip(51.3f)
                                                }

                                                // 분석 결과 설명 2
                                                linearLayout {
                                                    imageView(R.drawable.shape_result_explanation_dot) {
                                                    }.lparams(dip(4.0f), dip(4.0f)) {
                                                        topMargin = dip(6.3f)
                                                    }

                                                    nonSpacingTextView(
                                                        when (position) {
                                                            MEASUREMENT_ITEM_BRAIN_STRESS -> owner.brainStressAnalysisExplanations[owner.measurementIndicator[position]]
                                                            MEASUREMENT_ITEM_CONCENTRATION -> owner.concentrationAnalysisExplanations[owner.measurementIndicator[position]]
                                                            MEASUREMENT_ITEM_BRAIN_ACTIVITY -> owner.brainActivityAnalysisExplanations[owner.measurementIndicator[position]]
                                                            else -> owner.balanceAnalysisExplanations[owner.measurementIndicator[position]]
                                                        }
                                                    ) {
                                                        typeface = Font.kopubDotumMedium
                                                        textSize = 21.0f
                                                        textColorResource = R.color.x_898989
                                                        letterSpacing = -0.01f
                                                        includeFontPadding = false
                                                    }.lparams(matchParent, wrapContent) {
                                                        marginStart = dip(11.3f)
                                                    }
                                                }.lparams(matchConstraint, wrapContent) {
                                                    startToStart = R.id.screen_inner_widget_id_02
                                                    topToBottom = R.id.screen_inner_widget_id_02
                                                    topMargin = dip(15.3f)
                                                    endToEnd = R.id.screen_inner_widget_id_02
                                                }
                                            }
                                        }.apply {
                                            layoutParams = ConstraintLayout.LayoutParams(
                                                ConstraintLayout.LayoutParams.MATCH_PARENT,
                                                ConstraintLayout.LayoutParams.MATCH_PARENT
                                            )
                                        }
                                    }
                            }.createView(
                                AnkoContext.create(
                                    owner.requireContext(),
                                    owner
                                )
                            )
                                .also { v ->
                                    v.tag = position
                                    container.addView(v)
                                }
                        }

                        override fun isViewFromObject(view: View, `object`: Any): Boolean = view == (`object` as? View)
                        override fun getCount(): Int = 4
                        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) =
                            container.removeView(`object` as? View)

                        override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
                            super.setPrimaryItem(container, position, `object`)
                        }

                    }
                    onPageChangeListener {
                        onPageSelected { n ->
                            updateResultExpressions(owner, n)
                            updatePrevWithNext(n)
                            updateNavigator(n)
                        }
                    }
                    currentItem = owner.shortcutResultItem
                }.lparams(matchParent, dip(438.7f)) {
                    topToBottom = R.id.screen_widget_id_01
                }

                navigator = radioGroup {
                    orientation = LinearLayout.HORIZONTAL

                    navigator01 = radioButton {
                        backgroundResource = R.drawable.selector_navigation
                        buttonDrawable = null
                    }.lparams(dip(8.0f), dip(8.0f))

                    navigator02 = radioButton {
                        backgroundResource = R.drawable.selector_navigation
                        buttonDrawable = null
                    }.lparams(dip(8.0f), dip(8.0f)) {
                        marginStart = dip(16.0f)
                    }

                    navigator03 = radioButton {
                        backgroundResource = R.drawable.selector_navigation
                        buttonDrawable = null
                    }.lparams(dip(8.0f), dip(8.0f)) {
                        marginStart = dip(16.0f)
                    }

                    navigator04 = radioButton {
                        backgroundResource = R.drawable.selector_navigation
                        buttonDrawable = null
                    }.lparams(dip(8.0f), dip(8.0f)) {
                        marginStart = dip(16.0f)
                    }

                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomMargin = dip(52.7f)

                }
            }.lparams(dip(977.3f), dip(565.3f)) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            nextItem = imageView(R.drawable.ic_right_arrow) {
                onClick {
                    showNextResult()
                }
            }.lparams(wrapContent, wrapContent) {
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                marginEnd = dip(46.7f)
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }
        }
    }

    override fun onViewCreated(owner: MeasurementResultPopScreen) {
        updateResultExpressions(owner, owner.shortcutResultItem)
        updatePrevWithNext(owner.shortcutResultItem)
        updateNavigator(owner.shortcutResultItem)
    }

    @SuppressLint("ResourceType")
    private fun updateResultAnalysisTitle(owner: MeasurementResultPopScreen, position: Int): SpannableStringBuilder {
        var result = when (position) {
            MEASUREMENT_ITEM_BRAIN_STRESS -> owner.brainStressAnalysisTitle[owner.measurementIndicator[position]]
            MEASUREMENT_ITEM_CONCENTRATION -> owner.concentrationAnalysisTitle[owner.measurementIndicator[position]]
            MEASUREMENT_ITEM_BRAIN_ACTIVITY -> owner.brainActivityAnalysisTitle[owner.measurementIndicator[position]]
            else -> owner.balanceAnalysisTitle[owner.measurementIndicator[position]]
        }.split("*")

        var ssb = SpannableStringBuilder()

        ssb.append(result[0])
        var startIdx = result[0].length

        var colorSpan = ForegroundColorSpan(owner.resources.getColor(owner.measurementSimpleLevel[position].color))
        ssb.append(result[1])
        var endIdx = startIdx + result[1].length
        ssb.setSpan(
            colorSpan,
            startIdx,
            endIdx,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        ssb.append(result[2])

        return ssb
    }

    private fun updateResultExpressions(owner: MeasurementResultPopScreen, position: Int) {
        resultExpressions?.text = owner.resultItemExpressions[position]
    }

    private fun updatePrevWithNext(position: Int) {
        when (position) {
            MEASUREMENT_ITEM_BRAIN_STRESS -> prevItem?.isInvisible = true
            MEASUREMENT_ITEM_BALANCE -> nextItem?.isInvisible = true
            else -> {
                prevItem?.isInvisible = false
                nextItem?.isInvisible = false
            }
        }
    }

    private fun updateNavigator(position: Int) {
//        (navigator?.getChildAt(position) as RadioButton).isChecked = true
        when (position) {
            MEASUREMENT_ITEM_BRAIN_STRESS -> navigator01?.isChecked = true
            MEASUREMENT_ITEM_CONCENTRATION -> navigator02?.isChecked = true
            MEASUREMENT_ITEM_BRAIN_ACTIVITY -> navigator03?.isChecked = true
            else -> navigator04?.isChecked = true
        }
    }

    private fun showPrevResult() {
        items?.run {
            if (currentItem > 0) currentItem--
        }
    }

    private fun showNextResult() {
        items?.run {
            if (currentItem < adapter!!.count - 1) currentItem++
        }
    }
}