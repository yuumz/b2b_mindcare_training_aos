package omnifit.mindcare.training.screen

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintLayout.LayoutParams.PARENT_ID
import androidx.core.view.isVisible
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.batteryLevelChangeObserver
import omnifit.mindcare.training.configuration.UserConfiguration
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.screen.pop.HeadsetPopScreen
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.view.NonSpacingTextView
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI

class MainScreen : UIScreen() {
    init {
        isRootScreen = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Headset.connectDeviceIfNeeded(context)
    }

    lateinit var lastMeasurementResult: ConstraintLayout
    lateinit var latestMeasurementdate: NonSpacingTextView
    lateinit var latestMeasurmentScore: NonSpacingTextView
    lateinit var noMeasurementResult: NonSpacingTextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                backgroundResource = R.drawable.img_bg_main

                // 홈으로 or 이전으로 버튼 위치
                view {
                    id = R.id.screen_widget_id_01
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                imageView(R.drawable.ic_service_logo_01) {
                    id = R.id.screen_widget_id_02
                }.lparams(wrapContent, wrapContent) {
                    startToStart = PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 밧데리잔량
                constraintLayout {
                    id = R.id.screen_widget_id_03
                    backgroundResource = R.drawable.shape_round_28_op15_bg_headset

                    // 헤드셋 이미지
                    imageView(R.drawable.ic_headset_wt) {
                        id = R.id.screen_inner_widget_id_01
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        marginStart = dip(25.3f)
                        topToTop = PARENT_ID
                        bottomToBottom = PARENT_ID
                    }
                    // 헤드셋 밧데리 잔량
                    textView {
                        id = R.id.screen_inner_widget_id_02
                        typeface = Font.latoRegular
                        textSize = 21.3f
                        textColorResource = R.color.x_ffffff
                        lines = 1
                        includeFontPadding = false
                        batteryLevelChangeObserver(owner)
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_01
                        endToEnd = PARENT_ID
                        topToTop = PARENT_ID
                        bottomToBottom = PARENT_ID
                        horizontalBias = 0.3f
                        verticalBias = 0.5f
                    }
                }.lparams(dip(137.3f), dip(56.0f)) {
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = PARENT_ID
                    marginEnd = dip(106.7f)
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 설정
                imageButton(R.drawable.selector_setting_wt) {
                    id = R.id.screen_widget_id_04
                    scaleType = ImageView.ScaleType.CENTER
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        HeadsetPopScreen().run { show(this@MainScreen.fragmentManager) }
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_03
                    marginStart = dip(28.0f)
                    topToTop = R.id.screen_widget_id_03
                    bottomToBottom = R.id.screen_widget_id_03
                }

                // 유저정보
                imageButton(R.drawable.selector_main_01) {
                    id = R.id.screen_widget_id_05
                    backgroundColor = Color.TRANSPARENT
                }.lparams(dip(561.3f), dip(198.7f)) {
                    startToStart = PARENT_ID
                    marginStart = dip(64.7f)
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(36.3f)
                }

                // 뇌파측정
                imageButton(R.drawable.selector_main_02) {
                    id = R.id.screen_widget_id_06
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        screenTo(MeasurementScreen())
                    }
                }.lparams(dip(561.3f), dip(0.0f)) {
                    endToEnd = PARENT_ID
                    marginEnd = dip(63.3f)
                    topToTop = R.id.screen_widget_id_05
                    bottomToBottom = R.id.screen_widget_id_05
                }
                // 뇌파측정 타이틀
                nonSpacingTextView(R.string.main_screen_050) {
                    id = R.id.screen_inner_widget_id_03
                    typeface = Font.kopubDotumBold
                    textSize = 31.0f
                    textColorResource = R.color.x_ffffff
                    lines = 1
                    includeFontPadding = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_06
                    marginStart = dip(48.0f)
                    topToTop = R.id.screen_widget_id_06
                    topMargin = dip(38.7f)
                }
                // 뇌파측정 - 최근측정일 및 점수
                lastMeasurementResult = constraintLayout {
                    id = R.id.screen_inner_widget_id_04
                    isVisible = false

                    // 최근 측정일
                    latestMeasurementdate = nonSpacingTextView {
                        id = R.id.screen_inner_widget_id_04
                        typeface = Font.kopubDotumMedium
                        textSize = 19.0f
                        textColorResource = R.color.x_ffffff_op50
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = PARENT_ID
                        topToTop = PARENT_ID
                    }

                    nonSpacingTextView(R.string.main_screen_070) {
                        id = R.id.screen_inner_widget_id_05
                        typeface = Font.kopubDotumMedium
                        textSize = 23.0f
                        textColorResource = R.color.x_ffffff
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToStart = R.id.screen_inner_widget_id_04
                        topToBottom = R.id.screen_inner_widget_id_04
                        topMargin = dip(36.0f)
                    }

                    // 두뇌컨디션 점수
                    latestMeasurmentScore = nonSpacingTextView {
                        id = R.id.screen_inner_widget_id_06
                        typeface = Font.latoBold
                        textSize = 58.0f
                        textColorResource = R.color.x_ffffff
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_05
                        marginStart = dip(18.7f)
                        bottomToBottom = R.id.screen_inner_widget_id_05
                    }

                    nonSpacingTextView(R.string.main_screen_080) {
                        id = R.id.screen_inner_widget_id_07
                        typeface = Font.kopubDotumMedium
                        textSize = 23.0f
                        textColorResource = R.color.x_ffffff
                        lines = 1
                        includeFontPadding = false
                    }.lparams(wrapContent, wrapContent) {
                        startToEnd = R.id.screen_inner_widget_id_06
                        marginStart = dip(6.7f)
                        bottomToBottom = R.id.screen_inner_widget_id_06
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_inner_widget_id_03
                    topToBottom = R.id.screen_inner_widget_id_03
                    topMargin = dip(8.7f)
                }

                // 뇌파측정 - 한번도 하지 않았을때
                noMeasurementResult = nonSpacingTextView(R.string.main_screen_090) {
                    id = R.id.screen_inner_widget_id_08
                    typeface = Font.kopubDotumMedium
                    textSize = 23.0f
                    textColorResource = R.color.x_ffffff
                    lines = 1
                    includeFontPadding = false
                    isVisible = false
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_inner_widget_id_03
                    topToBottom = R.id.screen_inner_widget_id_03
                    topMargin = dip(45.0f)
                }

                // 명상테라피
                imageButton(R.drawable.selector_main_03) {
                    id = R.id.screen_widget_id_07
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        screenTo(MeditationContentListScreen())
                    }
                }.lparams(dip(364.0f), dip(379.3f)) {
                    startToStart = R.id.screen_widget_id_05
                    bottomToBottom = PARENT_ID
                    bottomMargin = dip(58.7f)
                }

                // 힐링뮤직
                imageButton(R.drawable.selector_main_04) {
                    id = R.id.screen_widget_id_08
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        when {
                            !Headset.isConnected() -> HeadsetPopScreen().run { show(this@MainScreen.fragmentManager) }
                            !Headset.isSensorsAttached() -> alert(
                                context,
                                R.string.notifications_020,
                                R.color.x_000000_op60
                            )
                            else -> screenTo(HealingMusicContentScreen())
                        }
                    }
                }.lparams(dip(364.0f), dip(379.3f)) {
                    startToEnd = R.id.screen_widget_id_07
                    topToTop = R.id.screen_widget_id_07
                    bottomToBottom = R.id.screen_widget_id_07
                    endToStart = R.id.screen_widget_id_09
                }

                // 게임
                imageButton(R.drawable.selector_main_05) {
                    id = R.id.screen_widget_id_09
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        screenTo(GameListScreen())
                    }
                }.lparams(dip(364.0f), dip(379.3f)) {
                    endToEnd = R.id.screen_widget_id_06
                    topToTop = R.id.screen_widget_id_07
                    bottomToBottom = R.id.screen_widget_id_07
                }
            }.applyRecursively {
                updateEEGMeasurementResult(owner.requireContext())
            }
        }.view

    override fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    @Suppress("CheckResult")
    override fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
    }

    override fun onBackPressed(): Boolean {
        alert(requireContext(), R.string.notifications_010)
        return appFinish()
    }

    private fun updateEEGMeasurementResult(context: Context) {
        if (UserConfiguration.isRegistered(context)) {
            lastMeasurementResult.isVisible = true
            UserConfiguration.get(requireContext()).also { configuration ->
                latestMeasurementdate.text = context.getString(R.string.main_screen_060, configuration.latestMeasurementdate)
                latestMeasurmentScore.text = configuration.brainScore.toString()
            }
        } else {
            noMeasurementResult.isVisible = true
        }
    }
}