package omnifit.mindcare.training.screen

import android.animation.LayoutTransition
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.media.AudioAttributes
import android.media.SoundPool
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.Lifecycle
import at.favre.lib.dali.Dali
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import hu.akarnokd.rxjava2.operators.FlowableTransformers
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import omnifit.commons.core.*
import omnifit.mindcare.training.*
import omnifit.mindcare.training.R
import omnifit.mindcare.training.common.*
import omnifit.mindcare.training.component.AnalyzingViewComponent
import omnifit.mindcare.training.configuration.UserConfiguration
import omnifit.mindcare.training.headset.*
import omnifit.mindcare.training.headset.fx2.MEASUREMENT_DURATION_1_MINUTES
import omnifit.mindcare.training.helper.ContentResultHelper
import omnifit.mindcare.training.screen.pop.HeadsetPopScreen
import omnifit.mindcare.training.screen.pop.StopPopScreen
import omnifit.mindcare.training.view.*
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.withArguments
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.properties.Delegates

class MeasurementScreen : UIScreen() {

    private var progressMessage: TextSwitcher? = null
    private var progressSubMessage: TextSwitcher? = null
    private var subMessageFront: ImageView? = null
    private var subMessageBack: ImageView? = null
    private var progressTime: TextView? = null
    private var progressButton: ImageButton? = null
    private var analyzingView: View? = null

    private var wave: BrainWave4Real? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        UI {
            constraintLayout {
                id = R.id.screen_widget_id_root_container
                backgroundResource = R.drawable.img_bg_measurement

                layoutTransition = LayoutTransition()
                    .apply {
                        addTransitionListener(object : LayoutTransition.TransitionListener {
                            override fun startTransition(
                                transition: LayoutTransition,
                                container: ViewGroup,
                                view: View,
                                transitionType: Int
                            ) {
                            }

                            override fun endTransition(
                                transition: LayoutTransition,
                                container: ViewGroup,
                                view: View,
                                transitionType: Int
                            ) {
                                // 결과 분석 프로그래스 애니메이션 시작 시점
                                if (view.id == R.id.screen_inner_widget_id_root_container) {
                                    find<CircularProgressBar>(R.id.screen_inner_widget_id_03).startAnimation()
                                }
                            }
                        })
                    }

                // 홈으로 or 이전으로 버튼 위치
                imageButton(R.drawable.selector_return_back_wt) {
                    id = R.id.screen_widget_id_01
                    backgroundColor = Color.TRANSPARENT
                    onClick {
                        if (progressStep == PROGRESS_STEP_MEASUREMENT_READY) screenTo(MainScreen())
                        else performBackPressed()
                    }
                }.lparams(dip(164.0f), dip(56.0f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(43.0f)
                }

                // 로고
                nonSpacingTextView(R.string.measurement_screen_010) {
                    id = R.id.screen_widget_id_02
                    typeface = Font.kopubDotumMedium
                    textSize = 32.0f
                    textColorResource = R.color.x_ffffff
                    lines = 1
                    includeFontPadding = false
                    letterSpacing = -0.03f
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToTop = R.id.screen_widget_id_01
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = R.id.screen_widget_id_01
                }

                // 측정멘트
                progressMessage = textSwitcher {
                    id = R.id.screen_widget_id_03
                    setFactory {
                        NonSpacingTextView(context).apply {
                            typeface = Font.kopubDotumBold
                            textSize = 43.0f
                            textColorResource = R.color.x_ffffff
                            letterSpacing = -0.03f
                            includeFontPadding = false
                            gravity = Gravity.CENTER
                            layoutParams = FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            )
                        }
                    }
                    measureAllChildren = false
                    inAnimation = fadeIn
                    outAnimation = fadeOut
                    setCurrentText(getString(R.string.measurement_screen_020))
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(106.0f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                }

                subMessageFront = imageView(R.drawable.quotes_front_wt) {
                }.lparams(wrapContent, wrapContent) {
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(17.3f)
                    endToStart = R.id.screen_widget_id_04
                    marginEnd = dip(10.7f)
                }

                // 측정 서브내용
                progressSubMessage = textSwitcher {
                    id = R.id.screen_widget_id_04
                    setFactory {
                        NonSpacingTextView(context).apply {
                            typeface = Font.kopubDotumMedium
                            textSize = 24.0f
                            textColorResource = R.color.x_ffffff_op50
                            letterSpacing = -0.03f
                            includeFontPadding = false
                            gravity = Gravity.CENTER
                            layoutParams = FrameLayout.LayoutParams(
                                FrameLayout.LayoutParams.WRAP_CONTENT,
                                FrameLayout.LayoutParams.WRAP_CONTENT
                            )
                        }
                    }
                    measureAllChildren = false
                    inAnimation = fadeIn
                    outAnimation = fadeOut
                    setCurrentText(getString(R.string.measurement_screen_030))
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_03
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(27.3f)
                    endToEnd = R.id.screen_widget_id_03
                }

                subMessageBack = imageView(R.drawable.quotes_back_wt) {
                }.lparams(wrapContent, wrapContent) {
                    startToEnd = R.id.screen_widget_id_04
                    marginStart = dip(10.7f)
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(17.3f)
                }

                // 측정시간
                progressTime = textView(
                    MEASUREMENT_DURATION_1_MINUTES.secondsToFormatString(
                        DTP_MM_SS
                    )
                ) {
                    id = R.id.screen_widget_id_05
                    typeface = Font.latoRegular
                    textSize = 80.7f
                    textColorResource = R.color.x_ffffff_op50
                    lines = 1
                    includeFontPadding = false
                    gravity = Gravity.CENTER
                    isInvisible = true
                }.lparams(wrapContent, wrapContent) {
                    startToStart = R.id.screen_widget_id_03
                    topToBottom = R.id.screen_widget_id_03
                    topMargin = dip(38.0f)
                    endToEnd = R.id.screen_widget_id_03
                }

                // 그래프
                wave = brainWave4Real {
                    bindBrainWaveChangeObserver(owner)
                }.lparams(matchParent, dip(84.0f)) {
                    topToBottom = R.id.screen_widget_id_01
                    topMargin = dip(327.3f)
                }
//                wave = brainWave4Visualizer {
//                    bindSensorsAttachmentStatusChangeObserver(owner)
//                }.lparams(matchParent, dip(84.0f)) {
//                    topToBottom = R.id.screen_widget_id_01
//                    topMargin = dip(327.3f)
//                }

                // 진행 버튼
                progressButton = imageButton(R.drawable.selector_measuremt_start) {
                    id = R.id.screen_widget_id_06
                    backgroundColor = Color.TRANSPARENT

                    onClick {
                        when (progressStep) {
                            PROGRESS_STEP_MEASUREMENT_READY -> {
                                when {
                                    !Headset.isConnected() -> HeadsetPopScreen().run { show(this@MeasurementScreen.fragmentManager) }
                                    !Headset.isSensorsAttached() -> alert(
                                        context,
                                        R.string.notifications_020,
                                        R.color.x_000000_op60
                                    )
                                    else -> updateProgressStep(PROGRESS_STEP_MEASUREMENT_PREPARE)
                                }
                            }
                            else -> cancelProgressStep(progressStep)
                        }
                    }
                }.lparams(wrapContent, wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                    bottomMargin = dip(22.0f)
                }

                analyzingView = AnalyzingViewComponent<MeasurementScreen>(ServiceCategory.MEASUREMENT)
                    .createView(AnkoContext.create(requireContext(), this@MeasurementScreen))
                    .also { v ->
                        addView(v)
                        v.isVisible = false
                    }
            }
        }.view

    override fun onDestroy() {
        releaseEffectSound()
        super.onDestroy()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // 측정에서 전달되는 값으로 처리 하기엔 페이지 이동이 불안정함
        Headset.observeSensorsAttachmentStatusChange(this@MeasurementScreen) { status ->
            Timber.d("---> 센서 부착 상태 : $status , $neuroFeedbackProgressTimeSeconds")
            when (status) {
                SensorsAttachmentStatus.DETACHED -> {
                    if (neuroFeedbackProgressTimeSeconds in 1 until MEASUREMENT_DURATION_1_MINUTES) {
                        progressiveEffectSound(EffectSound.SCAN_DETACHED)
                    }
                }
                else -> doseNothing()
            }
        }
    }

    private val downstreamTrigger: PublishProcessor<Boolean> by lazy {
        PublishProcessor.create<Boolean>()
    }

    private fun pauseScanningDelayed() {
        downstreamTrigger.onNext(false)
    }

    private fun resumeScanningDelayed() {
        downstreamTrigger.onNext(true)
    }

    private var scanningDelayedFinalizer: Disposable? = null

    private var neuroFeedbackProgressTimeSeconds: Int = 0

    private var progressStep: Int by Delegates.observable(PROGRESS_STEP_MEASUREMENT_READY) { _, o, n ->
        Timber.d("--> 현재 측정 진행 상태 : [${dumpProgressStep(o)}] -> [${dumpProgressStep(n)}]")
        if (o != n) {
            updateMessage(n)
            updateSubMessage(n)
            updateProgressController(n)
            when (n) {
                PROGRESS_STEP_MEASUREMENT_READY -> {
                    stopEffectSound()
                }
                PROGRESS_STEP_MEASUREMENT_PREPARE -> {
                    initEffectSounds {
                        progressiveEffectSound(EffectSound.SCAN_START)
                        scanningDelayedFinalizer =
                            Flowable.timer(EffectSound.SCAN_START.duration(requireContext()).millisToSeconds().toLong().also {
                                Timber.d(
                                    "--> 측정시작 DURATION [$it]"
                                )
                            }, TimeUnit.SECONDS)
                                .subscribeOn(Schedulers.io())
                                .observeOn(Schedulers.io())
                                .compose(FlowableTransformers.valve(downstreamTrigger, true))
                                .observeOn(AndroidSchedulers.mainThread())
                                .bindUntilEvent(this@MeasurementScreen, Lifecycle.Event.ON_DESTROY)
                                .subscribe(
                                    { updateProgressStep(PROGRESS_STEP_MEASUREMENT_SCANNING) },
                                    { e -> Timber.e(e) }
                                )
                    }
                }
                PROGRESS_STEP_MEASUREMENT_SCANNING -> {
                    updateProgressTime()
                    Headset.startMeasurement(
                        this@MeasurementScreen,
                        MEASUREMENT_DURATION_1_MINUTES
                    ) { status ->
                        neuroFeedbackProgressTimeSeconds = status.elapsedTime
                        updateProgressTime(neuroFeedbackProgressTimeSeconds)
                        when (status) {
                            is MeasurementStatus.Start -> doseNothing()
                            is MeasurementStatus.Measuring -> {
                                if (status.values.isNotEmpty() && status.sensorsStatus == SensorsAttachmentStatus.DETACHED) {
                                    progressiveEffectSound(EffectSound.SCAN_DETACHED)
                                }
                            }
                            is MeasurementStatus.Stop -> {
                                Timber.i("--> 측정 종료")
                                progressiveEffectSound(EffectSound.SCAN_FINISH)
                                ContentResultHelper.createMeasurementResult(
                                    requireContext(),
                                    status.elapsedTime,
                                    status.result
                                ) { r ->
                                    // 데이터 저장
                                    UserConfiguration.get(requireContext()).apply {
                                        latestMeasurementdate = r?.id?.convertIdToFormatString("yyyy.MM.dd HH:mm") ?: ""
                                        brainScore = r?.brainHealthScore ?: 0
                                    }.set(requireContext())
                                }
                                Single.timer(500L, TimeUnit.MILLISECONDS)
                                    .subscribeOn(Schedulers.newThread())
                                    .bindUntilEvent(this@MeasurementScreen, Lifecycle.Event.ON_DESTROY)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(
                                        { updateProgressStep(PROGRESS_STEP_MEASUREMENT_ANALYZING) },
                                        { e -> Timber.e(e) }
                                    )
                            }
                            is MeasurementStatus.Cancel -> {
                                Timber.i("---> 측정 취소 : [${status.cause}]")
                                updateProgressStep(PROGRESS_STEP_MEASUREMENT_READY)

                            }
                        }
                    }
                }
                else -> {
                    this@MeasurementScreen.view?.run {
                        Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888).also { bmp ->
                            draw(Canvas(bmp))
                        }
                    }?.let { bmp ->
                        Dali.create(context)
                            .load(bmp)
                            .blurRadius(25)
                            .concurrent()
                            .skipCache()
                            .get()
                    }.let { screenshot ->
                        analyzingView?.backgroundDrawable = screenshot
                        analyzingView?.isVisible = true
                    }
                }
            }
        }
    }


    fun updateProgressStep(step: Int) {
        progressStep = step
    }

    private fun updateMessage(progressStep: Int) {
        progressMessage?.setText(
            when (progressStep) {
                PROGRESS_STEP_MEASUREMENT_READY -> getString(R.string.measurement_screen_020)
                PROGRESS_STEP_MEASUREMENT_PREPARE -> getString(R.string.measurement_screen_040)
                PROGRESS_STEP_MEASUREMENT_SCANNING -> getString(R.string.measurement_screen_060)
                else -> ""
            }
        )
    }

    private fun updateSubMessage(progressStep: Int) {
        progressTime?.isInvisible = true
        progressSubMessage?.isInvisible = false
        progressSubMessage?.setText(
            when (progressStep) {
                PROGRESS_STEP_MEASUREMENT_READY -> getString(R.string.measurement_screen_030)
                PROGRESS_STEP_MEASUREMENT_PREPARE -> getString(R.string.measurement_screen_050)
                else -> ""
            }
        )

        // 따옴표 노출 관련
        updateQuotes()
    }

    private fun updateProgressTime(seconds: Int = 0) {
        progressSubMessage?.isInvisible = true
        progressTime?.isInvisible = false
        if (MEASUREMENT_DURATION_1_MINUTES - seconds >= 0) {
            progressTime?.text = (MEASUREMENT_DURATION_1_MINUTES - seconds).also { s ->
            }.secondsToFormatString(DTP_MM_SS).also {
                Timber.d("--> Time($it)")
            }
        }

        // 따옴표 노출 관련
        updateQuotes()
    }

    private fun updateQuotes() {
        subMessageFront?.isInvisible = progressSubMessage?.isInvisible!!
        subMessageBack?.isInvisible = progressSubMessage?.isInvisible!!
        if (!progressSubMessage?.isInvisible!!) {
            var tv: TextView = progressSubMessage?.currentView as TextView
            if (tv.text.toString() == "") {
                subMessageFront?.isInvisible = true
                subMessageBack?.isInvisible = true
            }
        }
    }

    private fun updateProgressController(progressStep: Int) {
        progressButton?.setImageResource(
            when (progressStep) {
                PROGRESS_STEP_MEASUREMENT_READY -> R.drawable.selector_measuremt_start
                else -> R.drawable.selector_measuremt_stop
            }
        )
    }

    private fun cancelProgressStep(progressStep: Int): Boolean {
        when (progressStep) {
            PROGRESS_STEP_MEASUREMENT_READY -> {
                releaseEffectSound()
                return false
            }
            PROGRESS_STEP_MEASUREMENT_ANALYZING -> {
                alert(requireContext(), R.string.measurement_screen_110, gravity = Gravity.CENTER)
                return true
            }
            else -> {
                if (progressStep == PROGRESS_STEP_MEASUREMENT_PREPARE) {
                    pauseScanningDelayed()
                }
                StopPopScreen()
                    .withArguments(
                        Pair(FRAGMENT_ARGUMENTS_COMMENT, R.string.measurement_screen_090),
                        Pair(FRAGMENT_ARGUMENTS_MESSAGE, R.string.measurement_screen_100)
                    )
                    .run {
                        positiveAction = {
                            stopEffectSound() // 팀장님이 안 넣은 이유를 찾아보자
                            when (progressStep) {
                                PROGRESS_STEP_MEASUREMENT_PREPARE -> {
                                    scanningDelayedFinalizer?.dispose()
                                    scanningDelayedFinalizer = null
                                    updateProgressStep(PROGRESS_STEP_MEASUREMENT_READY)
                                }
                                PROGRESS_STEP_MEASUREMENT_SCANNING -> {
                                    if (Headset.isMeasuring()) {
                                        Headset.stopMeasurement(CancelCause.Forced)
                                    } else updateProgressStep(PROGRESS_STEP_MEASUREMENT_READY)
                                }
                            }
                        }
                        negativeAction = {
                            if (progressStep == PROGRESS_STEP_MEASUREMENT_PREPARE) {
                                resumeScanningDelayed()
                            }
                        }
                        show(this@MeasurementScreen.fragmentManager)
                    }
                return true
            }
        }
    }

    override fun onBackPressed(): Boolean {
        return cancelProgressStep(progressStep)
    }

    private fun dumpProgressStep(step: Int): String {
        return when (step) {
            PROGRESS_STEP_MEASUREMENT_READY -> "STEP_SCAN_READY"
            PROGRESS_STEP_MEASUREMENT_PREPARE -> "STEP_SCAN_PREPARE"
            PROGRESS_STEP_MEASUREMENT_SCANNING -> "STEP_SCANNING"
            PROGRESS_STEP_MEASUREMENT_ANALYZING -> "STEP_ANALYZING"
            else -> "UNKNOWN_PROGRESS_STEP"
        }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥ SOUND POOL
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */
    private var effectSoundInitializedCount: Int = 0

    private fun initEffectSounds(onComplete: () -> Unit) {
        if (effectSoundInitializedCount == 3) {
            onComplete()
            return
        }
        effectSound.setOnLoadCompleteListener { _, _, status ->
            if (status == 0 && ++effectSoundInitializedCount == 3) {
                onComplete()
            }
        }
        effectSoundIds
    }

    private val effectSound: SoundPool by lazy {
        SoundPool.Builder()
            .setMaxStreams(1)
            .setAudioAttributes(
                AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            .build()
    }

    private val effectSoundIds: IntArray by lazy {
        intArrayOf(
            effectSound.load(requireContext(), EffectSound.SCAN_START.resource, 1),   // 측정시작
            effectSound.load(requireContext(), EffectSound.SCAN_FINISH.resource, 1),  // 측정완료
            effectSound.load(requireContext(), EffectSound.SCAN_DETACHED.resource, 1) // 센서부착상태
        )
    }

    private var playingEffectSoundId: Int = 0
    private var playingEffectSoundStreamId: Int = 0

    private fun progressiveEffectSound(sound: EffectSound) {
        Timber.d("--> 효과음[$sound]")
        effectSoundIds[sound.ordinal].let { soundId ->
            if (playingEffectSoundId != soundId) {
                playingEffectSoundId = soundId
                playingEffectSoundStreamId = effectSound.play(soundId, 1f, 1f, 0, 0, 1f)
            } else if (sound == EffectSound.SCAN_DETACHED) { // 부착상태 효과음 플레이타임이 7초
                playingEffectSoundId = 0
            }
        }
    }

    private fun stopEffectSound() {
        if (playingEffectSoundId != 0) {
            playingEffectSoundId = 0
            effectSound.stop(playingEffectSoundStreamId)
        }
    }

    private fun releaseEffectSound() {
        effectSound.release()
        effectSoundInitializedCount = 0
    }
}