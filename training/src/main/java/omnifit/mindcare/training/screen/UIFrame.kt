package omnifit.mindcare.training.screen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.AssetFileDescriptor
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity
import androidx.collection.SparseArrayCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import com.crashlytics.android.Crashlytics
import com.trello.rxlifecycle3.android.lifecycle.kotlin.bindUntilEvent
import es.dmoral.toasty.Toasty
import io.fabric.sdk.android.Fabric
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import omnifit.commons.core.doseNothing
import omnifit.mindcare.training.TrainingApplication
import omnifit.mindcare.training.R
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.identifier
import omnifit.mindcare.training.screenTo
import omnifit.mindcare.training.helper.MeditationSource
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.constraint.layout.matchConstraint
import timber.log.Timber
import java.util.concurrent.TimeUnit

class UIFrame : AppCompatActivity() {

    private var liveBackground: VideoView? = null
    private var onPlaybackStatusChangeListener: OnPlaybackStatusChangeListener? = null

    @Suppress("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        requireDensity()
        registerReceiver(keyEventReceiver, IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
        super.onCreate(savedInstanceState)

        constraintLayout {

            liveBackground = videoView {
                setOnPreparedListener { mp ->
                    mp.setOnInfoListener { _, what, _ ->
                        (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START)
                            .also { b ->
                                if (b) {
                                    Observable.interval(0L, 1L, TimeUnit.SECONDS)
                                        .subscribeOn(Schedulers.io())
                                        .bindUntilEvent(attachedScreen as LifecycleOwner, Lifecycle.Event.ON_DESTROY)
                                        .takeUntil {
                                            it == mp.duration.div(1000).toLong()
                                        }
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(
                                            {
                                                onPlaybackStatusChangeListener?.onPlaybackStatusChanged(
                                                    PLAYBACK_STATUS_PLAYING,
                                                    mp.currentPosition,
                                                    mp.duration
                                                )
                                            },
                                            { e -> Timber.e(e) }
                                        )
                                    onPlaybackStatusChangeListener?.onPlaybackStatusChanged(PLAYBACK_STATUS_START, mp.currentPosition, mp.duration)
                                    background = EMPTY_DRAWABLE
                                }
                            }
                    }
                    mp.setOnCompletionListener {
                        background = temporaryBackground
                        onPlaybackStatusChangeListener?.onPlaybackStatusChanged(PLAYBACK_STATUS_COMPLETE, mp.duration, mp.duration)
                    }
                }
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }

            frameLayout {
                id = R.id.frame_container
            }.lparams(matchConstraint, matchConstraint) {
                startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
            }
        }

        screenTo(SplashScreen(), R.anim.slide_hold, R.anim.slide_hold)
    }

    private val attachedScreenCache: SparseArrayCompat<Class<out UIScreen>> = SparseArrayCompat()
    var attachedScreen: UIScreen? = null
    override fun onAttachFragment(fragment: Fragment) {
        attachedScreen?.let { screen ->
            attachedScreenCache.put(attachedScreenCache.size(), screen::class.java)
        }
        (fragment as? UIScreen)?.let { screen ->
            screen::class.java.let { cls ->
                if (attachedScreenCache.containsValue(cls)) {
                    attachedScreenCache.indexOfValue(cls).let { n ->
                        attachedScreenCache.removeAtRange(n, attachedScreenCache.size() - n)
                    }
                }
            }
            attachedScreen = screen
            onPlaybackStatusChangeListener = screen
        }
        startLiveBackground()
    }

    override fun onUserLeaveHint() {
        attachedScreen?.onUserLeaveHint()
    }

    private fun startLiveBackground() {
        liveBackground?.let { v ->
            if (v.isVisible) v.start()
        }
    }

    fun stopLiveBackground() {
        attachedScreen?.view?.background = temporaryBackground
        liveBackground?.let { v ->
            if (v.isVisible && v.isPlaying) {
                v.suspend()
            }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
            keepScreenOn()
            layoutNoLimits()
            hideNavigation()
            attachedScreen?.onWindowFocusChanged(hasFocus)
        }
    }

    override fun onDestroy() {
        unregisterReceiver(keyEventReceiver)
        super.onDestroy()
    }

    override fun onBackPressed() {
        attachedScreen?.let { screen ->
            if (screen.onBackPressed()) return
            if (!screen.isRootScreen
                && !attachedScreenCache.isEmpty
            ) {
                attachedScreenCache[attachedScreenCache.size() - 1]
                    ?.newInstance()
                    ?.let { backScreen ->
                        screenTo(backScreen)
                    }
                return
            }
        }
        super.onBackPressed()
    }

    private val keyEventReceiver: BroadcastReceiver by lazy {
        object : BroadcastReceiver() {
            val SYSTEM_DIALOG_REASON_KEY = "reason"
            val SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions"
            val SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps"
            val SYSTEM_DIALOG_REASON_HOME_KEY = "homekey"
            override fun onReceive(context: Context, intent: Intent) {
                when (intent.action) {
                    Intent.ACTION_CLOSE_SYSTEM_DIALOGS -> {
                        when (intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY)) {
                            SYSTEM_DIALOG_REASON_HOME_KEY    -> attachedScreen?.onHomePressed()
                            SYSTEM_DIALOG_REASON_RECENT_APPS -> attachedScreen?.onRecentAppsPressed()
                            else                             -> doseNothing()
                        }
                    }
                    else                        -> doseNothing()
                }
            }
        }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private fun keepScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    private fun layoutNoLimits() {
        window.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
    }

    private fun hideNavigation() {
        window.decorView.systemUiVisibility = DEFAULT_SYSTEM_UI_FLAG
    }

    private fun requireDensity() {
        (application as TrainingApplication).desiredDisplayMetrics?.let { metrics ->
            resources.displayMetrics.setTo(metrics)
        }
    }

    fun initVideoSource(source: MeditationSource?) {
        liveBackground?.let { vv ->
            vv.isVisible = source != null
            if (vv.isVisible) {
                source?.run {
                    vv.background = BitmapDrawable(resources, thumbnail).also {
                        temporaryBackground = it
                    }
                    vv.setVideoURI(this.source as? Uri)
                }
            }
        }
    }




    /*
   ╔═══════════════════════════════════════════════════════════════════════════
   ║
   ║ ⥥
   ║
   ╚═══════════════════════════════════════════════════════════════════════════
   */

    fun alert(
        context: Context,
        messageRes: Int,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = alert(context, context.getString(messageRes), colorRes, gravity)

    fun alert(
        context: Context,
        message: String,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = Toasty.custom(
        context,
        message,
        null,
        ContextCompat.getColor(context, colorRes),
        Toast.LENGTH_SHORT,
        false,
        true
    ).apply { setGravity(gravity, 0, 0) }.show()

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    private var temporaryBackground: Drawable? = null

    val width: Int
        get() = resources.displayMetrics.widthPixels

    val height: Int
        get() = (resources.displayMetrics.heightPixels + navigationBarHeight)

    val dpi: Int
        get() = resources.displayMetrics.densityDpi

    val navigationBarHeight: Int
        get() = identifier(ID_NAME_NAVIGATION_BAR_HEIGHT, DEF_TYPE_DIMEN, DEF_PACKAGE_ANDROID)
            .let { id ->
                if (id != 0) dimen(id) else 0
            }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    companion object {
        const val ID_NAME_NAVIGATION_BAR_HEIGHT: String = "navigation_bar_height"
        const val DEF_TYPE_DIMEN: String = "dimen"
        const val DEF_PACKAGE_ANDROID: String = "android"

        const val DEFAULT_SYSTEM_UI_FLAG: Int =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_FULLSCREEN

        val EMPTY_DRAWABLE: Drawable? = null

        const val PLAYBACK_STATUS_START: Int = 0
        const val PLAYBACK_STATUS_PLAYING: Int = 1
        const val PLAYBACK_STATUS_COMPLETE: Int = 2
    }

    interface OnPlaybackStatusChangeListener {
        fun onPlaybackStatusChanged(status: Int, curr: Int, duration: Int)
    }
}