package omnifit.mindcare.training.screen.pop

import org.jetbrains.anko.AnkoComponent

interface AnkoLifecycleComponent<P> : AnkoComponent<P> {
    fun onViewCreated(owner: P)
}