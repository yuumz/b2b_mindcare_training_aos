package omnifit.mindcare.training.screen.pop

import android.content.DialogInterface
import android.view.Gravity
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.UIPopScreen
import omnifit.mindcare.training.Font
import omnifit.mindcare.training.R
import omnifit.mindcare.training.view.nonSpacingTextView
import org.jetbrains.anko.*
import org.jetbrains.anko.constraint.layout.constraintLayout
import org.jetbrains.anko.sdk27.coroutines.onClick

class SensorsAttachmentPopScreen : UIPopScreen() {

    lateinit var screenUiComponent: SensorsAttachmentPopScreenComponent

    override fun <T : UIPopScreen> T.createScreenUiComponent(): AnkoComponent<T> =
        SensorsAttachmentPopScreenComponent().apply { screenUiComponent = this } as AnkoComponent<T>

    override fun onStart() {
        super.onStart()
        dialog!!.window.setWindowAnimations(R.style.PopScreenAnimation)
    }
}

class SensorsAttachmentPopScreenComponent :
    AnkoLifecycleComponent<SensorsAttachmentPopScreen> {

    override fun createView(ui: AnkoContext<SensorsAttachmentPopScreen>): View = with(ui) {
        relativeLayout {
            backgroundColorResource = R.color.x_000000_op15

            constraintLayout {
                backgroundResource = R.drawable.shape_round_7_bg_pop

                nonSpacingTextView(R.string.measurement_stop_010) {
                    id = R.id.screen_widget_id_01
                    typeface = Font.kopubDotumBold
                    textSize = 28.0f
                    textColorResource = R.color.x_1c1c1c
                    letterSpacing = -0.01f
                    includeFontPadding = false
                    setLineSpacing(10.7f, 1.0f)
                }.lparams(dip(0.0f), wrapContent) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(50.7f)
                    topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                    topMargin = dip(53.3f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(50.7f)
                }

                button(R.string.dialog_010) {
                    backgroundResource = R.drawable.selector_round_10_bottom_button_bg
                    textSize = 24.0f
                    typeface = Font.kopubDotumBold
                    animation = null
                    setTextColor(AppCompatResources.getColorStateList(context, R.color.selector_bottom_button_text))
                    onClick {
                        owner.buttonWhich = DialogInterface.BUTTON_POSITIVE
                        owner.dismiss()
                    }
                }.lparams(dip(0.0f), dip(74.7f)) {
                    startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                    marginStart = dip(42.7f)
                    endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    marginEnd = dip(42.7f)
                    bottomMargin = dip(30.0f)
                    bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                }

            }.lparams(dip(514.0f), dip(320.0f)) {
                gravity = Gravity.CENTER
            }
        }
    }

    override fun onViewCreated(owner: SensorsAttachmentPopScreen) {
    }
}