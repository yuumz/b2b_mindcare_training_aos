package omnifit.mindcare.training.screen

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.UIPopScreen
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.*
import omnifit.mindcare.training.GlideApp
import omnifit.mindcare.training.R
import omnifit.mindcare.training.TrainingApplication
import omnifit.mindcare.training.common.OnFocusListenable
import omnifit.mindcare.training.headset.Headset
import omnifit.mindcare.training.helper.MeditationSource
import org.jetbrains.anko.doAsync
import timber.log.Timber

abstract class UIScreen : Fragment(), OnFocusListenable, UIFrame.OnPlaybackStatusChangeListener {

    var isRootScreen: Boolean = false
    var videoBackgroundSource: MeditationSource? = null

    private var screenFrame: UIFrame? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        screenFrame = context as? UIFrame
        screenFrame?.initVideoSource(videoBackgroundSource)
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (nextAnim == 0) {
            onScreenTransitionEnd(this, enter)
            return super.onCreateAnimation(transit, enter, nextAnim)
        }
        return AnimationUtils.loadAnimation(requireContext(), nextAnim).run {
            view?.let { v ->
                v.setLayerType(View.LAYER_TYPE_HARDWARE, null)
                setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        onScreenTransitionStart(this@UIScreen, enter)
                        if (nextAnim == R.anim.slide_enter_right) {
                            ViewCompat.setTranslationZ(v, 1.0f)
                        }
                    }

                    override fun onAnimationRepeat(animation: Animation) {
                    }

                    override fun onAnimationEnd(animation: Animation) {
                        v.setLayerType(View.LAYER_TYPE_NONE, null)
                        onScreenTransitionEnd(this@UIScreen, enter)
                        if (nextAnim == R.anim.slide_enter_right) {
                            Handler(Looper.getMainLooper()).post {
                                ViewCompat.setTranslationZ(v, 0.0f)
                            }
                        }
                    }
                })
            }
            AnimationSet(true).apply { addAnimation(this@run) }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        if (hasFocus) {
        }
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    open fun onBackPressed(): Boolean {
        return false
    }

    open fun onScreenTransitionStart(screen: UIScreen, isEnter: Boolean) {
    }

    open fun onScreenTransitionEnd(screen: UIScreen, isEnter: Boolean) {
    }

    override fun onResume() {
        super.onResume()
        onUserLeave = false
    }

    @Volatile var onUserLeave: Boolean = false
    open fun onUserLeaveHint() {
        onUserLeave = true
    }

    open fun onHomePressed() {

    }

    open fun onRecentAppsPressed() {

    }

    override fun onPlaybackStatusChanged(status: Int, curr: Int, duration: Int) {
    }

    open fun alert(
        context: Context,
        messageRes: Int,
        colorRes: Int = R.color.x_000000_op60,
        gravity: Int = Gravity.BOTTOM
    ) = screenFrame?.alert(context, messageRes, colorRes, gravity)

    open fun alert(
        context: Context,
        message: String
    ) = Toasty.custom(
        context,
        message,
        null,
        ContextCompat.getColor(context, R.color.x_000000_op60),
        Toast.LENGTH_SHORT,
        false,
        true
    ).show()

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    protected fun appFinish(): Boolean {
        GlideApp.get(requireContext()).run {
            clearMemory()
            clearFindViewByIdCache()
            doAsync { clearDiskCache() }
        }
        Headset.close {
            requireActivity().run {
                (application as? TrainingApplication)?.close()
                finishAndRemoveTask()
            }
            System.exit(0)
        }
        return true
    }

    protected fun performBackPressed() {
        screenFrame?.onBackPressed()
    }

    protected fun stopLiveBackground() {
        screenFrame?.stopLiveBackground()
    }

    /*
    ╔═══════════════════════════════════════════════════════════════════════════
    ║
    ║ ⥥
    ║
    ╚═══════════════════════════════════════════════════════════════════════════
    */

    protected val fadeIn: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_in)
    }

    protected val fadeOut: Animation by lazy {
        AnimationUtils.loadAnimation(context, android.R.anim.fade_out)
    }
}