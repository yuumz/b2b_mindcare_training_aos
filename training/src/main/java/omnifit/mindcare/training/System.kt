package omnifit.mindcare.training

import android.content.Context
import android.content.SharedPreferences

private const val PREFS_NAME: String = "${BuildConfig.APPLICATION_ID}.prefs"
private var sharedPreferences: SharedPreferences? = null
fun requireSharedPreferences(context: Context): SharedPreferences = sharedPreferences ?: context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)