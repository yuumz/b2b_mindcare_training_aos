package omnifit.mindcare.training

object DebugOptions {
    const val HEADSET_STATUS_VISUALIZER_VISIBLE: Boolean = true
}