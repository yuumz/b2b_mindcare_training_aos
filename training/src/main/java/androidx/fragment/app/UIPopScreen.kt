package androidx.fragment.app

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import omnifit.mindcare.training.R
import omnifit.mindcare.training.screen.pop.AnkoLifecycleComponent
import omnifit.mindcare.training.screen.UIFrame
import omnifit.mindcare.training.whenTrue
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext

abstract class UIPopScreen : DialogFragment() {

    var dismissListener: (DialogInterface) -> Unit = {}
    var windowAnimations = -1

    val isPopCanceledOnTouchOutside = false
    val isPopCancelable = false
    val isAllowCrossPopScreen = false

    var buttonWhich = 0
    var positiveAction: () -> Unit = {}
    var negativeAction: () -> Unit = {}

    private val systemUiFullscreen = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)


    override fun onAttach(context: Context) {
        super.onAttach(context)
        isCancelable = isPopCancelable
    }

    private var screenFrame: UIFrame? = null

    private lateinit var screenUiComponent: AnkoComponent<UIPopScreen>

    abstract fun <T : UIPopScreen> T.createScreenUiComponent(): AnkoComponent<T>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        screenFrame = context as? UIFrame
        return createScreenUiComponent()
            .apply { screenUiComponent = this@apply }
            .createView(AnkoContext.create(context!!, this))
            .apply {
                setOnTouchListener { _, _ -> true }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (screenUiComponent as? AnkoLifecycleComponent<UIPopScreen>)?.onViewCreated(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (windowAnimations != -1) dialog!!.window.attributes.windowAnimations = windowAnimations
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(activity, R.style.AppTheme_PopFullScreen) {
            override fun onBackPressed() {
                if (!this@UIPopScreen.onBackPressed()) super.onBackPressed()
            }
        }.apply {
            window.requestFeature(Window.FEATURE_NO_TITLE)
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window.attributes.dimAmount = 0.6f
            window.decorView.systemUiVisibility = systemUiFullscreen

            setCancelable(isPopCancelable)
            setCanceledOnTouchOutside(isPopCanceledOnTouchOutside)
        }
    }

    override fun onStart() {
        super.onStart()
        KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK).let { has ->
            dialog!!.window.setLayout(frameWidth, frameHeight)
        }
        dialog!!.onContentChanged()
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        dialog.dismiss()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        when (buttonWhich) {
            DialogInterface.BUTTON_POSITIVE -> positiveAction()
            DialogInterface.BUTTON_NEGATIVE -> negativeAction()
        }
        dismissListener(dialog)
    }

    fun show(manager: FragmentManager?) {
        fun isPopScreen(f: Fragment): Boolean {
            return f as? DialogFragment != null && !(f as UIPopScreen).isAllowCrossPopScreen
        }

        fun doDismiss(f: Fragment): Fragment {
            if (f::class.java.simpleName != this::class.java.simpleName) {
                (f as DialogFragment).dismissAllowingStateLoss()
            }
            return f
        }

        fun isDismissed(f: Fragment): Boolean {
            return (f as DialogFragment).mDismissed
        }

        fun showPopScreen() {
            if (!isAdded) {
                mDismissed = false
                mShownByMe = true
                manager
                    ?.beginTransaction()
                    ?.add(this, this::class.java.simpleName)
                    ?.commitAllowingStateLoss()
            }
        }
        manager
            ?.fragments
            ?.filter(::isPopScreen)
            ?.map(::doDismiss)
            ?.filterNot(::isDismissed)
            ?.isEmpty()
            ?.whenTrue(::showPopScreen)
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    companion object {
        fun clear(manager: FragmentManager?) {
            fun isPopScreen(f: Fragment): Boolean {
                return f as? DialogFragment != null && f.isCancelable
            }

            fun doDismiss(f: Fragment): Fragment {
                (f as DialogFragment).dismissAllowingStateLoss()
                return f
            }
            manager
                ?.fragments
                ?.filter(::isPopScreen)
                ?.map(::doDismiss)
        }
    }

    val frameWidth: Int
        get() = screenFrame?.width ?: 0

    val frameHeight: Int
        get() = screenFrame?.height ?: 0

    val frameDpi: Int
        get() = screenFrame?.dpi ?: 0

}